# NASTI: Coarse grained RNA structure modeling

NASTI means NAST improved. NAST stands for Nucleic Acids Simulation Toolkit.
NASTI was developed to replace NAST.
It does basically the same: Sampling 3D RNA structures using a low resolution forcefield and Newtonian
dynamics simulation.
However, unlike NAST, NASTI does not explode numerically. 

This Project is described in the article "Improving the Numerical Stability of the NAST Force Field for RNA Simulations"
in the "Journal of Chemical Theory and Computation".

## Installation

### Version

This is version 1.0.

### Requirements

NASTI was compiled with the following software:
- openSUSE 42.2
- gcc (SUSE Linux) 4.8.5
- g++ (SUSE Linux) 4.8.5
- GNU Make 4.0
- Python 2.7.13
- cmake 3.5.2
- virtualenv 15.1.0

### The install.sh script

If you have the previously described tools or similar versions, you can use
the install.sh script to install NASTI.
It will create a virtual environment for python and install NASTI in there.
The installation includes downloading and compiling OpenMM 7.2.
So the whole process might take a while.

Move to the directory where you found this README file.
Run the install script by typing
```
./install.sh
```

If you want to run NASTI now, you have to start by loading the virtual environment.
The following commands assume that you did not change any paths in the script and
did not yet move to another directory.
If you are using bash type
```
source build/venv_nasti/bin/activate
```
and if you are using cshell type
```
source build/venv_nasti/bin/activate.csh
```

To get rid of the installation just delete the 'build' directory.

## Run NASTI

There are some examples in the examples folder.
Go to the folder 1fir.
Type
```
nasti sim.yaml
```
to run a simulation.

### Output of the simulation

The simulation will write all output to a folder out (unless you changed that in sim.yaml).
For each simulation run there is one folder with a number.
Therein one finds files with the prefix eq and prod for data from equilibration and production
respectively.
Usually one is interested in the production only.
The following files are written by NASTI:

- eq.thermodat and prod.thermodat contain energies and temperatures of each snapshot in human readable text format.
- rna.bin is a binary file of the C3' RNA structure that can be read by NASTI's routines.
- eq.traj and prod.traj contain binary trajectories 

The trajectories can be viewed at after conversion to .psf and .pdb format.
Type
```
nasti2pdb prod.traj
```

to get a pdb-trajectory in the same directory.
However, the tool nasti2pdb requires rna.bin and XXX.traj to be in the same directory,
so move them only together and do not rename rna.bin.
You can view the .psf/.pdb trajectory using vmd version 1.9.1 (https://www-s.ks.uiuc.edu/Research/vmd/).

You can also compute RMSD and GDT-TS values with respect to some reference structure (usually the crystal structure from the pdb) for
all snapshots in the trajectory.
First, you need to convert the reference structure which is in .pdb format to the binary format read by NASTI.
Go back to examples/1fir/ .
Type
```
nastiBinStruct 1FIR.pdb A
```

The second parameter (in this case A) is the chain identifier.
The distributed examples have the following identifiers

- 1FIR: A
- 1Y26: X
- 2GDI: X
- 3Q3Z: A
- 4LVV: A

Type
```
nastiRmsd out/1/prod.traj 1FIR.bin
```

The output is a human readable file with RMSD and GDT-TS scores.
They are in the same order as the snapshots in the trajectories.

### The input file sim.yaml

Use this input file to change the simulation conditions.
You can run mutliple simulations using the parameters from and to under seeds.
