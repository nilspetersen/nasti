#ifndef OPENMM_NASTI_ANGLEFORCE_PROXY_H_
#define OPENMM_NASTI_ANGLEFORCE_PROXY_H_

#include "openmm/serialization/SerializationProxy.h"

namespace OpenMM {

/**
 * This is a proxy for serializing NastiAngleforce objects.
 */

class NastiAngleForceProxy : public SerializationProxy {
public:
    NastiAngleForceProxy();
    
    void serialize(const void* object,
		   SerializationNode& node) const;

    void* deserialize(const SerializationNode& node) const;
};

} // namespace OpenMM

#endif /*OPENMM_NASTI_ANGLEFORCE_PROXY_H_*/
