
#include "NastiTorsionForce.h"
#include "openmm/Platform.h"
#include "openmm/internal/AssertionUtilities.h"
#include "openmm/serialization/XmlSerializer.h"
#include <iostream>
#include <sstream>

using namespace Nasti;
using namespace OpenMM;
using namespace std;

extern "C" void registerNastiSerializationProxies();

void testSerialization() {

    // Create a Force.

    NastiTorsionForce force;
    force.addTorsion(0, 1, 2, 3, 1.0, 2.0, 0.1);
    force.addTorsion(3, 2, 4, 8, 0.2, 2.7, 1.1);
    force.addTorsion(2, 3, 9, 7, 0.1, 6.0, 0.2);
    force.addTorsion(22, 100, 10, 12, 3.1, 1000.0, 0.78);

    // Serialize and then deserialize it.

    stringstream buffer;
    XmlSerializer::serialize<NastiTorsionForce>(&force, "Force", buffer);
    NastiTorsionForce* copy = XmlSerializer::deserialize<NastiTorsionForce>(buffer);

    // Compare the two forces to see if they are identical.

    NastiTorsionForce& force2 = *copy;
    ASSERT_EQUAL(force.getNumTorsions(), force2.getNumTorsions());
    for (int i = 0; i < force.getNumTorsions(); i++) {
        int i1, i2, i3, i4;
	int j1, j2, j3, j4;
	double phase1, phase2;
	double margin1, margin2;
	double forceConstant1, forceConstant2;
	
	force.getTorsionParameters(i,
				   i1, i2, i3, i4,
				   phase1,
				   forceConstant1,
				   margin1);

	force2.getTorsionParameters(i,
				    j1, j2, j3, j4,
				    phase2,
				    forceConstant2,
				    margin2);
	ASSERT_EQUAL(i1, j1);
        ASSERT_EQUAL(i2, j2);
	ASSERT_EQUAL(i3, j3);
        ASSERT_EQUAL(i4, j4);
	
        ASSERT_EQUAL(phase1, phase2);
        ASSERT_EQUAL(forceConstant1, forceConstant2);
	ASSERT_EQUAL(margin1, margin2);
    }
}

int main() {
    try {
	registerNastiSerializationProxies();
        testSerialization();
    }
    catch(const exception& e) {
        cout << "exception: " << e.what() << endl;
        return 1;
    }
    cout << "Done" << endl;
    return 0;
}
