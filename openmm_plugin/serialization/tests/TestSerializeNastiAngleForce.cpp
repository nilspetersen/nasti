
#include "NastiAngleForce.h"
#include "openmm/Platform.h"
#include "openmm/internal/AssertionUtilities.h"
#include "openmm/serialization/XmlSerializer.h"
#include <iostream>
#include <sstream>

using namespace Nasti;
using namespace OpenMM;
using namespace std;

extern "C" void registerNastiSerializationProxies();

void testSerialization() {

    // Create a Force.

    vector<double> constCoeffs {2.5, 3.7, 2.23423413241324};
    vector<double> linCoeffs {234.234, 234.456, -87.5};
    vector<double> quadCoeffs {4.0, -95.4, 3.2};
    vector<double> intervalBorders {0.0, 1.5, 2.0};
    
    NastiAngleForce force(constCoeffs,
			  linCoeffs,
			  quadCoeffs,
			  intervalBorders);
    
    force.addAngle(0, 1, 2);
    force.addAngle(3, 2, 4);
    force.addAngle(2, 3, 9);
    force.addAngle(22, 100, 10);

    // Serialize and then deserialize it.

    stringstream buffer;
    XmlSerializer::serialize<NastiAngleForce>(&force, "Force", buffer);
    NastiAngleForce* copy = XmlSerializer::deserialize<NastiAngleForce>(buffer);

    // Compare the two forces to see if they are identical.

    NastiAngleForce& force2 = *copy;
    ASSERT_EQUAL(force.getNumAngles(), force2.getNumAngles());

    // compare the global parameters
    ASSERT_EQUAL_CONTAINERS(force.getConstantCoefficients(),
			    force2.getConstantCoefficients());
    ASSERT_EQUAL_CONTAINERS(force.getLinearCoefficients(),
			    force2.getLinearCoefficients());
    ASSERT_EQUAL_CONTAINERS(force.getQuadraticCoefficients(),
			    force2.getQuadraticCoefficients());
    ASSERT_EQUAL_CONTAINERS(force.getIntervalBorders(),
			    force2.getIntervalBorders());

    // compare the angle indices
    for (int i = 0; i < force.getNumAngles(); i++) {
        int i1, i2, i3;
	int j1, j2, j3;
	
	force.getAngleParameters(i, i1, i2, i3);
	force2.getAngleParameters(i, j1, j2, j3);
	
	ASSERT_EQUAL(i1, j1);
        ASSERT_EQUAL(i2, j2);
	ASSERT_EQUAL(i3, j3);
    }
}

int main() {
    try {
	registerNastiSerializationProxies();
        testSerialization();
    }
    catch(const exception& e) {
        cout << "exception: " << e.what() << endl;
        return 1;
    }
    cout << "Done" << endl;
    return 0;
}
