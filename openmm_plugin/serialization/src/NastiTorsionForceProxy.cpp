
#include "NastiTorsionForceProxy.h"
#include "NastiTorsionForce.h"
#include "openmm/serialization/SerializationNode.h"
#include <sstream>

using namespace Nasti;
using namespace OpenMM;
using namespace std;

NastiTorsionForceProxy::NastiTorsionForceProxy() : SerializationProxy("NastiTorsionForce") {
}

void
NastiTorsionForceProxy::serialize(const void* object,
				  SerializationNode& node) const {
    
    node.setIntProperty("version", 1);
    const NastiTorsionForce& force = *reinterpret_cast<const NastiTorsionForce*>(object);
    SerializationNode& torsions = node.createChildNode("Torsions");

    for (int i = 0; i < force.getNumTorsions(); i++) {
        int particle1, particle2, particle3, particle4;
        double phase, forceConstant, margin;
        force.getTorsionParameters(i,
				   particle1,
				   particle2,
				   particle3,
				   particle4,
				   phase,
				   forceConstant,
				   margin);

	SerializationNode& child = torsions.createChildNode("Torsion");
        child.setIntProperty("p1", particle1);
	child.setIntProperty("p2", particle2);
        child.setIntProperty("p3", particle3);
	child.setIntProperty("p4", particle4);
	child.setDoubleProperty("phase", phase);
	child.setDoubleProperty("forceConstant", forceConstant);
	child.setDoubleProperty("margin", margin);
    }
}

void*
NastiTorsionForceProxy::deserialize(const SerializationNode& node) const {
    
    if (node.getIntProperty("version") != 1)
        throw OpenMMException("Unsupported version number");

    NastiTorsionForce* force = new NastiTorsionForce();

    try {
        const SerializationNode& torsions = node.getChildNode("Torsions");
        for (int i = 0; i < (int) torsions.getChildren().size(); i++) {
            const SerializationNode& torsion = torsions.getChildren()[i];
            force->addTorsion(torsion.getIntProperty("p1"),
			      torsion.getIntProperty("p2"),
			      torsion.getIntProperty("p3"),
			      torsion.getIntProperty("p4"),
			      torsion.getDoubleProperty("phase"),
			      torsion.getDoubleProperty("forceConstant"),
			      torsion.getDoubleProperty("margin"));
        }
    }
    catch (...) {
        delete force;
        throw;
    }
    return force;
}
