
#include "NastiAngleForceProxy.h"
#include "NastiAngleForce.h"
#include "openmm/serialization/SerializationNode.h"
#include <sstream>

using namespace Nasti;
using namespace OpenMM;
using namespace std;

NastiAngleForceProxy::NastiAngleForceProxy() : SerializationProxy("NastiAngleForce") {
}

void
NastiAngleForceProxy::serialize(const void* object,
				SerializationNode& node) const {
    
    node.setIntProperty("version", 1);
    const NastiAngleForce& force = *reinterpret_cast<const NastiAngleForce*>(object);
    
    const vector<double>& constCoeffs = force.getConstantCoefficients();
    const vector<double>& linCoeffs = force.getLinearCoefficients();
    const vector<double>& quadCoeffs = force.getQuadraticCoefficients();
    const vector<double>& intervalBorders = force.getIntervalBorders();
    
    int nSplineNodes = intervalBorders.size();

    SerializationNode& globalParams = node.createChildNode("GlobalParameters");
    for(int i = 0; i < nSplineNodes; i++) {
	SerializationNode& splineNodeParams = globalParams.createChildNode("SplineNode");
	splineNodeParams.setDoubleProperty("a", constCoeffs[i]);
	splineNodeParams.setDoubleProperty("b", linCoeffs[i]);
	splineNodeParams.setDoubleProperty("c", quadCoeffs[i]);
	splineNodeParams.setDoubleProperty("t", intervalBorders[i]);
    }
    
    SerializationNode& angles = node.createChildNode("Angles");
    for (int i = 0; i < force.getNumAngles(); i++) {
        int particle1, particle2, particle3;
        force.getAngleParameters(i,
				 particle1,
				 particle2,
				 particle3);
	SerializationNode& child = angles.createChildNode("Angle");
        child.setIntProperty("p1", particle1);
	child.setIntProperty("p2", particle2);
        child.setIntProperty("p3", particle3);
    }
}

void*
NastiAngleForceProxy::deserialize(const SerializationNode& node) const {
    
    if (node.getIntProperty("version") != 1)
        throw OpenMMException("Unsupported version number");

    // 1 get global parameters
    vector<double> constCoeffs;
    vector<double> linCoeffs;
    vector<double> quadCoeffs;
    vector<double> intervalBorders;

    const SerializationNode& globParams = node.getChildNode("GlobalParameters");
    for(int i = 0; i < globParams.getChildren().size(); i++) {
	const SerializationNode& splineNode = globParams.getChildren()[i];
	constCoeffs.push_back(splineNode.getDoubleProperty("a"));
	linCoeffs.push_back(splineNode.getDoubleProperty("b"));
    	quadCoeffs.push_back(splineNode.getDoubleProperty("c"));
    	intervalBorders.push_back(splineNode.getDoubleProperty("t"));
    }
    
    // 2 create force
    NastiAngleForce* force = new NastiAngleForce(constCoeffs,
						 linCoeffs,
						 quadCoeffs,
						 intervalBorders);
    // 3 add angles
    try {
        const SerializationNode& angles = node.getChildNode("Angles");
        for (int i = 0; i < (int) angles.getChildren().size(); i++) {
            const SerializationNode& angle = angles.getChildren()[i];
            force->addAngle(angle.getIntProperty("p1"),
			    angle.getIntProperty("p2"),
			    angle.getIntProperty("p3"));
        }
    }
    catch (...) {
        delete force;
        throw;
    }
    return force;
}
