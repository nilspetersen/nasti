
#include <dlfcn.h>
#include <dirent.h>
#include <cstdlib>

#include "NastiTorsionForce.h"
#include "NastiTorsionForceProxy.h"
#include "NastiAngleForce.h"
#include "NastiAngleForceProxy.h"
#include "openmm/serialization/SerializationProxy.h"

extern "C" void __attribute__((constructor)) registerNastiSerializationProxies();

using namespace Nasti;
using namespace OpenMM;

extern "C" void registerNastiSerializationProxies() {
    SerializationProxy::registerProxy(typeid(NastiTorsionForce), new NastiTorsionForceProxy());
    SerializationProxy::registerProxy(typeid(NastiAngleForce), new NastiAngleForceProxy());
}
