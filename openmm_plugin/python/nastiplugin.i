%module nastiplugin

%import(module="simtk.openmm") "swig/OpenMMSwigHeaders.i"
%include "swig/typemaps.i"

/*
 * The following lines are needed to handle std::vector.
 * Similar lines may be needed for vectors of vectors or
 * for other STL types like maps.
 */

%include "std_vector.i"
namespace std {
  %template(vectord) vector<double>;
  %template(vectori) vector<int>;
};

%{
#include "NastiTorsionForce.h"
#include "NastiAngleForce.h"
#include "OpenMM.h"
#include "OpenMMAmoeba.h"
#include "OpenMMDrude.h"
#include "openmm/RPMDIntegrator.h"
#include "openmm/RPMDMonteCarloBarostat.h"
%}

%pythoncode %{
import simtk.openmm as mm
import simtk.unit as unit
%}

/*
 * Add units to function outputs.
*/
%pythonappend Nasti::NastiTorsionForce::getTorsionParameters(int index,
							     int& particle1,
							     int& particle2,
							     int& particle3,
							     int& particle4,
							     double& phase,
							     double& forceConstant,
							     double& margin) const %{
    val[4] = unit.Quantity(val[4], unit.radian)
    val[5] = unit.Quantity(val[5], unit.kilojoule_per_mole)
    val[6] = unit.Quantity(val[6], unit.radian)		
%}


namespace Nasti {

class NastiTorsionForce : public OpenMM::Force {
public:
    NastiTorsionForce();

    int getNumTorsions() const;

    int addTorsion(int particle1,
		   int particle2,
		   int particle3,
		   int particle4,
		   double phase,
		   double forceConstant,
		   double margin);

    void setTorsionParameters(int index,
			      int particle1,
			      int particle2,
			      int particle3,
			      int particle4,
			      double phase,
			      double forceConstant,
			      double margin);

    void updateParametersInContext(OpenMM::Context& context);

    /*
     * The reference parameters to this function are output values.
     * Marking them as such will cause swig to return a tuple.
    */
    %apply int& OUTPUT {int& particle1};
    %apply int& OUTPUT {int& particle2};
    %apply int& OUTPUT {int& particle3};
    %apply int& OUTPUT {int& particle4};
    %apply double& OUTPUT {double& phase};
    %apply double& OUTPUT {double& forceConstant};
    %apply double& OUTPUT {double& margin};
    void getTorsionParameters(int index,
			      int &particle1,
			      int &particle2,
			      int &particle3,
			      int &particle4,
			      double &phase,
			      double &forceConstant,
			      double &margin) const;
    %clear int& particle1;
    %clear int& particle2;
    %clear int& particle3;
    %clear int& particle4;
    %clear double& phase;
    %clear double& forceConstant;
    %clear double& margin;
};


class NastiAngleForce : public OpenMM::Force{
public:
    NastiAngleForce(const std::vector<double>& constCoeffs,
		    const std::vector<double>& linCoeffs,
		    const std::vector<double>& quadCoeffs,
		    const std::vector<double>& intervalBorders);

    int getNumAngles() const;
    
    int addAngle(int particle1,
		 int particle2,
		 int particle3);
    
    void setAngleParameters(int index,
			    int particle1,
			    int particle2,
			    int particle3);

    void updateParametersInContext(OpenMM::Context& context);

    
    %apply int& OUTPUT {int& particle1};
    %apply int& OUTPUT {int& particle2};
    %apply int& OUTPUT {int& particle3};
    void getAngleParameters(int index,
			    int &particle1,
			    int &particle2,
			    int &particle3) const;
    %clear int& particle1;
    %clear int& particle2;
    %clear int& particle3;

    const std::vector<double>& getConstantCoefficients() const;
    const std::vector<double>& getLinearCoefficients() const;
    const std::vector<double>& getQuadraticCoefficients() const;
    const std::vector<double>& getIntervalBorders() const;
};

} /* namespace Nasti */
