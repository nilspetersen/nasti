from distutils.core import setup
from distutils.extension import Extension
import os
import sys
import platform

openmm_dir = '@OPENMM_DIR@'

nastiplugin_header_dir = '@NASTIPLUGIN_HEADER_DIR@'
nastiplugin_library_dir = '@NASTIPLUGIN_LIBRARY_DIR@'

# setup extra compile and link arguments on Mac
extra_compile_args = []
extra_link_args = []

extra_link_args += ['-Wl,-rpath,' + openmm_dir+'/lib']

extension = Extension(name='_nastiplugin',
                      sources=['NastiPluginWrapper.cpp'],
                      libraries=['OpenMM', 'NastiPlugin'],
                      include_dirs=[os.path.join(openmm_dir, 'include'),
                                    nastiplugin_header_dir],
                      library_dirs=[os.path.join(openmm_dir, 'lib'),
                                    nastiplugin_library_dir],
                      extra_compile_args=extra_compile_args,
                      extra_link_args=extra_link_args)

setup(name='nastiplugin',
      version='1.0',
      py_modules=['nastiplugin'],
      ext_modules=[extension])
