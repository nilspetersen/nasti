
#include "NastiTorsionForce.h"
#include "internal/NastiTorsionForceImpl.h"
#include "openmm/OpenMMException.h"
#include "openmm/internal/AssertionUtilities.h"

using namespace Nasti;
using namespace OpenMM;
using namespace std;

NastiTorsionForce::NastiTorsionForce() {
}

int NastiTorsionForce::addTorsion(int particle1,
				  int particle2,
				  int particle3,
				  int particle4,
				  double phase,
				  double forceConstant,
				  double margin) {
    
    torsions.push_back(NastiTorsionInfo(particle1,
					particle2,
					particle3,
					particle4,
					phase,
					forceConstant,
					margin));
    return torsions.size() - 1;
}

void NastiTorsionForce::getTorsionParameters(int index,
					     int& particle1,
					     int& particle2,
					     int& particle3,
					     int& particle4,
					     double& phase,
					     double& forceConstant,
					     double& margin) const {
    ASSERT_VALID_INDEX(index, torsions);
    particle1 = torsions[index].particle1;
    particle2 = torsions[index].particle2;
    particle3 = torsions[index].particle3;
    particle4 = torsions[index].particle4;

    phase = torsions[index].phase;
    forceConstant = torsions[index].forceConstant;
    margin = torsions[index].margin;
}

void NastiTorsionForce::setTorsionParameters(int index,
					     int particle1,
					     int particle2,
					     int particle3,
					     int particle4,
					     double phase,
					     double forceConstant,
					     double margin) {
    ASSERT_VALID_INDEX(index, torsions);
    torsions[index].particle1 = particle1;
    torsions[index].particle2 = particle2;
    torsions[index].particle3 = particle3;
    torsions[index].particle4 = particle4;

    torsions[index].phase = phase;
    torsions[index].forceConstant = forceConstant;
    torsions[index].margin = margin;
}

ForceImpl* NastiTorsionForce::createImpl() const {
    return new NastiTorsionForceImpl(*this);
}

void NastiTorsionForce::updateParametersInContext(Context& context) {
    dynamic_cast<NastiTorsionForceImpl&>(getImplInContext(context)).updateParametersInContext(getContextImpl(context));
}
