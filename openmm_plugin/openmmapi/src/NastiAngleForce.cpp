
#include "NastiAngleForce.h"
#include "internal/NastiAngleForceImpl.h"
#include "openmm/OpenMMException.h"
#include "openmm/internal/AssertionUtilities.h"

using namespace Nasti;
using namespace OpenMM;
using namespace std;

NastiAngleForce::NastiAngleForce(const vector<double>& constCoeffs,
				 const vector<double>& linCoeffs,
				 const vector<double>& quadCoeffs,
				 const vector<double>& intervalBorders)
    :constCoeffs(constCoeffs),
     linCoeffs(linCoeffs),
     quadCoeffs(quadCoeffs),
     intervalBorders(intervalBorders)
{
    if(intervalBorders.size() != constCoeffs.size() ||
       intervalBorders.size() != linCoeffs.size() ||
       intervalBorders.size() != quadCoeffs.size()) {
	OpenMMException("NastiAngleForce parameter lists have unequal length");
    }
    this->intervalBorders[0] = 0.0;
}

int NastiAngleForce::addAngle(int particle1,
			      int particle2,
			      int particle3) {
    
    angles.push_back(NastiAngleInfo(particle1,
				    particle2,
				    particle3));
    return angles.size() - 1;
}

void NastiAngleForce::getAngleParameters(int index,
					 int& particle1,
					 int& particle2,
					 int& particle3) const {
    ASSERT_VALID_INDEX(index, angles);
    particle1 = angles[index].particle1;
    particle2 = angles[index].particle2;
    particle3 = angles[index].particle3;
}

void NastiAngleForce::setAngleParameters(int index,
					 int particle1,
					 int particle2,
					 int particle3) {
    ASSERT_VALID_INDEX(index, angles);
    angles[index].particle1 = particle1;
    angles[index].particle2 = particle2;
    angles[index].particle3 = particle3;
}

ForceImpl* NastiAngleForce::createImpl() const {
    return new NastiAngleForceImpl(*this);
}

void NastiAngleForce::updateParametersInContext(Context& context) {
    dynamic_cast<NastiAngleForceImpl&>(getImplInContext(context)).updateParametersInContext(getContextImpl(context));
}
