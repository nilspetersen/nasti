
#include "internal/NastiAngleForceImpl.h"
#include "NastiKernels.h"
#include "openmm/OpenMMException.h"
#include "openmm/internal/ContextImpl.h"
#include <cmath>
#include <map>
#include <set>
#include <sstream>

using namespace Nasti;
using namespace OpenMM;
using namespace std;

NastiAngleForceImpl::NastiAngleForceImpl(const NastiAngleForce& owner)
    :owner(owner){
}

NastiAngleForceImpl::~NastiAngleForceImpl() {
}

void NastiAngleForceImpl::initialize(ContextImpl& context) {
    kernel = context.getPlatform().createKernel(CalcNastiAngleForceKernel::Name(), context);
    kernel.getAs<CalcNastiAngleForceKernel>().initialize(context.getSystem(), owner);
}

double NastiAngleForceImpl::calcForcesAndEnergy(ContextImpl& context,
						  bool includeForces,
						  bool includeEnergy,
						  int groups) {
    if ((groups&(1<<owner.getForceGroup())) != 0)
        return kernel.getAs<CalcNastiAngleForceKernel>().execute(context, includeForces, includeEnergy);
    return 0.0;
}

std::vector<std::string> NastiAngleForceImpl::getKernelNames() {
    std::vector<std::string> names;
    names.push_back(CalcNastiAngleForceKernel::Name());
    return names;
}

void NastiAngleForceImpl::updateParametersInContext(ContextImpl& context) {
    kernel.getAs<CalcNastiAngleForceKernel>().copyParametersToContext(context, owner);
}
