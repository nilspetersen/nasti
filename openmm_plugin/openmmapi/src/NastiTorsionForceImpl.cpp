
#include "internal/NastiTorsionForceImpl.h"
#include "NastiKernels.h"
#include "openmm/OpenMMException.h"
#include "openmm/internal/ContextImpl.h"
#include <cmath>
#include <map>
#include <set>
#include <sstream>

using namespace Nasti;
using namespace OpenMM;
using namespace std;

NastiTorsionForceImpl::NastiTorsionForceImpl(const NastiTorsionForce& owner)
    :owner(owner){
}

NastiTorsionForceImpl::~NastiTorsionForceImpl() {
}

void NastiTorsionForceImpl::initialize(ContextImpl& context) {
    kernel = context.getPlatform().createKernel(CalcNastiTorsionForceKernel::Name(), context);
    kernel.getAs<CalcNastiTorsionForceKernel>().initialize(context.getSystem(), owner);
}

double NastiTorsionForceImpl::calcForcesAndEnergy(ContextImpl& context,
						  bool includeForces,
						  bool includeEnergy,
						  int groups) {
    if ((groups&(1<<owner.getForceGroup())) != 0)
        return kernel.getAs<CalcNastiTorsionForceKernel>().execute(context, includeForces, includeEnergy);
    return 0.0;
}

std::vector<std::string> NastiTorsionForceImpl::getKernelNames() {
    std::vector<std::string> names;
    names.push_back(CalcNastiTorsionForceKernel::Name());
    return names;
}

// vector<pair<int, int> > NastiTorsionForceImpl::getBondedParticles() const {
//     int numBonds = owner.getNumBonds();
//     vector<pair<int, int> > bonds(numBonds);
//     for (int i = 0; i < numBonds; i++) {
//         double length, k;
//         owner.getBondParameters(i, bonds[i].first, bonds[i].second, length, k);
//     }
//     return bonds;
// }

void NastiTorsionForceImpl::updateParametersInContext(ContextImpl& context) {
    kernel.getAs<CalcNastiTorsionForceKernel>().copyParametersToContext(context, owner);
}
