#ifndef OPENMM_NASTIDIHEDRALFORCEIMPL_H_
#define OPENMM_NASTIDIHEDRALFORCEIMPL_H_

#include "NastiTorsionForce.h"
#include "openmm/internal/ForceImpl.h"
#include "openmm/Kernel.h"
#include <utility>
#include <set>
#include <string>

namespace Nasti {

class System;

/**
 * This is the internal implementation of NastiTorsionForce.
 */

class NastiTorsionForceImpl : public OpenMM::ForceImpl {
public:

    NastiTorsionForceImpl(const NastiTorsionForce& owner);

    ~NastiTorsionForceImpl();

    void initialize(OpenMM::ContextImpl& context);

    const NastiTorsionForce& getOwner() const {
        return owner;
    }

    void updateContextState(OpenMM::ContextImpl& context) {
        // This force field doesn't update the state directly.
    }

    double calcForcesAndEnergy(OpenMM::ContextImpl& context,
			       bool includeForces,
			       bool includeEnergy,
			       int groups);

    std::map<std::string, double> getDefaultParameters() {
        return std::map<std::string, double>(); // This force field doesn't define any parameters.
    }
    
    std::vector<std::string> getKernelNames();

    // std::vector<std::pair<int, int> > getBondedParticles() const; // change this - can I avoid it ???

    void updateParametersInContext(OpenMM::ContextImpl& context);

private:

    const NastiTorsionForce& owner;

    OpenMM::Kernel kernel;
};

} // namespace Nasti

#endif /*OPENMM_NASTIDIHEDRALFORCEIMPL_H_*/
