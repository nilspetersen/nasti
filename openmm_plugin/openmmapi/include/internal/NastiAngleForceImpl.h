#ifndef OPENMM_NASTIANGLEFORCEIMPL_H_
#define OPENMM_NASTIANGLEFORCEIMPL_H_

#include "NastiAngleForce.h"
#include "openmm/internal/ForceImpl.h"
#include "openmm/Kernel.h"
#include <utility>
#include <set>
#include <string>

namespace Nasti {

class System;

/**
 * This is the internal implementation of NastiAngleForce.
 */

class NastiAngleForceImpl : public OpenMM::ForceImpl {
public:

    NastiAngleForceImpl(const NastiAngleForce& owner);

    ~NastiAngleForceImpl();

    void initialize(OpenMM::ContextImpl& context);

    const NastiAngleForce& getOwner() const {
        return owner;
    }

    void updateContextState(OpenMM::ContextImpl& context) {
        // This force field doesn't update the state directly.
    }

    double calcForcesAndEnergy(OpenMM::ContextImpl& context,
			       bool includeForces,
			       bool includeEnergy,
			       int groups);

    std::map<std::string, double> getDefaultParameters() {
        return std::map<std::string, double>(); // This force field doesn't define any parameters.
    }
    
    std::vector<std::string> getKernelNames();

    void updateParametersInContext(OpenMM::ContextImpl& context);

private:

    const NastiAngleForce& owner;

    OpenMM::Kernel kernel;
};

} // namespace Nasti

#endif /*OPENMM_NASTIANGLEFORCEIMPL_H_*/
