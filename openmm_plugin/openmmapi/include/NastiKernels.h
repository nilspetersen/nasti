#ifndef NASTI_KERNELS_H_
#define NASTI_KERNELS_H_


#include "NastiTorsionForce.h"
#include "NastiAngleForce.h"
#include "openmm/KernelImpl.h"
#include "openmm/Platform.h"
#include "openmm/System.h"
#include <string>

namespace Nasti {

/**
 * This kernel is invoked by NastiTorsionForce to calculate the torsion forces 
 * acting on the system and the energy of the system.
 */

class CalcNastiTorsionForceKernel : public OpenMM::KernelImpl {
public:
    
    static std::string Name() {
        return "CalcNastiTorsionForce";
    }
    
    CalcNastiTorsionForceKernel(std::string name,
				const OpenMM::Platform& platform)
	:OpenMM::KernelImpl(name, platform)
	{}
    
    /**
     * Initialize the kernel.
     * 
     * @param system     the System this kernel will be applied to
     * @param force      the NastiTorsionForce this kernel will be used for
     */

    virtual void initialize(const OpenMM::System& system,
			    const NastiTorsionForce& force) = 0;
    
    /**
     * Execute the kernel to calculate the forces and/or energy.
     *
     * @param context        the context in which to execute this kernel
     * @param includeForces  true if forces should be calculated
     * @param includeEnergy  true if the energy should be calculated
     * @return the potential energy due to the force
     */

    virtual double execute(OpenMM::ContextImpl& context,
			   bool includeForces,
			   bool includeEnergy) = 0;

   /**
     * Copy changed parameters over to a context.
     *
     * @param context    the context to copy parameters to
     * @param force      the NastiTorsionForce to copy the parameters from
     */

    virtual void copyParametersToContext(OpenMM::ContextImpl& context,
					 const NastiTorsionForce& force) = 0;
};



/**
 * This kernel is invoked by NastiAngleForce to calculate the angle forces 
 * acting on the system and the energy of the system.
 */

class CalcNastiAngleForceKernel : public OpenMM::KernelImpl {
public:
    
    static std::string Name() {
        return "CalcNastiAngleForce";
    }
    
    CalcNastiAngleForceKernel(std::string name,
			      const OpenMM::Platform& platform)
	:OpenMM::KernelImpl(name, platform)
	{}
    
    /**
     * Initialize the kernel.
     * 
     * @param system     the System this kernel will be applied to
     * @param force      the NastiAngleForce this kernel will be used for
     */

    virtual void initialize(const OpenMM::System& system,
			    const NastiAngleForce& force) = 0;
    
    /**
     * Execute the kernel to calculate the forces and/or energy.
     *
     * @param context        the context in which to execute this kernel
     * @param includeForces  true if forces should be calculated
     * @param includeEnergy  true if the energy should be calculated
     * @return the potential energy due to the force
     */

    virtual double execute(OpenMM::ContextImpl& context,
			   bool includeForces,
			   bool includeEnergy) = 0;

   /**
     * Copy changed parameters over to a context.
     *
     * @param context    the context to copy parameters to
     * @param force      the NastiAngleForce to copy the parameters from
     */

    virtual void copyParametersToContext(OpenMM::ContextImpl& context,
					 const NastiAngleForce& force) = 0;
};


} // namespace Nasti

#endif /*NASTI_KERNELS_H_*/
