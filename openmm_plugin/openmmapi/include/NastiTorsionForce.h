#ifndef OPENMM_NASTITORSIONFORCE_
#define OPENMM_NASTITORSIONFORCE_

#include "openmm/Context.h"
#include "openmm/Force.h"
#include <vector>

namespace Nasti {

class NastiTorsionForce : public OpenMM::Force{
public:

    NastiTorsionForce();

    int getNumTorsions() const {
	return torsions.size();
    }

    int addTorsion(int particle1,
		   int particle2,
		   int particle3,
		   int particle4,
		   double phase,
		   double forceConstant,
		   double margin);

    void getTorsionParameters(int index,
			      int &particle1,
			      int &particle2,
			      int &particle3,
			      int &particle4,
			      double &phase,
			      double &forceConstant,
			      double &margin) const;

    void setTorsionParameters(int index,
			      int particle1,
			      int particle2,
			      int particle3,
			      int particle4,
			      double phase,
			      double forceConstant,
			      double margin);

    void updateParametersInContext(OpenMM::Context& context);

    bool usesPeriodicBoundaryConditions() const {
        return false;
    }
    
protected:
    OpenMM::ForceImpl* createImpl() const;

private:
    class NastiTorsionInfo;
    std::vector<NastiTorsionInfo> torsions;
};


/**
 * This is an internal class used to record information about a torsion angle.
 * @private
 */

class NastiTorsionForce::NastiTorsionInfo {

public:

    int particle1,
	particle2,
	particle3,
	particle4;

    double phase, forceConstant, margin;

    NastiTorsionInfo()
	:particle1(-1),
	 particle2(-1),
	 particle3(-1),
	 particle4(-1),
	 phase(0),
	 forceConstant(0),
	 margin(0)
	{}
    
    NastiTorsionInfo(int particle1,
		     int particle2,
		     int particle3,
		     int particle4,
		     double phase,
		     double forceConstant,
		     double margin)
	:particle1(particle1),
	 particle2(particle2),
	 particle3(particle3),
	 particle4(particle4),
	 phase(phase),
	 forceConstant(forceConstant),
	 margin(margin)
	{}
};


} /* namespace Nasti */

#endif	/* OPENMM_NASTITORSIONFORCE_ */
