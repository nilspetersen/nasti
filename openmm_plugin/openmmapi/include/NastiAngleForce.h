#ifndef OPENMM_NASTIANGLEFORCE_
#define OPENMM_NASTIANGLEFORCE_

#include "openmm/Context.h"
#include "openmm/Force.h"
#include <vector>

namespace Nasti {

class NastiAngleForce : public OpenMM::Force{
public:

    NastiAngleForce(const std::vector<double>& constCoeffs,
		    const std::vector<double>& linCoeffs,
		    const std::vector<double>& quadCoeffs,
		    const std::vector<double>& intervalBorders);

    int getNumAngles() const {
	return angles.size();
    }

    int addAngle(int particle1,
		 int particle2,
		 int particle3);

    void getAngleParameters(int index,
			    int &particle1,
			    int &particle2,
			    int &particle3) const;

    void setAngleParameters(int index,
			    int particle1,
			    int particle2,
			    int particle3);

    void updateParametersInContext(OpenMM::Context& context);

    bool usesPeriodicBoundaryConditions() const {
        return false;
    }

    const std::vector<double>& getConstantCoefficients() const { return constCoeffs; }
    const std::vector<double>& getLinearCoefficients() const { return linCoeffs; }
    const std::vector<double>& getQuadraticCoefficients() const { return quadCoeffs; }
    const std::vector<double>& getIntervalBorders() const { return intervalBorders; }
    
protected:
    OpenMM::ForceImpl* createImpl() const;

private:
    std::vector<double> constCoeffs;
    std::vector<double> linCoeffs;
    std::vector<double> quadCoeffs;
    std::vector<double> intervalBorders;
    class NastiAngleInfo;
    std::vector<NastiAngleInfo> angles;
};

/**
 * This is an internal class used to record information about a angle angle.
 * @private
 */

class NastiAngleForce::NastiAngleInfo {

public:

    int particle1,
	particle2,
	particle3;

    NastiAngleInfo()
	:particle1(-1),
	 particle2(-1),
	 particle3(-1)
	{}
    
    NastiAngleInfo(int particle1,
		   int particle2,
		   int particle3)
	:particle1(particle1),
	 particle2(particle2),
	 particle3(particle3)
	{}
};

} /* namespace Nasti */

#endif	/* OPENMM_NASTIANGLEFORCE_ */
