#include "openmm/VerletIntegrator.h"
#include "openmm/internal/AssertionUtilities.h"
#include "NastiTorsionForce.h"
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include "ReferenceNastiKernels.h"
#include "ReferenceNastiForceFuncs.h"

using namespace Nasti;
using namespace OpenMM;
using namespace std;


extern "C" OPENMM_EXPORT void registerNastiReferenceKernelFactories();

class TestNastiTorsionForce {
public:
    void enableAngfunc() {_computeAngfunc=true;}
    void disableAngfunc() {_computeAngfunc=false;}
    
    // setup
    TestNastiTorsionForce();
    ~TestNastiTorsionForce();

    // tests
    // change parameters
    void testChangingReferenceDihedral();
    void testChangingForceConstant();
    void testChangingCoordinates();
    void testForceDihedral();

    void testEnergies();
    void testAngleDerivative() const;
    void testAngleDerivativeRandomized() const;

    void testDihedralDerivativeRotate() const;
    
    void testForceAngle();
    void testForceRandomized();

    void testForceWithExamples();
    void testForceWithRandomExamples();

private:
    void reset();
    
    void setForce(double refDih,
		  double forceConstant);

    void setPositions(const vector<Vec3>& pos);
    
    vector<Vec3> makeQuadrupel(double alpha,
			       double beta,
			       double dihedral) const;
    
    double getAngleFuncFromAngle(double theta) const;
    
    double getEnergyFromAnglesAndDihedral(double alpha,
					  double beta,
					  double dihedral) const;
    double getEnergy() const;
    vector<Vec3> getForces() const;

    double multiDot(const vector<Vec3>& a,
		    const vector<Vec3>& b) const;

    double forceTest(const vector<Vec3>& startPos,
		     const vector<Vec3>& endPos,
		     int nSteps);

    void assertForce(const vector<Vec3>& pos,
		     const vector<Vec3>& force);

    void examplesForceTest(const vector<vector<Vec3>>& coords,
			   const vector<vector<Vec3>>& expectedForces);

    // derivative tests

    double angDeriv(Vec3* derivatives,
		    const vector<Vec3>& coords) const;

    double angFuncDeriv(Vec3* derivatives,
			const vector<Vec3>& coords) const;

    double dihDeriv(Vec3* derivatives,
		    const vector<Vec3>& coords) const;

    void derivativeTest(const vector<Vec3>& startPos,
			const vector<Vec3>& endPos,
			int nSteps,
			double (TestNastiTorsionForce::*derivativeMethod)(Vec3*, const vector<Vec3>&) const) const;

    void angleDerivativeTest(const vector<Vec3>& startPos,
			     const vector<Vec3>& endPos,
			     int nSteps) const;

    void dihedralDerivativeTest(const vector<Vec3>& startPos,
				const vector<Vec3>& endPos,
				int nSteps) const;

    void randomAngleDerivativeTest() const;

    void dihedralDerivativeTestMoveOneParticle(int nSteps,
					       int particleI,
					       const vector<Vec3>& pos,
					       const vector<Vec3>& flexPos) const;

    void testDihedralDerivativeRotateEnds() const;
    void testDihedralDerivativeRotateInterior() const;

    
    // energy tests
    
    void energyTest(double alpha,
		    double beta,
		    double dih);

    void energiesTest(const vector<double>& angleList,
		      const vector<double>& dihList);

    // variables
    
    Platform& _platform;
    System _system;
    VerletIntegrator _integ;
    Context *_context;
    NastiTorsionForce *_force;
    
    vector<Vec3> _positions;

    // default parameters
    double _refDih;
    double _forceConstant;
    double _margin;

    bool _computeAngfunc; 	// used to test angle derivative and anglefunc derivative
};


void
TestNastiTorsionForce::setForce(double refDih,
				double forceConstant) {

    _force->setTorsionParameters(0,
				 0, 1, 2, 3,
				 refDih,
				 forceConstant,
				 _margin);

    _force->updateParametersInContext(*_context);
}

TestNastiTorsionForce::TestNastiTorsionForce()
    :_platform(Platform::getPlatformByName("Reference")),
     _integ(1.0),
     _positions(4),
     _refDih(0.0),
     _forceConstant(-1.0),
     _margin(1.0),
     _computeAngfunc(true)
{
    // Create a system with one Torsion Angle.
    for(int i; i < 4; ++i) {
	_system.addParticle(1.0);
    }

    _force = new NastiTorsionForce();
    _force->addTorsion(0, 1, 2, 3,
		       _refDih,
		       _forceConstant,
		       _margin);
    _system.addForce(_force);

    _context = new Context(_system, _integ, _platform);
    
    reset();
}

TestNastiTorsionForce::~TestNastiTorsionForce() {
    delete _context;
}

void
TestNastiTorsionForce::reset() {
    _positions[0] = Vec3(-1, -1, 0);
    _positions[1] = Vec3(-1, 0, 0);
    _positions[2] = Vec3(0, 0, 0);
    _positions[3] = Vec3(0, -1, 0);
    
    setPositions(_positions);    

    setForce(_refDih, _forceConstant);
}

void
TestNastiTorsionForce::setPositions(const vector<Vec3>& pos) {
    _context->setPositions(pos);
}

double
TestNastiTorsionForce::getEnergy() const {
    return _context->getState(State::Energy).getPotentialEnergy();
}

double
TestNastiTorsionForce::getAngleFuncFromAngle(double theta) const {
    if(theta < _margin) {
	return 0.5 * cos(M_PI * (theta / _margin - 1.0)) + 0.5;
    } else if(theta > M_PI - _margin) {
	return 0.5 * cos(M_PI * ((theta - M_PI) / _margin + 1.0)) + 0.5;
    }
    return 1.0;
}

double
TestNastiTorsionForce::getEnergyFromAnglesAndDihedral(double alpha,
						      double beta,
						      double dihedral) const {
    double fAlpha = getAngleFuncFromAngle(alpha);
    double fBeta = getAngleFuncFromAngle(beta);
    
    double dihEnergy = _forceConstant * (1 + cos(dihedral - _refDih));
    double energy = fAlpha * fBeta * (dihEnergy - 2*_forceConstant) + 2*_forceConstant;
    return energy;
}

vector<Vec3>
TestNastiTorsionForce::getForces() const {
    State state = _context->getState(State::Forces);
    vector<Vec3> forces = state.getForces();
    return forces;
}

double
TestNastiTorsionForce::multiDot(const vector<Vec3>& a,
				const vector<Vec3>& b) const {
    assert(a.size() == b.size());
    double dotsum = 0.0;
    for(int i=0; i < a.size(); ++i) {
	dotsum += a[i].dot(b[i]);
    }
    return dotsum;
}

void
TestNastiTorsionForce::testChangingReferenceDihedral() {

    reset();
    
    // change the dihedral optimum
    double refDihedrals[4] = {0.0,
			      M_PI / 2.0,
			      M_PI,
			      3.0 / 2.0 * M_PI};

    double expectedEnergies[4] = {2*_forceConstant,
				  _forceConstant,
				  0,
				  _forceConstant};

    for(int i=0; i < 4; ++i) {
	setForce(refDihedrals[i], _forceConstant);	
	double energy = getEnergy();
	ASSERT_EQUAL_TOL(expectedEnergies[i], energy, 1e-5);
    }
}

void
TestNastiTorsionForce::testChangingForceConstant() {

    reset();
    
    // change force constant
    for(int i=0; i < 10; ++i) {
    	double forceConstantTemp = i * _forceConstant;
	setForce(_refDih, forceConstantTemp);
	double expectedEnergy = forceConstantTemp * 2;
	double energy = getEnergy();
    	ASSERT_EQUAL_TOL(expectedEnergy, energy, 1e-5);
    }
}

void
TestNastiTorsionForce::testChangingCoordinates() {

    reset();
    
    Vec3 pos_particle4[4] = {Vec3(0, -1, 0),
    			     Vec3(0, 0, -1),
    			     Vec3(0, 1, 0),
    			     Vec3(0, 0, 1)};

    double expectedEnergies[4] = {2*_forceConstant,
				  _forceConstant,
				  0,
				  _forceConstant};

    vector<Vec3> pos = _positions;
    
    for(int i=0; i < 4; ++i) {
    	pos[3] = pos_particle4[i];
    	setPositions(pos);
    	double energy = getEnergy();
	ASSERT_EQUAL_TOL(expectedEnergies[i],
    			 energy,
    			 1e-5);
    }
}

double
TestNastiTorsionForce::forceTest(const vector<Vec3>& startPos,
				 const vector<Vec3>& endPos,
				 int nSteps) {

    assert(startPos.size() == endPos.size());
    
    int nParticles = startPos.size();
    
    vector<Vec3> dr(nParticles);
    for(int i=0; i < nParticles; ++i) {
	dr[i] = (endPos[i] - startPos[i]) / double(nSteps); 
    }
    
    vector<Vec3> pos = startPos;
    setPositions(pos);
    
    double energyStart = getEnergy();
    double dotsum = 0.0;	// sum of dot(dr, gradient)
    
    for(int ii=0; ii < nSteps; ++ii) {
	vector<Vec3> forces = getForces();
	dotsum -= multiDot(forces, dr);
	for(int i=0; i < nParticles; ++i) {
	    pos[i] += dr[i];
	}
	setPositions(pos);
    }
    double energyFinal = getEnergy();
    double energyDiff = energyFinal - energyStart;

    ASSERT_EQUAL_TOL(energyDiff, dotsum, 1e-5);
}

void
TestNastiTorsionForce::assertForce(const vector<Vec3>& pos,
				   const vector<Vec3>& expectedForces) {

    assert(pos.size() == expectedForces.size());

    setPositions(pos);
    vector<Vec3> forces = getForces();

    for(int i=0; i < forces.size(); ++i) {
	for(int ii=0; i < 3; ++i) {
	    double fCalc = forces[i][ii];
	    double fExpected = expectedForces[i][ii];
	    ASSERT_EQUAL_TOL(fExpected, fCalc, 1e-4);
	}
    }
}

vector<Vec3>
TestNastiTorsionForce::makeQuadrupel(double alpha,
				     double beta,
				     double dihedral) const {
    // alpha angle
    double x1 = cos(alpha);
    double y1 = sin(alpha);
    // Last particle, determines beta and the dihedral
    // get x
    double x4 = -cos(beta);
    // distance to the axis
    double d = sin(beta);
    // get y and z
    double y4 = d * cos(dihedral);
    double z4 = d * sin(dihedral);
    
    vector<Vec3> coords(4);
    coords[0] = Vec3(-1+x1, y1, 0);
    coords[1] = Vec3(-1, 0, 0);
    coords[2] = Vec3(0, 0, 0);
    coords[3] = Vec3(x4, y4, z4);

    return coords;
}

static Vec3
randVec3() {
    Vec3 v;
    for(int i=0; i < 3; ++i) {
	v[i] = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
    }
    return v;
}

static vector<Vec3>
randCoords(int len) {
    vector<Vec3> coords(len);
    for(int i=0; i < len; ++i) {
	coords[i] = randVec3();
    }
    return coords;
}
    

void
TestNastiTorsionForce::energyTest(double alpha,
				  double beta,
				  double dih) {
    double expectedEnergy = getEnergyFromAnglesAndDihedral(alpha, beta, dih);
    vector<Vec3> pos = makeQuadrupel(alpha, beta, dih);
    setPositions(pos);
    double energy = getEnergy();
    ASSERT_EQUAL_TOL(expectedEnergy, energy, 1e-5);    
}

static vector<double>
linspace(double from, double to, int n) {
    vector<double> ret(n);
    double d = (to - from) / double(n-1);
    for(int i=0; i<n; ++i) {
	ret[i] = from + i*d;
    }
    return ret;
}

void
TestNastiTorsionForce::energiesTest(const vector<double>& angleList,
				    const vector<double>& dihList) {
    for(int i=0; i < angleList.size(); ++i) {
	for(int j=0; j < angleList.size(); ++j) {
	    for(int k=0; k < dihList.size(); ++k) {
		energyTest(angleList[i], angleList[j], dihList[k]);
	    }
	}
    }
}

static void
append(vector<double>& target,
       const vector<double>& source) {
    target.insert(target.end(), source.begin(), source.end());
}

void
TestNastiTorsionForce::testEnergies() {

    reset();

    vector<double> dihList = linspace(0.0, 2 * M_PI, 10);
    vector<double> angListOne = linspace(0.0, _margin, 10);
    vector<double> angListTwo = linspace(_margin, M_PI-_margin, 10);
    vector<double> angListThree = linspace(M_PI-_margin, M_PI, 10);

    vector<double> angList;
    append(angList, angListOne);
    append(angList, angListTwo);
    append(angList, angListThree);

    energiesTest(angList, dihList);
}

double
TestNastiTorsionForce::angDeriv(Vec3* derivatives,
				const vector<Vec3>& coords) const {
    assert(coords.size() == 3);
    return NastiKernelStaticExport::angleDerivative(derivatives,
						    coords[0],
						    coords[1],
						    coords[2]);
}

double
TestNastiTorsionForce::angFuncDeriv(Vec3* derivatives,
				    const vector<Vec3>& coords) const {
    assert(coords.size() == 3);
    return NastiKernelStaticExport::angleFuncDerivative(derivatives,
							_margin,
							coords[0],
							coords[1],
							coords[2]);
}

double
TestNastiTorsionForce::dihDeriv(Vec3* derivatives,
				const vector<Vec3>& coords) const {
    assert(coords.size() == 4);
    return NastiKernelStaticExport::nastiDihedralDerivative(derivatives,
							    _refDih,
							    _forceConstant,
							    coords[0],
							    coords[1],
							    coords[2],
							    coords[3]);
}

void
TestNastiTorsionForce::derivativeTest(const vector<Vec3>& startPos,
				      const vector<Vec3>& endPos,
				      int nSteps,
				      double (TestNastiTorsionForce::*derivativeMethod)(
					  Vec3*, const vector<Vec3>&) const) const {
    int nParticles = startPos.size();
    vector<Vec3> pos = startPos;
    vector<Vec3> dr(nParticles);
    for(int i=0; i < nParticles; ++i) {
	dr[i] = (endPos[i] - startPos[i]) / double(nSteps);
    }
    double dotsum = 0.0;
    double valStart = (this->*derivativeMethod)(NULL, pos);
    
    for(int i=0; i < nSteps; ++i) {
	vector<Vec3> derivatives(nParticles);
	
	(this->*derivativeMethod)(&derivatives[0], pos);
	
    	dotsum += multiDot(derivatives, dr);
	
	for(int ii=0; ii < nParticles; ++ii) {
	    pos[ii] += dr[ii];
	}
    }
    double valEnd = (this->*derivativeMethod)(NULL, pos);

    double angleDiff = valEnd - valStart;
    ASSERT_EQUAL_TOL(angleDiff, dotsum, 1e-5);
}

void
TestNastiTorsionForce::angleDerivativeTest(const vector<Vec3>& startPos,
					   const vector<Vec3>& endPos,
					   int nSteps) const {
    if(_computeAngfunc) {
	derivativeTest(startPos,
		       endPos,
		       nSteps,
		       &TestNastiTorsionForce::angFuncDeriv);
    } else {
	derivativeTest(startPos,
		       endPos,
		       nSteps,
		       &TestNastiTorsionForce::angDeriv);
    }
}

void
TestNastiTorsionForce::dihedralDerivativeTest(const vector<Vec3>& startPos,
					      const vector<Vec3>& endPos,
					      int nSteps) const {
    derivativeTest(startPos,
		   endPos,
		   nSteps,
		   &TestNastiTorsionForce::dihDeriv);
}


void
TestNastiTorsionForce::testAngleDerivative() const {

    int nSteps = 1000000;

    vector<Vec3> startPos(3);
    vector<Vec3> endPos(3);
    
    // test 1: move k
    // x,y dimensions
    startPos[0] = Vec3(-1, 0, 0);
    startPos[1] = Vec3(0, 0, 0);
    startPos[2] = Vec3(0, 1, 0);
    
    endPos[0] = Vec3(-1, 0, 0);
    endPos[1] = Vec3(0, 0, 0);
    endPos[2] = Vec3(1, 0, 0);

    angleDerivativeTest(startPos, endPos, nSteps);

    // y,z dimensions
    startPos[0] = Vec3(0, -1, 0);
    startPos[1] = Vec3(0, 0, 0);
    startPos[2] = Vec3(0, 0, 1);
    
    endPos[0] = Vec3(0, -1, 0);
    endPos[1] = Vec3(0, 0, 0);
    endPos[2] = Vec3(0, 1, 0);

    angleDerivativeTest(startPos, endPos, nSteps);
    
    // test 2 move i
    // x,y dimensions
    startPos[0] = Vec3(0, -1, 0);
    startPos[1] = Vec3(0, 0, 0);
    startPos[2] = Vec3(1, 0, 0);

    endPos[0] = Vec3(-1, 0, 0);
    endPos[1] = Vec3(0, 0, 0);
    endPos[2] = Vec3(1, 0, 0);

    angleDerivativeTest(startPos, endPos, nSteps);

    // y,z dimensions
    startPos[0] = Vec3(0, 0, -1);
    startPos[1] = Vec3(0, 0, 0);
    startPos[2] = Vec3(0, 1, 0);

    endPos[0] = Vec3(0, -1, 0);
    endPos[1] = Vec3(0, 0, 0);
    endPos[2] = Vec3(0, 1, 0);

    angleDerivativeTest(startPos, endPos, nSteps);

    // test 2 move j
    // x dimension
    startPos[0] = Vec3(0, -1, 0);
    startPos[1] = Vec3(1, 0, 0);
    startPos[2] = Vec3(0, 1, 0);

    endPos[0] = Vec3(0, -1, 0);
    endPos[1] = Vec3(0, 0, 0);
    endPos[2] = Vec3(0, 1, 0);

    angleDerivativeTest(startPos, endPos, nSteps);

    // y dimension
    startPos[0] = Vec3(-1, 0, 0);
    startPos[1] = Vec3(0, 1, 0);
    startPos[2] = Vec3(1, 0, 0);

    endPos[0] = Vec3(-1, 0, 0);
    endPos[1] = Vec3(0, 0, 0);
    endPos[2] = Vec3(1, 0, 0);

    angleDerivativeTest(startPos, endPos, nSteps);

    // z dimension
    startPos[0] = Vec3(0, -1, 0);
    startPos[1] = Vec3(0, 0, 1);
    startPos[2] = Vec3(0, 1, 0);

    endPos[0] = Vec3(0, -1, 0);
    endPos[1] = Vec3(0, 0, 0);
    endPos[2] = Vec3(0, 1, 0);

    angleDerivativeTest(startPos, endPos, nSteps);
}

void
TestNastiTorsionForce::dihedralDerivativeTestMoveOneParticle(int nSteps,
							     int particleI,
							     const vector<Vec3>& pos,
							     const vector<Vec3>& flexPos) const {
    vector<Vec3> startPos = pos;
    vector<Vec3> endPos = pos;
    
    for(int i=0; i < flexPos.size()-1; ++i) {
	startPos[particleI] = flexPos[i];
	endPos[particleI] = flexPos[i+1];
	dihedralDerivativeTest(startPos, endPos, nSteps);
    }
}

void
TestNastiTorsionForce::testDihedralDerivativeRotateEnds() const {

    int nSteps = 100000;
    
    vector<Vec3> startPosA(4), startPosB(4);
    startPosA[0] = startPosB[3] = Vec3(-1, -1, 0);
    startPosA[1] = startPosB[2] = Vec3(-1, 0, 0);
    startPosA[2] = startPosB[1] = Vec3(0, 0, 0);

    vector<Vec3> posParticleFW(4), posParticleBW(4);
    posParticleFW[0] = posParticleBW[3] = Vec3(0, -1, 0);
    posParticleFW[1] = posParticleBW[2] = Vec3(0, 0, -1);
    posParticleFW[2] = posParticleBW[1] = Vec3(0, 1, 0);
    posParticleFW[3] = posParticleBW[0] = Vec3(0, 0, 1);

    dihedralDerivativeTestMoveOneParticle(nSteps,
					  3,
					  startPosA,
					  posParticleFW);
    
    dihedralDerivativeTestMoveOneParticle(nSteps,
					  3,
					  startPosA,
					  posParticleBW);
    
    dihedralDerivativeTestMoveOneParticle(nSteps,
					  0,
					  startPosB,
					  posParticleFW);

    dihedralDerivativeTestMoveOneParticle(nSteps,
					  0,
					  startPosB,
					  posParticleBW);
}

void
TestNastiTorsionForce::testDihedralDerivativeRotateInterior() const {

    int nSteps = 100000;
    
    vector<Vec3> startPosA(4), startPosB(4);
    startPosA[0] = startPosB[3] = Vec3(-1, -1, 0);
    startPosA[1] = startPosB[2] = Vec3(-1, 0, 0);
    startPosA[3] = startPosB[0] = Vec3(0, 1, 0);

    vector<Vec3> posParticleFW(4), posParticleBW(4);
    posParticleFW[0] = posParticleBW[3] = Vec3(0, 0, 0);
    posParticleFW[1] = posParticleBW[2] = Vec3(0, 1, 1);
    posParticleFW[2] = posParticleBW[1] = Vec3(0, 2, 0);
    posParticleFW[3] = posParticleBW[0] = Vec3(0, 1, -1);

    dihedralDerivativeTestMoveOneParticle(nSteps,
					  2,
					  startPosA,
					  posParticleFW);
    
    dihedralDerivativeTestMoveOneParticle(nSteps,
					  2,
					  startPosA,
					  posParticleBW);
    
    dihedralDerivativeTestMoveOneParticle(nSteps,
					  1,
					  startPosB,
					  posParticleFW);

    dihedralDerivativeTestMoveOneParticle(nSteps,
					  1,
					  startPosB,
					  posParticleBW);
}

void
TestNastiTorsionForce::testDihedralDerivativeRotate() const {
    testDihedralDerivativeRotateEnds();
    testDihedralDerivativeRotateInterior();    
}

void
TestNastiTorsionForce::randomAngleDerivativeTest()  const {
    int nSteps = 1000000;
    vector<Vec3> startPos = randCoords(3);
    vector<Vec3> endPos = randCoords(3);
    angleDerivativeTest(startPos, endPos, nSteps);
}

void
TestNastiTorsionForce::testAngleDerivativeRandomized()  const {
    int nTrials = 10;
    for(int i=0; i < nTrials; ++i) {
	randomAngleDerivativeTest();
    }
}


void
TestNastiTorsionForce::testForceDihedral() {

    reset();
    
    int nSteps = 100000;

    vector<Vec3> startPos(4);
    startPos[0] = Vec3(-1, -1, 0);
    startPos[1] = Vec3(-1, 0, 0);
    startPos[2] = Vec3(0, 0, 0);

    vector<Vec3> endPos = startPos;
    
    Vec3 posParticle4[4] = {Vec3(0, -1, 0),
			    Vec3(0, 0, -1),
			    Vec3(0, 1, 0),
			    Vec3(0, 0, 1)};

    for(int i=0; i < 3; ++i) {
	startPos[3] = posParticle4[i];
	endPos[3] = posParticle4[i+1];
	forceTest(startPos, endPos, nSteps);
    }
}

void
TestNastiTorsionForce::testForceAngle() {

    reset();
    
    int nSteps = 100000;

    vector<Vec3> startPos(4);
    startPos[0] = Vec3(-1, -1, 0);
    startPos[1] = Vec3(-1, 0, 0);
    startPos[2] = Vec3(0, 0, 0);

    vector<Vec3> endPos = startPos;
    
    Vec3 posParticle4[4] = {Vec3(0, -1, 0),
			    Vec3(1, 0, 0),
			    Vec3(0, 1, 0),
			    Vec3(-1, 0, 0)};

    for(int i=0; i < 3; ++i) {
	startPos[3] = posParticle4[i];
	endPos[3] = posParticle4[i+1];
	forceTest(startPos, endPos, nSteps);
    }
}

void
TestNastiTorsionForce::testForceRandomized() {
    reset();

    int nTrials = 1;
    int nSteps = 1000000;
    
    for(int i=0; i < nTrials; ++i) {
	vector<Vec3> startPos = randCoords(4);
	vector<Vec3> endPos = randCoords(4);
	
	forceTest(startPos, endPos, nSteps);
    }
}

void
TestNastiTorsionForce::examplesForceTest(const vector<vector<Vec3>>& coords,
					 const vector<vector<Vec3>>& expectedForces) {
    assert(coords.size() == expectedForces.size());
    for(int i=0; i < coords.size(); ++i) {
	const vector<Vec3> &c = coords[i];
	const vector<Vec3> &f = expectedForces[i];
	assertForce(c, f);
    }
}

void
TestNastiTorsionForce::testForceWithExamples() {

    reset();

    vector<vector<Vec3>> coords {
	{// 1
	    {1.000000, 0.000000, 1.000000},
	    {1.000000, 0.000000, 0.000000},
	    {2.000000, 0.000000, 0.000000},
	    {2.000000, 1.000000, 0.000000}
	},
	{     // 2
	    {0.000000, 0.000000, 0.100000},
	    {1.000000, 0.000000, 0.000000},
	    {2.000000, 0.000000, 0.000000},
	    {2.000000, 1.000000, 0.000000}
	},
	{     // 3
	    {1.000000, 0.000000, 1.000000},
	    {1.000000, 0.000000, 0.000000},
	    {2.000000, 0.000000, 0.000000},
	    {3.000000, 0.100000, 0.000000}
	},
	{     // 4
	    {0.000000, 0.000000, 0.100000},
	    {1.000000, 0.000000, 0.000000},
	    {2.000000, 0.000000, 0.000000},
	    {3.000000, 0.100000, 0.000000}}
    };

    vector<vector<Vec3>> expectedForces {
	{// 1
	    {0.000000, 1.000000, 0.000000},
	    {0.000000, -1.000000, 0.000000},
	    {0.000000, 0.000000, -1.000000},
	    {0.000000, 0.000000, 1.000000}
	},
	{     // 2
	    {-0.047906, 0.243112, -0.479057},
	    {0.047906, -0.486225, 0.962905},
	    {0.000000, 0.243112, -0.508159},
	    {0.000000, 0.000000, 0.024311}
	},
	{     // 3
	    {0.000000, 0.024311, 0.000000},
	    {0.000000, -0.508159, 0.243112},
	    {-0.047906, 0.962905, -0.486225},
	    {0.047906, -0.479057, 0.243112}
	},
	{     // 4
	    {-0.001165, 0.005910, -0.011646},
	    {0.001165, -0.023584, 0.029320},
	    {-0.001165, 0.029320, -0.023584},
	    {0.001165, -0.011646, 0.005910}}
    };
    
    examplesForceTest(coords, expectedForces);
}


void
TestNastiTorsionForce::testForceWithRandomExamples() {

    reset();

    vector<vector<Vec3>> coords {
	{// 1
	    {0.417022, 0.720324, 0.000114},
	    {0.302333, 0.146756, 0.092339},
	    {0.186260, 0.345561, 0.396767},
	    {0.538817, 0.419195, 0.685220}
	},
	{     // 2
	    {0.204452, 0.878117, 0.027388},
	    {0.670468, 0.417305, 0.558690},
	    {0.140387, 0.198101, 0.800745},
	    {0.968262, 0.313424, 0.692323}
	},
	{     // 3
	    {0.876389, 0.894607, 0.085044},
	    {0.039055, 0.169830, 0.878143},
	    {0.098347, 0.421108, 0.957890},
	    {0.533165, 0.691877, 0.315516}
	},
	{     // 4
	    {0.686501, 0.834626, 0.018288},
	    {0.750144, 0.988861, 0.748166},
	    {0.280444, 0.789279, 0.103226},
	    {0.447894, 0.908596, 0.293614}
	},
	{     // 5
	    {0.287775, 0.130029, 0.019367},
	    {0.678836, 0.211628, 0.265547},
	    {0.491573, 0.053363, 0.574118},
	    {0.146729, 0.589306, 0.699758}
	},
	{     // 6
	    {0.102334, 0.414056, 0.694400},
	    {0.414179, 0.049953, 0.535896},
	    {0.663795, 0.514889, 0.944595},
	    {0.586555, 0.903402, 0.137475}
	},
	{     // 7
	    {0.139276, 0.807391, 0.397677},
	    {0.165354, 0.927509, 0.347766},
	    {0.750812, 0.725998, 0.883306},
	    {0.623672, 0.750942, 0.348898}
	},
	{     // 8
	    {0.269928, 0.895886, 0.428091},
	    {0.964840, 0.663441, 0.621696},
	    {0.114746, 0.949489, 0.449912},
	    {0.578390, 0.408137, 0.237027}
	},
	{     // 9
	    {0.903380, 0.573679, 0.002870},
	    {0.617145, 0.326645, 0.527058},
	    {0.885942, 0.357270, 0.908535},
	    {0.623360, 0.015821, 0.929437}
	},
	{     // 10
	    {0.690897, 0.997323, 0.172341},
	    {0.137136, 0.932595, 0.696818},
	    {0.066000, 0.755463, 0.753876},
	    {0.923025, 0.711525, 0.124271}
	},
	{     // 11
	    {0.019880, 0.026211, 0.028306},
	    {0.246211, 0.860028, 0.538831},
	    {0.552822, 0.842031, 0.124173},
	    {0.279184, 0.585759, 0.969596}
	},
	{     // 12
	    {0.561030, 0.018647, 0.800633},
	    {0.232974, 0.807105, 0.387861},
	    {0.863542, 0.747122, 0.556240},
	    {0.136455, 0.059918, 0.121343}
	},
	{     // 13
	    {0.044552, 0.107494, 0.225709},
	    {0.712989, 0.559717, 0.012556},
	    {0.071974, 0.967276, 0.568100},
	    {0.203293, 0.252326, 0.743826}
	},
	{     // 14
	    {0.195429, 0.581359, 0.970020},
	    {0.846829, 0.239848, 0.493770},
	    {0.619956, 0.828981, 0.156791},
	    {0.018576, 0.070022, 0.486345}
	},
	{     // 15
	    {0.606329, 0.568851, 0.317362},
	    {0.988616, 0.579745, 0.380141},
	    {0.550948, 0.745334, 0.669233},
	    {0.264920, 0.066335, 0.370084}
	},
	{     // 16
	    {0.629718, 0.210174, 0.752756},
	    {0.066536, 0.260315, 0.804755},
	    {0.193434, 0.639461, 0.524670},
	    {0.924808, 0.263297, 0.065961}
	},
	{     // 17
	    {0.735066, 0.772178, 0.907816},
	    {0.931972, 0.013952, 0.234362},
	    {0.616778, 0.949016, 0.950176},
	    {0.556653, 0.915606, 0.641566}
	},
	{     // 18
	    {0.390008, 0.485991, 0.604310},
	    {0.549548, 0.926181, 0.918733},
	    {0.394876, 0.963263, 0.173956},
	    {0.126330, 0.135079, 0.505662}
	},
	{     // 19
	    {0.021525, 0.947970, 0.827115},
	    {0.015019, 0.176196, 0.332064},
	    {0.130997, 0.809491, 0.344737},
	    {0.940107, 0.582014, 0.878832}
	},
	{     // 20
	    {0.844734, 0.905392, 0.459880},
	    {0.546347, 0.798604, 0.285719},
	    {0.490254, 0.599110, 0.015533},
	    {0.593481, 0.433676, 0.807361}
	},
	{     // 21
	    {0.315245, 0.892889, 0.577857},
	    {0.184010, 0.787929, 0.612031},
	    {0.053909, 0.420194, 0.679069},
	    {0.918602, 0.000402, 0.976759}
	},
	{     // 22
	    {0.376580, 0.973784, 0.604716},
	    {0.828846, 0.574712, 0.628076},
	    {0.285576, 0.586833, 0.750022},
	    {0.858314, 0.755082, 0.698057}
	},
	{     // 23
	    {0.864479, 0.322681, 0.670789},
	    {0.450874, 0.382103, 0.410811},
	    {0.401480, 0.317384, 0.621919},
	    {0.430247, 0.973802, 0.677801}
	},
	{     // 24
	    {0.198570, 0.426701, 0.343346},
	    {0.797639, 0.879998, 0.903842},
	    {0.662720, 0.270208, 0.252367},
	    {0.854898, 0.527715, 0.802161}
	},
	{     // 25
	    {0.572489, 0.733143, 0.519012},
	    {0.770884, 0.568858, 0.465710},
	    {0.342689, 0.068209, 0.377924},
	    {0.079626, 0.982817, 0.181613}
	},
	{     // 26
	    {0.811859, 0.874962, 0.688413},
	    {0.569494, 0.160971, 0.466880},
	    {0.345172, 0.225040, 0.592512},
	    {0.312270, 0.916306, 0.909636}
	},
	{     // 27
	    {0.257118, 0.110891, 0.192963},
	    {0.499584, 0.728586, 0.208194},
	    {0.248034, 0.851672, 0.415849},
	    {0.616685, 0.233666, 0.101967}
	},
	{     // 28
	    {0.515857, 0.477141, 0.152672},
	    {0.621806, 0.544010, 0.654137},
	    {0.144546, 0.751528, 0.222049},
	    {0.519352, 0.785296, 0.022330}
	},
	{     // 29
	    {0.324362, 0.872922, 0.844710},
	    {0.538441, 0.866608, 0.949806},
	    {0.826407, 0.854115, 0.098743},
	    {0.651304, 0.703517, 0.610241}
	},
	{     // 30
	    {0.799615, 0.034571, 0.770239},
	    {0.731729, 0.259698, 0.257069},
	    {0.632303, 0.345297, 0.796589},
	    {0.446146, 0.782749, 0.990472}
	},
	{     // 31
	    {0.300248, 0.143006, 0.901308},
	    {0.541559, 0.974740, 0.636604},
	    {0.993913, 0.546071, 0.526426},
	    {0.135428, 0.355705, 0.026219}
	},
	{     // 32
	    {0.160395, 0.745637, 0.030400},
	    {0.366543, 0.862346, 0.692678},
	    {0.690942, 0.188637, 0.441904},
	    {0.581577, 0.989752, 0.203906}
	},
	{     // 33
	    {0.247733, 0.262173, 0.750172},
	    {0.456975, 0.056929, 0.508516},
	    {0.211960, 0.798604, 0.297331},
	    {0.027606, 0.593432, 0.843840}
	},
	{     // 34
	    {0.381016, 0.749858, 0.511141},
	    {0.540952, 0.959434, 0.803961},
	    {0.032323, 0.709387, 0.465001},
	    {0.947549, 0.221433, 0.267072}
	},
	{     // 35
	    {0.081474, 0.428619, 0.109019},
	    {0.633787, 0.802963, 0.696800},
	    {0.766211, 0.342454, 0.845851},
	    {0.428769, 0.824010, 0.626496}
	},
	{     // 36
	    {0.143423, 0.078387, 0.018333},
	    {0.066725, 0.458584, 0.113342},
	    {0.027783, 0.754861, 0.394850},
	    {0.746938, 0.452405, 0.450087}
	},
	{     // 37
	    {0.478073, 0.474004, 0.803163},
	    {0.402393, 0.904686, 0.037061},
	    {0.773874, 0.125641, 0.618514},
	    {0.010364, 0.538627, 0.003018}
	},
	{     // 38
	    {0.951194, 0.905402, 0.795967},
	    {0.915274, 0.145558, 0.157730},
	    {0.187632, 0.622496, 0.905809},
	    {0.989955, 0.711122, 0.731800}
	},
	{     // 39
	    {0.909293, 0.400874, 0.249851},
	    {0.173430, 0.119457, 0.812611},
	    {0.146792, 0.264297, 0.819089},
	    {0.310587, 0.982417, 0.266639}
	},
	{     // 40
	    {0.533653, 0.314467, 0.910773},
	    {0.366557, 0.433592, 0.512293},
	    {0.938886, 0.030949, 0.716879},
	    {0.891019, 0.027287, 0.522051}
	},
	{     // 41
	    {0.325990, 0.859489, 0.558517},
	    {0.690228, 0.452853, 0.628309},
	    {0.290097, 0.009349, 0.576756},
	    {0.311444, 0.517268, 0.916406}
	},
	{     // 42
	    {0.426475, 0.247396, 0.371294},
	    {0.931861, 0.936868, 0.844330},
	    {0.920207, 0.227900, 0.087482},
	    {0.227310, 0.314377, 0.174766}
	},
	{     // 43
	    {0.607094, 0.413586, 0.816352},
	    {0.185130, 0.701877, 0.240356},
	    {0.574219, 0.348988, 0.056964},
	    {0.228814, 0.664103, 0.497250}
	},
	{     // 44
	    {0.519016, 0.174720, 0.570716},
	    {0.996753, 0.816835, 0.594373},
	    {0.975989, 0.901563, 0.595608},
	    {0.032426, 0.093577, 0.065372}
	},
	{     // 45
	    {0.451733, 0.375435, 0.975350},
	    {0.167983, 0.972788, 0.767475},
	    {0.824238, 0.632616, 0.668733},
	    {0.476882, 0.013136, 0.353006}
	},
	{     // 46
	    {0.492072, 0.730091, 0.468628},
	    {0.457405, 0.137663, 0.010889},
	    {0.758278, 0.319953, 0.984383},
	    {0.220234, 0.338708, 0.523896}
	},
	{     // 47
	    {0.754891, 0.463858, 0.124823},
	    {0.312501, 0.504519, 0.673849},
	    {0.770150, 0.130336, 0.022915},
	    {0.519082, 0.809989, 0.012604}
	},
	{     // 48
	    {0.672470, 0.686808, 0.449247},
	    {0.914789, 0.644361, 0.005240},
	    {0.484428, 0.859318, 0.830400},
	    {0.649154, 0.673698, 0.578500}
	},
	{     // 49
	    {0.274120, 0.560530, 0.671730},
	    {0.352430, 0.855828, 0.195037},
	    {0.747321, 0.289603, 0.773799},
	    {0.427737, 0.807698, 0.353535}
	},
	{     // 50
	    {0.213693, 0.767285, 0.308642},
	    {0.733245, 0.744473, 0.221397},
	    {0.214112, 0.198948, 0.142518},
	    {0.377083, 0.026628, 0.110920}
	},
	{     // 51
	    {0.674564, 0.799777, 0.080530},
	    {0.231702, 0.207626, 0.917334},
	    {0.711315, 0.553885, 0.304518},
	    {0.834854, 0.435306, 0.923456}
	},
	{     // 52
	    {0.706052, 0.478031, 0.126210},
	    {0.976044, 0.159834, 0.202602},
	    {0.431182, 0.404202, 0.146751},
	    {0.729319, 0.188745, 0.643896}
	},
	{     // 53
	    {0.754306, 0.210732, 0.600954},
	    {0.748928, 0.638219, 0.597127},
	    {0.295482, 0.731606, 0.945308},
	    {0.425561, 0.782182, 0.056141}
	},
	{     // 54
	    {0.835272, 0.192250, 0.395097},
	    {0.300081, 0.080104, 0.904631},
	    {0.370154, 0.530697, 0.494116},
	    {0.132161, 0.206454, 0.076189}
	},
	{     // 55
	    {0.507922, 0.261550, 0.357062},
	    {0.108065, 0.787552, 0.106584},
	    {0.985709, 0.177161, 0.572405},
	    {0.044845, 0.787116, 0.189606}
	},
	{     // 56
	    {0.527904, 0.740078, 0.149931},
	    {0.551087, 0.216617, 0.759196},
	    {0.722915, 0.176549, 0.861967},
	    {0.019775, 0.860237, 0.558904}
	},
	{     // 57
	    {0.403220, 0.758747, 0.716929},
	    {0.987326, 0.278085, 0.003794},
	    {0.933903, 0.857897, 0.728851},
	    {0.516689, 0.706956, 0.780530}
	},
	{     // 58
	    {0.374876, 0.770323, 0.750624},
	    {0.613211, 0.401866, 0.697308},
	    {0.003113, 0.774897, 0.896417},
	    {0.239316, 0.120767, 0.220284}
	},
	{     // 59
	    {0.302097, 0.883029, 0.543166},
	    {0.286712, 0.138355, 0.290144},
	    {0.613871, 0.324139, 0.457360},
	    {0.444117, 0.828135, 0.426348}
	},
	{     // 60
	    {0.345699, 0.674972, 0.221482},
	    {0.467246, 0.314766, 0.626856},
	    {0.877360, 0.447689, 0.784457},
	    {0.456966, 0.656229, 0.131841}
	},
	{     // 61
	    {0.432982, 0.909312, 0.605479},
	    {0.766775, 0.504701, 0.498056},
	    {0.842900, 0.067807, 0.573272},
	    {0.942763, 0.517860, 0.194466}
	},
	{     // 62
	    {0.847939, 0.251639, 0.700726},
	    {0.540261, 0.948836, 0.624337},
	    {0.837978, 0.007933, 0.989340},
	    {0.077715, 0.322130, 0.946152}
	},
	{     // 63
	    {0.008939, 0.822730, 0.861212},
	    {0.439831, 0.255745, 0.802690},
	    {0.477862, 0.134339, 0.927849},
	    {0.895970, 0.491545, 0.856702}
	},
	{     // 64
	    {0.418578, 0.683465, 0.397991},
	    {0.505742, 0.189552, 0.964989},
	    {0.294216, 0.103460, 0.144315},
	    {0.014092, 0.715946, 0.564498}
	},
	{     // 65
	    {0.794578, 0.507080, 0.791821},
	    {0.695764, 0.777848, 0.406483},
	    {0.647771, 0.179794, 0.321820},
	    {0.172605, 0.408637, 0.241419}
	},
	{     // 66
	    {0.406922, 0.975222, 0.320319},
	    {0.982491, 0.636306, 0.375091},
	    {0.857484, 0.619587, 0.252033},
	    {0.792856, 0.432939, 0.357511}
	},
	{     // 67
	    {0.330277, 0.697369, 0.268650},
	    {0.808278, 0.295289, 0.544121},
	    {0.487921, 0.855356, 0.888386},
	    {0.184384, 0.585348, 0.898205}
	},
	{     // 68
	    {0.446117, 0.921868, 0.278991},
	    {0.608831, 0.682454, 0.228206},
	    {0.013768, 0.416724, 0.938482},
	    {0.343028, 0.779744, 0.174736}
	},
	{     // 69
	    {0.341953, 0.144598, 0.716771},
	    {0.699308, 0.688497, 0.253396},
	    {0.692360, 0.227298, 0.424649},
	    {0.371922, 0.355308, 0.057655}
	},
	{     // 70
	    {0.631647, 0.707317, 0.613589},
	    {0.648313, 0.169941, 0.149447},
	    {0.514175, 0.875333, 0.183953},
	    {0.462839, 0.428932, 0.497289}
	},
	{     // 71
	    {0.161511, 0.342441, 0.261880},
	    {0.844527, 0.800332, 0.426639},
	    {0.607015, 0.145466, 0.509613},
	    {0.296947, 0.859651, 0.671598}
	},
	{     // 72
	    {0.633474, 0.124751, 0.470588},
	    {0.986573, 0.948299, 0.645086},
	    {0.151725, 0.639127, 0.565662},
	    {0.468666, 0.428037, 0.599270}
	},
	{     // 73
	    {0.849970, 0.751121, 0.579361},
	    {0.924704, 0.064740, 0.991347},
	    {0.052995, 0.199496, 0.422753},
	    {0.107509, 0.623670, 0.047993}
	},
	{     // 74
	    {0.284624, 0.061037, 0.703519},
	    {0.668456, 0.378581, 0.188194},
	    {0.747005, 0.340379, 0.795301},
	    {0.487901, 0.525669, 0.028491}
	},
	{     // 75
	    {0.644232, 0.350657, 0.229205},
	    {0.433883, 0.382467, 0.469789},
	    {0.979483, 0.364378, 0.774410},
	    {0.552768, 0.889131, 0.354953}
	},
	{     // 76
	    {0.245519, 0.911019, 0.043534},
	    {0.950753, 0.556407, 0.376363},
	    {0.995052, 0.058363, 0.516706},
	    {0.031097, 0.571176, 0.180469}
	},
	{     // 77
	    {0.630959, 0.980924, 0.874903},
	    {0.451836, 0.708461, 0.777469},
	    {0.494843, 0.528533, 0.150784},
	    {0.369400, 0.142221, 0.726894}
	},
	{     // 78
	    {0.477013, 0.448879, 0.885998},
	    {0.527619, 0.409091, 0.268892},
	    {0.072012, 0.418136, 0.025753},
	    {0.291154, 0.503510, 0.965933}
	},
	{     // 79
	    {0.109383, 0.673041, 0.499932},
	    {0.777098, 0.143607, 0.083203},
	    {0.399219, 0.796962, 0.191676},
	    {0.767777, 0.290298, 0.216891}
	},
	{     // 80
	    {0.016716, 0.398659, 0.381081},
	    {0.659345, 0.070918, 0.152604},
	    {0.016576, 0.113796, 0.651789},
	    {0.402657, 0.321026, 0.557912}
	},
	{     // 81
	    {0.993460, 0.834487, 0.699623},
	    {0.918259, 0.039729, 0.070333},
	    {0.474006, 0.349167, 0.937252},
	    {0.489565, 0.539649, 0.895260}
	},
	{     // 82
	    {0.446635, 0.877034, 0.253582},
	    {0.273810, 0.328361, 0.547564},
	    {0.220129, 0.671429, 0.142793},
	    {0.094100, 0.870192, 0.236869}
	},
	{     // 83
	    {0.386004, 0.571542, 0.525802},
	    {0.076024, 0.874126, 0.951136},
	    {0.812507, 0.283802, 0.527847},
	    {0.339417, 0.554667, 0.974403}
	},
	{     // 84
	    {0.311703, 0.668797, 0.325967},
	    {0.774477, 0.325810, 0.889827},
	    {0.751708, 0.762632, 0.469479},
	    {0.210765, 0.041475, 0.321829}
	},
	{     // 85
	    {0.037113, 0.693855, 0.670350},
	    {0.430472, 0.767789, 0.536008},
	    {0.039860, 0.134793, 0.193416},
	    {0.335664, 0.052313, 0.605117}
	},
	{     // 86
	    {0.512061, 0.617461, 0.432356},
	    {0.847700, 0.454059, 0.015404},
	    {0.873068, 0.656202, 0.823003},
	    {0.951776, 0.050912, 0.235072}
	},
	{     // 87
	    {0.063343, 0.421658, 0.863829},
	    {0.081624, 0.473112, 0.125543},
	    {0.772886, 0.841422, 0.043291},
	    {0.486441, 0.239411, 0.952474}
	},
	{     // 88
	    {0.943893, 0.613934, 0.973487},
	    {0.344861, 0.897851, 0.434595},
	    {0.235815, 0.940828, 0.684218},
	    {0.064912, 0.870425, 0.701380}
	},
	{     // 89
	    {0.604927, 0.732375, 0.253439},
	    {0.600489, 0.814619, 0.054114},
	    {0.130511, 0.842446, 0.618346},
	    {0.531288, 0.248291, 0.295079}
	},
	{     // 90
	    {0.872686, 0.421666, 0.064430},
	    {0.896985, 0.203381, 0.826228},
	    {0.881771, 0.486751, 0.598465},
	    {0.527267, 0.624821, 0.855042}
	},
	{     // 91
	    {0.282139, 0.883756, 0.567690},
	    {0.115103, 0.227001, 0.595982},
	    {0.239446, 0.131416, 0.161848},
	    {0.844873, 0.602184, 0.963567}
	},
	{     // 92
	    {0.345679, 0.595625, 0.598985},
	    {0.615704, 0.059177, 0.750317},
	    {0.948209, 0.534679, 0.192556},
	    {0.752926, 0.007319, 0.328256}
	},
	{     // 93
	    {0.917606, 0.588367, 0.855190},
	    {0.604672, 0.822578, 0.879484},
	    {0.320986, 0.122955, 0.721303},
	    {0.440347, 0.126736, 0.589824}
	},
	{     // 94
	    {0.036068, 0.200182, 0.788301},
	    {0.012097, 0.303346, 0.021376},
	    {0.997485, 0.582030, 0.293376},
	    {0.928949, 0.507119, 0.454692}
	},
	{     // 95
	    {0.587872, 0.264138, 0.305288},
	    {0.371655, 0.244448, 0.584547},
	    {0.695846, 0.071947, 0.971083},
	    {0.753071, 0.806163, 0.751640}
	},
	{     // 96
	    {0.080061, 0.481696, 0.445673},
	    {0.672473, 0.448737, 0.704313},
	    {0.681645, 0.697149, 0.618601},
	    {0.150981, 0.760802, 0.781046}
	},
	{     // 97
	    {0.904103, 0.233769, 0.176679},
	    {0.391099, 0.320565, 0.815477},
	    {0.613526, 0.760002, 0.427665},
	    {0.099441, 0.115035, 0.373407}
	},
	{     // 98
	    {0.193848, 0.820746, 0.599636},
	    {0.688845, 0.491110, 0.088174},
	    {0.235078, 0.461602, 0.198592},
	    {0.025476, 0.729398, 0.724917}
	},
	{     // 99
	    {0.330392, 0.843494, 0.428442},
	    {0.864686, 0.657065, 0.581036},
	    {0.200727, 0.529547, 0.894432},
	    {0.304994, 0.870946, 0.908745}
	},
	{     // 100
	    {0.329845, 0.683074, 0.899221},
	    {0.075697, 0.878588, 0.190839},
	    {0.849769, 0.667119, 0.344339},
	    {0.151376, 0.635371, 0.847788}
	}
    };

    vector<vector<Vec3>> expectedForces {
	{// 1
	    {1.598984, -0.200635, 0.740682},
	    {-0.595998, 0.929614, -0.834318},
	    {-1.489865, -2.691716, 1.189749},
	    {0.486879, 1.962736, -1.096113}
	},
	{     // 2
	    {-0.000080, 0.006384, 0.005607},
	    {2.407375, -2.507399, 3.001273},
	    {-1.774183, 0.346882, -0.463877},
	    {-0.633113, 2.154134, -2.543002}
	},
	{     // 3
	    {-0.065355, 0.028930, -0.042563},
	    {-0.024274, 0.013503, -0.024498},
	    {-0.015304, -0.000711, 0.013619},
	    {0.104933, -0.041721, 0.053443}
	},
	{     // 4
	    {-0.154561, 0.121198, -0.012134},
	    {-0.131988, 0.449603, 0.044185},
	    {-0.372729, 0.713650, -0.257169},
	    {0.659279, -1.284451, 0.225118}
	},
	{     // 5
	    {-0.727225, 1.890823, 0.528470},
	    {0.548547, -1.825499, -0.603399},
	    {1.277252, 0.426150, 0.993697},
	    {-1.098574, -0.491475, -0.918768}
	},
	{     // 6
	    {0.544309, 1.210267, -1.709241},
	    {0.040050, -0.911056, 1.011958},
	    {0.455918, -0.630167, 0.438424},
	    {-1.040277, 0.330956, 0.258860}
	},
	{     // 7
	    {2.862052, -2.277536, -3.985805},
	    {-1.046651, 2.679391, 2.152398},
	    {1.747455, 0.455808, 1.025807},
	    {-3.562856, -0.857663, 0.807599}
	},
	{     // 8
	    {-0.070155, -0.114095, 0.114826},
	    {0.014504, 0.008737, -0.014868},
	    {0.046421, 0.094805, -0.093223},
	    {0.009231, 0.010554, -0.006735}
	},
	{     // 9
	    {0.325130, -0.737253, -0.169908},
	    {-0.781423, 1.356508, 0.441708},
	    {-0.395802, 0.069396, 0.273320},
	    {0.852094, -0.688651, -0.545120}
	},
	{     // 10
	    {0.151765, -0.009718, 0.159039},
	    {-0.263853, 0.036595, -0.215344},
	    {0.199748, -0.023715, 0.175408},
	    {-0.087661, -0.003162, -0.119104}
	},
	{     // 11
	    {0.290283, -0.215954, 0.224017},
	    {2.072982, -0.352472, 1.548126},
	    {-0.827262, 0.462301, -1.307153},
	    {-1.536002, 0.106125, -0.464989}
	},
	{     // 12
	    {0.147814, -0.280630, -0.653524},
	    {0.191491, 0.024646, -0.708337},
	    {-0.009650, 0.339874, 0.678166},
	    {-0.329655, -0.083889, 0.683695}
	},
	{     // 13
	    {0.425653, -0.295490, 0.707918},
	    {-0.036245, 0.259686, -0.232333},
	    {0.296127, 0.307201, 0.116316},
	    {-0.685534, -0.271397, -0.591901}
	},
	{     // 14
	    {-0.607761, -0.503153, -0.470473},
	    {0.360082, -0.068080, -0.361451},
	    {0.281595, 0.182033, -0.002508},
	    {-0.033916, 0.389200, 0.834432}
	},
	{     // 15
	    {-0.175094, -1.596932, 1.343335},
	    {0.403451, 0.738651, -0.506000},
	    {0.018827, 0.498485, -0.257024},
	    {-0.247184, 0.359795, -0.580310}
	},
	{     // 16
	    {-0.121454, -0.543755, -0.791100},
	    {0.204249, 0.452237, 0.704725},
	    {-0.437075, -0.094502, -0.325951},
	    {0.354281, 0.186020, 0.412326}
	},
	{     // 17
	    {-0.576562, 0.371969, -0.587369},
	    {0.094511, -0.109234, 0.031897},
	    {0.430713, -0.224482, 0.561333},
	    {0.051339, -0.038254, -0.005861}
	},
	{     // 18
	    {-0.379894, 0.081103, 0.079217},
	    {0.144508, -0.012957, -0.026940},
	    {0.045613, 0.010748, -0.008938},
	    {0.189773, -0.078894, -0.043339}
	},
	{     // 19
	    {1.495000, 0.387455, -0.623680},
	    {0.319057, -0.693889, -0.664715},
	    {-1.498533, 0.258623, 0.790040},
	    {-0.315524, 0.047810, 0.498356}
	},
	{     // 20
	    {-1.390978, -1.472571, 3.286058},
	    {1.551927, 1.664663, -3.461306},
	    {-1.631074, -0.860328, 0.227291},
	    {1.470125, 0.668235, -0.052043}
	},
	{     // 21
	    {-0.038449, 0.823429, 2.381369},
	    {0.186209, -1.056685, -3.374136},
	    {-0.192829, 0.286840, 1.199238},
	    {0.045068, -0.053584, -0.206470}
	},
	{     // 22
	    {0.002936, 0.023611, 0.346518},
	    {0.244646, 0.116925, 0.742497},
	    {-0.211888, -0.012196, -0.280065},
	    {-0.035694, -0.128341, -0.808950}
	},
	{     // 23
	    {-0.088540, 2.071445, 0.614321},
	    {-0.927367, -0.462788, -0.358858},
	    {-0.498372, -1.514585, -0.580930},
	    {1.514279, -0.094071, 0.325467}
	},
	{     // 24
	    {0.497796, -0.529479, -0.103841},
	    {1.176701, -0.613104, 0.826531},
	    {-0.388712, -0.425262, -0.437800},
	    {-1.285786, 1.567846, -0.284889}
	},
	{     // 25
	    {-0.133682, 0.438641, -1.849541},
	    {0.277015, -0.489334, 1.439510},
	    {0.048871, -0.014927, -0.153249},
	    {-0.192204, 0.065620, 0.563281}
	},
	{     // 26
	    {-0.007175, 0.007616, -0.016695},
	    {0.016175, -0.016574, 0.037334},
	    {-0.017412, 0.017430, -0.039978},
	    {0.008411, -0.008472, 0.019339}
	},
	{     // 27
	    {0.110432, -0.047339, 0.161836},
	    {0.236127, -0.071528, 0.328440},
	    {-0.173537, 0.079585, -0.209719},
	    {-0.173022, 0.039283, -0.280557}
	},
	{     // 28
	    {0.461647, 1.663888, -0.319411},
	    {-0.118538, -0.449482, 0.523671},
	    {-0.201027, 0.146749, 0.292523},
	    {-0.142082, -1.361155, -0.496783}
	},
	{     // 29
	    {-0.025010, -0.794661, 0.003202},
	    {0.677142, -0.629730, 0.238362},
	    {0.641159, -0.731713, -0.433642},
	    {-1.293291, 2.156104, 0.192077}
	},
	{     // 30
	    {-2.036223, 4.066354, 2.053277},
	    {0.235043, 0.171790, -3.057623},
	    {1.577506, -4.168284, 0.631961},
	    {0.223674, -0.069861, 0.372385}
	},
	{     // 31
	    {-0.406682, -0.184698, -0.951087},
	    {0.380361, 0.407917, -0.025449},
	    {0.337114, 0.292267, 0.246953},
	    {-0.310794, -0.515486, 0.729583}
	},
	{     // 32
	    {0.690717, 0.441583, -0.292818},
	    {0.454532, 0.168871, 0.134304},
	    {0.054415, -0.437218, 0.190366},
	    {-1.199664, -0.173236, -0.031853}
	},
	{     // 33
	    {-0.085151, -0.039557, -0.040132},
	    {0.048213, 0.022177, 0.021949},
	    {-0.013426, -0.006666, -0.007833},
	    {0.050363, 0.024045, 0.026016}
	},
	{     // 34
	    {-0.165825, -0.595993, 0.517135},
	    {0.082302, 0.247953, -0.135058},
	    {0.057919, 0.257268, -0.276694},
	    {0.025605, 0.090772, -0.105383}
	},
	{     // 35
	    {-0.098573, 0.001354, 0.091762},
	    {-0.475671, 0.026087, 0.503209},
	    {0.149370, -0.082449, -0.062132},
	    {0.424873, 0.055008, -0.532839}
	},
	{     // 36
	    {1.926518, 0.883627, -1.980778},
	    {-3.263505, -2.221322, 3.203705},
	    {1.537914, 1.742678, -1.621363},
	    {-0.200927, -0.404983, 0.398435}
	},
	{     // 37
	    {0.035160, -0.934013, -0.528550},
	    {-0.421326, -0.131624, -0.652501},
	    {0.232370, 0.309070, 0.864190},
	    {0.153796, 0.756566, 0.316861}
	},
	{     // 38
	    {0.045856, -0.105018, 0.122447},
	    {0.011300, -0.032008, 0.020509},
	    {-0.011609, 0.003632, -0.000889},
	    {-0.045547, 0.133394, -0.142067}
	},
	{     // 39
	    {-0.248521, -0.030488, -0.340212},
	    {2.166378, 0.310905, 1.956592},
	    {-2.417873, -0.441083, -1.973477},
	    {0.500017, 0.160667, 0.357097}
	},
	{     // 40
	    {2.556445, -2.385166, -1.785050},
	    {-2.054088, 1.921911, -0.532022},
	    {0.036462, 1.155276, 2.171682},
	    {-0.538819, -0.692021, 0.145390}
	},
	{     // 41
	    {-0.205899, 0.036283, 1.285955},
	    {0.437304, -0.369063, -0.219146},
	    {0.054804, -0.599601, 0.309503},
	    {-0.286209, 0.932381, -1.376313}
	},
	{     // 42
	    {-0.060240, 0.258928, -0.313041},
	    {0.038047, 0.005560, 0.065627},
	    {0.023696, -0.043032, 0.039945},
	    {-0.001504, -0.221456, 0.207470}
	},
	{     // 43
	    {-0.031383, -0.036941, 0.004501},
	    {-0.075895, -0.088691, 0.009641},
	    {0.023379, 0.022373, -0.006058},
	    {0.083899, 0.103259, -0.008084}
	},
	{     // 44
	    {0.063018, -0.023760, -0.627698},
	    {0.156864, 0.051457, 2.424051},
	    {-0.089914, 0.008327, -2.082527},
	    {-0.129968, -0.036024, 0.286175}
	},
	{     // 45
	    {0.034136, -0.544358, -1.610868},
	    {-0.200496, 0.566968, 0.427323},
	    {0.243618, 0.380945, 0.306744},
	    {-0.077258, -0.403555, 0.876802}
	},
	{     // 46
	    {-0.696221, -0.460983, 0.649354},
	    {-0.436381, -0.422306, -0.133908},
	    {0.274708, 0.066095, 0.453653},
	    {0.857894, 0.817193, -0.969100}
	},
	{     // 47
	    {-0.522227, -0.190316, -0.406701},
	    {-0.033639, 0.019354, 0.114167},
	    {0.322398, 0.082816, 0.167148},
	    {0.233468, 0.088147, 0.125385}
	},
	{     // 48
	    {0.061688, 0.016767, 0.032064},
	    {-0.014796, -0.009575, -0.009481},
	    {-0.010556, -0.008717, 0.002303},
	    {-0.036336, 0.001525, -0.024885}
	},
	{     // 49
	    {0.059180, 0.024571, 0.024943},
	    {0.149394, 0.238341, 0.089963},
	    {0.021524, 0.009809, 0.046325},
	    {-0.230098, -0.272720, -0.161231}
	},
	{     // 50
	    {-0.108649, -1.987243, -0.127422},
	    {1.104141, 1.071166, -0.088726},
	    {-1.009863, 1.031426, -0.487027},
	    {0.014371, -0.115349, 0.703174}
	},
	{     // 51
	    {1.237061, -0.495518, 0.304044},
	    {0.340467, -0.388352, 0.431174},
	    {-1.347894, 0.810581, -0.795094},
	    {-0.229634, 0.073290, 0.059876}
	},
	{     // 52
	    {-3.669066, -2.461920, 2.712852},
	    {1.959336, -0.766111, -0.157134},
	    {0.897905, 2.659382, -2.315312},
	    {0.811825, 0.568649, -0.240405}
	},
	{     // 53
	    {-1.265701, -0.030605, -1.640150},
	    {0.688358, 1.916326, 0.382479},
	    {-0.289838, -0.224706, 1.225286},
	    {0.867182, -1.661015, 0.032385}
	},
	{     // 54
	    {-1.220253, -0.250048, -1.336730},
	    {-0.206982, -0.065410, 0.746851},
	    {-0.050226, 0.899162, 0.978375},
	    {1.477460, -0.583704, -0.388496}
	},
	{     // 55
	    {0.011295, 0.018659, 0.021155},
	    {-0.019995, 0.021130, 0.047376},
	    {-0.000228, -0.016774, -0.009913},
	    {0.008929, -0.023015, -0.058617}
	},
	{     // 56
	    {-0.152984, 0.532642, 0.463450},
	    {-1.400491, 1.243214, 2.826266},
	    {1.428730, -1.405446, -2.164673},
	    {0.124745, -0.370411, -1.125044}
	},
	{     // 57
	    {0.056760, -0.157428, 0.152599},
	    {0.011473, -0.008480, -0.014897},
	    {0.022769, -0.029759, 0.025475},
	    {-0.091001, 0.195668, -0.163176}
	},
	{     // 58
	    {-3.916382, -2.521622, -0.080723},
	    {1.858549, -0.556109, -0.458643},
	    {1.272684, 2.706202, 0.624516},
	    {0.785149, 0.371528, -0.085151}
	},
	{     // 59
	    {0.119669, 0.115225, -0.346396},
	    {-0.026707, -0.008252, 0.045664},
	    {0.102192, -0.067439, -0.125012},
	    {-0.195154, -0.039534, 0.425745}
	},
	{     // 60
	    {0.158329, -0.210471, -0.234493},
	    {-0.041059, 0.023042, 0.087410},
	    {0.036249, -0.033842, -0.022515},
	    {-0.153519, 0.221272, 0.169598}
	},
	{     // 61
	    {1.643854, 2.016454, -2.487106},
	    {-2.001685, -2.163240, 1.996654},
	    {2.544273, -1.722551, -1.154075},
	    {-2.186441, 1.869338, 1.644527}
	},
	{     // 62
	    {-1.477791, -0.434170, 1.989579},
	    {0.139758, 0.579980, -0.522337},
	    {1.301377, -0.198602, -1.206032},
	    {0.036656, 0.052792, -0.261210}
	},
	{     // 63
	    {1.445297, 1.117152, -0.181840},
	    {-5.552706, -3.269480, -0.657866},
	    {4.831224, 1.047927, -0.451520},
	    {-0.723816, 1.104401, 1.291226}
	},
	{     // 64
	    {-1.073922, -0.081460, 0.094132},
	    {0.126198, -0.289165, 0.189021},
	    {0.275113, -0.089007, -0.061572},
	    {0.672611, 0.459633, -0.221581}
	},
	{     // 65
	    {-1.946626, 0.077810, 0.553858},
	    {1.399299, 0.009595, -0.861009},
	    {0.934667, 0.121429, -1.387609},
	    {-0.387340, -0.208833, 1.694761}
	},
	{     // 66
	    {-1.023347, -1.688939, 0.303031},
	    {0.972973, -3.711342, 0.481853},
	    {2.715681, 3.122240, -3.182886},
	    {-2.665307, 2.278041, 2.398002}
	},
	{     // 67
	    {-1.225886, -0.960035, 0.725898},
	    {0.746174, 0.293730, -0.088316},
	    {1.286049, -0.163769, 1.463163},
	    {-0.806337, 0.830074, -2.100744}
	},
	{     // 68
	    {-0.333767, -0.155207, -0.337693},
	    {-0.056475, -0.094148, -0.082537},
	    {-0.058722, -0.061958, 0.078704},
	    {0.448965, 0.311313, 0.341526}
	},
	{     // 69
	    {0.254881, -1.414057, -1.463226},
	    {1.129084, 0.196189, -1.760455},
	    {-0.486737, 0.868163, 2.318295},
	    {-0.897227, 0.349704, 0.905386}
	},
	{     // 70
	    {-0.792388, 0.005532, -0.034857},
	    {-0.380882, -0.222121, -0.098442},
	    {0.208523, 0.222175, -0.016804},
	    {0.964747, -0.005586, 0.150103}
	},
	{     // 71
	    {-0.183786, -0.128513, 1.119052},
	    {0.339039, 0.313666, 0.786662},
	    {-0.009902, -0.345557, -0.920270},
	    {-0.145352, 0.160404, -0.985445}
	},
	{     // 72
	    {-0.028349, -0.051590, 0.300847},
	    {0.001678, 0.025834, 0.079762},
	    {-0.047881, 0.017396, 0.269948},
	    {0.074552, 0.008361, -0.650557}
	},
	{     // 73
	    {0.166619, -0.157603, -0.292795},
	    {-0.142296, 0.167111, 0.257759},
	    {0.148577, -0.333831, -0.306900},
	    {-0.172900, 0.324323, 0.341936}
	},
	{     // 74
	    {-0.058433, 0.249927, 0.110483},
	    {-0.296627, 1.426904, 0.040968},
	    {0.150933, -0.633015, 0.169749},
	    {0.204127, -1.043817, -0.321199}
	},
	{     // 75
	    {0.185701, 2.583244, -0.179203},
	    {-0.578394, -0.452079, 1.009103},
	    {0.520606, -0.867447, 0.620921},
	    {-0.127913, -1.263718, -1.450821}
	},
	{     // 76
	    {-0.054963, 0.041994, 0.161205},
	    {0.067729, -0.038800, -0.153897},
	    {-0.039855, 0.028463, 0.118636},
	    {0.027089, -0.031657, -0.125944}
	},
	{     // 77
	    {1.183254, -0.899264, 0.339390},
	    {-2.367848, -0.492516, -0.021090},
	    {0.239264, 0.158413, -1.351177},
	    {0.945329, 1.233367, 1.032877}
	},
	{     // 78
	    {0.004722, 0.090824, -0.005469},
	    {-0.001147, -0.047391, 0.000386},
	    {0.000886, 0.026921, -0.002346},
	    {-0.004461, -0.070355, 0.007428}
	},
	{     // 79
	    {0.108532, 0.132444, 0.005634},
	    {0.489546, 0.178550, 0.204667},
	    {-0.137218, 0.038691, 0.079950},
	    {-0.460860, -0.349685, -0.290251}
	},
	{     // 80
	    {0.416737, -0.155029, 1.394521},
	    {0.817313, -0.040876, 0.211313},
	    {-0.813522, 0.136804, -0.006826},
	    {-0.420528, 0.059101, -1.599008}
	},
	{     // 81
	    {-0.575204, 0.253400, -0.251292},
	    {0.180697, -0.156938, 0.014695},
	    {-1.694536, -0.157058, -0.812304},
	    {2.089044, 0.060596, 1.048901}
	},
	{     // 82
	    {-2.303892, 0.473319, -0.471027},
	    {-0.158968, -0.777590, 0.539765},
	    {0.609193, -0.617611, -0.604255},
	    {1.853667, 0.921883, 0.535516}
	},
	{     // 83
	    {0.477888, -0.381079, 0.619382},
	    {-0.209462, -0.030536, 0.421697},
	    {0.074435, -0.146486, -0.339322},
	    {-0.342861, 0.558101, -0.701757}
	},
	{     // 84
	    {0.372877, -0.293971, -0.484847},
	    {0.293008, -0.194960, -0.059315},
	    {-0.391909, 0.321980, 0.355827},
	    {-0.273976, 0.166951, 0.188335}
	},
	{     // 85
	    {0.943509, -1.600662, 1.881734},
	    {-0.193368, 1.122361, -1.853278},
	    {0.998391, 0.118288, -1.356889},
	    {-1.748531, 0.360013, 1.328433}
	},
	{     // 86
	    {1.335448, -1.122118, 1.514768},
	    {1.087457, -1.394737, -0.960904},
	    {-1.104577, 1.424981, 0.746735},
	    {-1.318328, 1.091875, -1.300598}
	},
	{     // 87
	    {0.239835, -0.455904, -0.025835},
	    {-0.148718, 0.257404, -0.097240},
	    {0.070379, -0.130453, -0.043858},
	    {-0.161496, 0.328952, 0.166933}
	},
	{     // 88
	    {-0.388943, -0.861570, -0.021573},
	    {0.562855, -0.820831, 0.387202},
	    {-1.952973, 5.645285, -1.825089},
	    {1.779061, -3.962885, 1.459460}
	},
	{     // 89
	    {-2.769748, -2.817324, -1.100797},
	    {1.660904, 1.918996, 0.221488},
	    {0.185258, 0.537539, 0.397394},
	    {0.923585, 0.360789, 0.481915}
	},
	{     // 90
	    {-1.233876, 2.864710, 0.860210},
	    {-0.771008, 1.250657, 4.393827},
	    {2.722386, -3.532248, -4.576481},
	    {-0.717502, -0.583119, -0.677555}
	},
	{     // 91
	    {1.083517, -0.259745, 0.367526},
	    {-0.410934, -0.767152, 0.051209},
	    {-0.243940, 0.217878, -0.267375},
	    {-0.428643, 0.809020, -0.151361}
	},
	{     // 92
	    {-0.484226, -0.428137, -0.653663},
	    {1.217054, -2.299872, -1.235146},
	    {1.706400, 0.963620, -1.457737},
	    {-2.439229, 1.764388, 3.346546}
	},
	{     // 93
	    {0.141880, 0.399019, -2.019293},
	    {-0.202450, -0.297912, 1.680732},
	    {-2.946397, 1.723496, -2.338773},
	    {3.006966, -1.824602, 2.677334}
	},
	{     // 94
	    {0.060616, -0.187821, -0.027160},
	    {-0.039056, 0.125037, 0.013382},
	    {0.244983, -0.661795, -0.209455},
	    {-0.266543, 0.724579, 0.223233}
	},
	{     // 95
	    {0.617120, 2.648961, 0.664576},
	    {-1.365007, -2.783661, -0.097430},
	    {-0.222168, 0.502536, 0.410603},
	    {0.970054, -0.367836, -0.977750}
	},
	{     // 96
	    {-0.363222, 0.314292, 0.872006},
	    {0.247633, -0.186269, -0.513342},
	    {0.471611, 0.213897, 0.670383},
	    {-0.356022, -0.341920, -1.029047}
	},
	{     // 97
	    {-0.958853, 0.160325, -0.791817},
	    {-1.611157, 0.372368, -0.078590},
	    {1.404718, 0.434551, 0.413693},
	    {1.165292, -0.967244, 0.456714}
	},
	{     // 98
	    {0.045130, -0.256736, 0.209143},
	    {-0.004105, 0.104669, -0.081186},
	    {0.016140, -0.121320, 0.033907},
	    {-0.057165, 0.273387, -0.161865}
	},
	{     // 99
	    {-0.023886, 1.255612, 1.617661},
	    {0.592763, -0.827108, -0.238090},
	    {0.621353, -0.889858, 0.954314},
	    {-1.190230, 0.461354, -2.333885}
	},
	{     // 100
	    {-0.047559, -0.203960, -0.039230},
	    {-0.006898, -0.038555, -0.020252},
	    {0.018714, 0.043142, -0.002674},
	    {0.035743, 0.199372, 0.062156}
	}
    };
    
    examplesForceTest(coords, expectedForces);    
}


int main() {
    srand(1);
    try {
        registerNastiReferenceKernelFactories();
	TestNastiTorsionForce tester;

	tester.testChangingReferenceDihedral();
	tester.testChangingForceConstant();
	tester.testChangingCoordinates();
	tester.testForceDihedral();

	tester.testEnergies();

	tester.disableAngfunc();
	tester.testAngleDerivative();
	tester.testAngleDerivativeRandomized();

	tester.enableAngfunc();
	tester.testAngleDerivative();
	tester.testAngleDerivativeRandomized();

	tester.testDihedralDerivativeRotate();
	tester.testForceAngle();
	
	tester.testForceWithExamples();
	tester.testForceWithRandomExamples();
	
	tester.testForceRandomized();
    }
    catch(const exception& e) {
        cout << "exception: " << e.what() << endl;
        return 1;
    }
    cout << "Done" << endl;
    return 0;
}
