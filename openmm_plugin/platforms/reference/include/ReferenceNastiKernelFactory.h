#ifndef OPENMM_REFERENCENASTIKERNELFACTORY_H_
#define OPENMM_REFERENCENASTIKERNELFACTORY_H_

#include "openmm/KernelFactory.h"

namespace OpenMM {

/**
 * This KernelFactory creates kernels for the reference implementation of the Nasti plugin.
 */

class ReferenceNastiKernelFactory : public KernelFactory {
public:
    KernelImpl* createKernelImpl(std::string name,
				 const Platform& platform,
				 ContextImpl& context) const;
};

} // namespace OpenMM

#endif /*OPENMM_REFERENCENASTIKERNELFACTORY_H_*/
