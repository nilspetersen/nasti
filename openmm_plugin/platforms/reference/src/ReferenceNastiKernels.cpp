
#include "ReferenceNastiKernels.h"
#include "ReferenceNastiForceFuncs.h"
#include "NastiTorsionForce.h"

#include "openmm/OpenMMException.h"
#include "openmm/internal/ContextImpl.h"
#include "openmm/Vec3.h"
#include "openmm/reference/ReferencePlatform.h"
#include "openmm/reference/ReferenceBondForce.h"

using namespace Nasti;
using namespace OpenMM;
using namespace std;


static int** allocateIntArray(int length, int width) {
    int** array = new int*[length];
    for (int i = 0; i < length; ++i)
        array[i] = new int[width];
    return array;
}

static double** allocateRealArray(int length, int width) {
    double** array = new double*[length];
    for (int i = 0; i < length; ++i)
        array[i] = new double[width];
    return array;
}

static double** allocateDummyDoubleNullArray(int length) {
    return new double*[length]();
}

static void disposeIntArray(int** array, int size) {
    if (array) {
        for (int i = 0; i < size; ++i)
            delete[] array[i];
        delete[] array;
    }
}

static void disposeRealArray(double** array, int size) {
    if (array) {
        for (int i = 0; i < size; ++i)
            delete[] array[i];
        delete[] array;
    }
}

static void disposeDummyDoubleNullArray(double** array) {
    if(array) {
	delete [] array;
    }
}


static vector<Vec3>&
extractPositions(ContextImpl& context) {
    ReferencePlatform::PlatformData* data =
	reinterpret_cast<ReferencePlatform::PlatformData*>(context.getPlatformData());
    return *((vector<Vec3>*) data->positions);
}

static vector<Vec3>&
extractForces(ContextImpl& context) {
    ReferencePlatform::PlatformData* data =
	reinterpret_cast<ReferencePlatform::PlatformData*>(context.getPlatformData());
    return *((vector<Vec3>*) data->forces);
}

ReferenceCalcNastiTorsionForceKernel::~ReferenceCalcNastiTorsionForceKernel() {
    disposeIntArray(torsionIndexArray, numTorsions);
    disposeRealArray(torsionParams, numTorsions);
}

void
ReferenceCalcNastiTorsionForceKernel::initialize(const System& system,
						 const NastiTorsionForce& force) {
    // Initialize bond parameters.

    numTorsions = force.getNumTorsions();

    torsionIndexArray = allocateIntArray(numTorsions, 4);
    torsionParams = allocateRealArray(numTorsions, 3);

    for (int i = 0; i < numTorsions; i++) {
        force.getTorsionParameters(i,
				   torsionIndexArray[i][0],
				   torsionIndexArray[i][1],
				   torsionIndexArray[i][2],
				   torsionIndexArray[i][3],
				   torsionParams[i][0], // phase
				   torsionParams[i][1], // forceConstant
				   torsionParams[i][2]); // margin
    }
}

double
ReferenceCalcNastiTorsionForceKernel::execute(ContextImpl& context,
					      bool includeForces,
					      bool includeEnergy) {
    vector<Vec3>& posData = extractPositions(context);
    vector<Vec3>& forceData = extractForces(context);

    ReferenceNastiTorsionIxn referenceNastiTorsionIxn;
    ReferenceBondForce refBondForce;
    
    double energy = 0.0;
    refBondForce.calculateForce(numTorsions,
				torsionIndexArray,
				posData,
				torsionParams,
				forceData,
				includeEnergy ? &energy : NULL,
				referenceNastiTorsionIxn);	
    return energy;
}

void
ReferenceCalcNastiTorsionForceKernel::copyParametersToContext(ContextImpl& context,
							      const NastiTorsionForce& force) {
    if (force.getNumTorsions() != numTorsions)
        throw OpenMMException("updateParametersInContext: The number of Nasti Torsions has changed");

    for (int i = 0; i < force.getNumTorsions(); i++) {

        int p1, p2, p3, p4;

        force.getTorsionParameters(i,
				   p1,
				   p2,
				   p3,
				   p4,
				   torsionParams[i][0], // phase
				   torsionParams[i][1], // forceConstant
				   torsionParams[i][2]); // margin

	if (p1 != torsionIndexArray[i][0] ||
	    p2 != torsionIndexArray[i][1] ||
	    p3 != torsionIndexArray[i][2] ||
	    p4 != torsionIndexArray[i][3]) {
            throw OpenMMException("updateParametersInContext: A particle index has changed");
	}
    }
}


// ReferenceCalcNastiAngleForceKernel

ReferenceCalcNastiAngleForceKernel::~ReferenceCalcNastiAngleForceKernel() {
    disposeIntArray(angleIndexArray, numAngles);
    disposeDummyDoubleNullArray(dummyNull);
}
    

void
ReferenceCalcNastiAngleForceKernel::initialize(const System& system,
					       const NastiAngleForce& force) {
    // Initialize bond parameters.
    numAngles = force.getNumAngles();
    angleIndexArray = allocateIntArray(numAngles, 3);
    dummyNull = allocateDummyDoubleNullArray(numAngles);
    
    for (int i = 0; i < numAngles; i++) {
        force.getAngleParameters(i,
				 angleIndexArray[i][0],
				 angleIndexArray[i][1],
				 angleIndexArray[i][2]);
    }
    referenceNastiAngleIxn.set_spline_parameters(force.getIntervalBorders(),
						 force.getConstantCoefficients(),
						 force.getLinearCoefficients(),
						 force.getQuadraticCoefficients());
}


double
ReferenceCalcNastiAngleForceKernel::execute(ContextImpl& context,
					    bool includeForces,
					    bool includeEnergy) {
    vector<Vec3>& posData = extractPositions(context);
    vector<Vec3>& forceData = extractForces(context);
    double energy = 0.0;
    ReferenceBondForce refBondForce;
    refBondForce.calculateForce(numAngles,
				angleIndexArray,
				posData,
				dummyNull,  // no parameter array needed here! (spline parameters stored in ixn)
				forceData,
				includeEnergy ? &energy : NULL,
				referenceNastiAngleIxn);
    return energy;
}

void
ReferenceCalcNastiAngleForceKernel::copyParametersToContext(ContextImpl& context,
							    const NastiAngleForce& force) {
    if (force.getNumAngles() != numAngles) {
        throw OpenMMException("updateParametersInContext: The number of Nasti Angles has changed");
    }
    
    for (int i = 0; i < numAngles; i++) {

        int p1, p2, p3;

        force.getAngleParameters(i, p1, p2, p3);

	if (p1 != angleIndexArray[i][0] || p2 != angleIndexArray[i][1] || p3 != angleIndexArray[i][2]) {
            throw OpenMMException("updateParametersInContext: A particle index has changed");
	}
    }
    referenceNastiAngleIxn.set_spline_parameters(force.getIntervalBorders(),
						 force.getConstantCoefficients(),
						 force.getLinearCoefficients(),
						 force.getQuadraticCoefficients());
}

