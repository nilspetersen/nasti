
#include <algorithm>

#include "ReferenceNastiForceFuncs.h"
#include "openmm/Vec3.h"

#include "openmm/reference/ReferenceForce.h"
#include "openmm/reference/SimTKOpenMMUtilities.h"
#include "openmm/reference/ReferenceProperDihedralBond.h"
#include "openmm/reference/ReferenceBondIxn.h"


using namespace OpenMM;
using namespace std;
using namespace Nasti;


static int
bin_search_energy_index(double val, const vector<double> t) {
    int index = upper_bound(t.begin(), t.end(), val) - t.begin() - 1;
    return index;
}

/*
  Names for delta vectors
  r_ij = r_i - r_j
  Names for cross products:
  A = IJ x KJ
  B = KJ x KL
 */

enum {I, J, K, L};
enum {IJ, KJ, KL};
enum {A, B};

/**---------------------------------------------------------------------------------------

    dihedralEnergyAndDerivatives

    @param deltaR             contains r_ij, r_jk, r_kl vectors and their norm
                              has shape [3][ReferenceForce::LastDeltaRIndex]
    @param referenceDihedral  when the force constant is negative it's the optimum
    @param forceConstant      force constant
    @param derivatives        derivatives of the energy with respect to the coordinates
                              of the four atoms will be written here if it is not NULL
    @param crossProducts      get the two cross products to re-use them for the angles
                              should be double[2][3]

    a lot of the code in this function is rewritten from ReferenceProperDihedralBond.h/.cpp 
    in OpenMM and uses functions from SimTKOpenMMUtilities.h/.cpp

   --------------------------------------------------------------------------------------- */

static double
dihedralEnergyAndDerivatives(double*const* deltaR,
			     double referenceDihedral,
			     double forceConstant,
			     Vec3* derivatives,
			     double** crossProducts) {

    const double MULTIPLICITY = 1.0;

    double dotDihedral;
    double signOfAngle;
    int hasREntry = 1;

    // get dihedral angle
    double dihedralAngle = ReferenceBondIxn::getDihedralAngleBetweenThreeVectors(deltaR[IJ],
										 deltaR[KJ],
										 deltaR[KL],
										 crossProducts,
										 &dotDihedral,
										 deltaR[IJ],
										 &signOfAngle,
										 hasREntry);

    // evaluate delta angle, dE/d(angle)
    double deltaAngle = MULTIPLICITY * dihedralAngle - referenceDihedral;
    double energy = forceConstant * cos(deltaAngle);
    // double energy = forceConstant * (1.0 + cos(deltaAngle));

    if(derivatives == NULL) {
	return energy;
    }

    double sinDeltaAngle = SIN(deltaAngle);
    double dEdAngle = -forceConstant * MULTIPLICITY * sinDeltaAngle;

    // compute derivatives
    double ff1, ff2, ff3, ff4; 	// forceFactors
    double normKJ = deltaR[KJ][ReferenceForce::RIndex];

    ff1 = (-dEdAngle * normKJ) / DOT3(crossProducts[A], crossProducts[A]);
    ff4 = (dEdAngle * normKJ) / DOT3(crossProducts[B], crossProducts[B]);

    ff2 = DOT3(deltaR[IJ], deltaR[KJ]) / deltaR[KJ][ReferenceForce::R2Index];
    ff3 = DOT3(deltaR[KL], deltaR[KJ]) / deltaR[KJ][ReferenceForce::R2Index];

    for (int ii = 0; ii < 3; ii++) {

	derivatives[0][ii] = ff1 * crossProducts[A][ii];
	derivatives[3][ii] = ff4 * crossProducts[B][ii];

	double s  = ff2 * derivatives[0][ii] - ff3 * derivatives[3][ii];

	derivatives[1][ii] = derivatives[0][ii] - s;
	derivatives[2][ii] = derivatives[3][ii] + s;
    }

    // change the sign of derivative 0 and 3
    derivatives[0] *= -1.0;
    derivatives[3] *= -1.0;

    return energy;
}

static void
calcAngleDerivatives(Vec3* derivatives,
		     double* deltaR_ij,
		     double* deltaR_kj,
		     const double* crossProduct) {
    
    // normalised cross product vector
    double p_norm = sqrt(DOT3(crossProduct, crossProduct));
    double p[3];
    for(int i=0; i < 3; ++i) {
	p[i] = crossProduct[i] / p_norm;
    }

    // more cross products
    double cross_ij_p[3], cross_kj_p[3];
    SimTKOpenMMUtilities::crossProductVector3(deltaR_ij, p, cross_ij_p);
    SimTKOpenMMUtilities::crossProductVector3(deltaR_kj, p, cross_kj_p);

    // dr_i (1 / n_ij*2) * cross(r_ij, p) 
    // dr_k -(1 / n_ij*2) * cross(r_kj, p)
    // dr_j = dr_k - dr_i
    for(int i=0; i < 3; ++i) {
	derivatives[I][i] = cross_ij_p[i] / deltaR_ij[ReferenceForce::R2Index];
	derivatives[K][i] = - cross_kj_p[i] / deltaR_kj[ReferenceForce::R2Index];
	derivatives[J][i] = - derivatives[K][i] - derivatives[I][i]; // change the sign of derivatives K ?
    }
}

static double
angleAndDerivative(Vec3* derivatives,
		   double* deltaR_ij,
		   double* deltaR_kj,
		   const double* crossProduct) {
    int hasREntry = 1;
    double theta = ReferenceBondIxn::getAngleBetweenTwoVectors(deltaR_ij,
    							       deltaR_kj,
    							       NULL,
    							       hasREntry);
    if(derivatives == NULL || theta == 0.0 || theta == M_PI) {
	return theta;
    }
    calcAngleDerivatives(derivatives,
			 deltaR_ij,
			 deltaR_kj,
			 crossProduct);
    return theta;
}

/**---------------------------------------------------------------------------------------

   calcAngleFuncAndDerivatives

   @param derivatives   vector with derivatives, NULL if no derivatives wanted, 
                        otherwise all elements are expected to be initialized to Vec3(0,0,0)
   @param margin        specific parameter for NASTi dihedrals
   @param deltaR_ij     r_ij vector with norm and squared norm
   @param deltaR_kj     r_kj vector with norm and squared norm
   @param crossProduct  cross product between r_ij and r_kj, required for the computation
                        of the angle derivatives

   --------------------------------------------------------------------------------------- */

static double 
calcAngleFuncAndDerivatives(Vec3* derivatives,
			    double margin,
			    double* deltaR_ij,
			    double* deltaR_kj,
			    const double* crossProduct) { // required for derivatives

    Vec3 angleDerivativesMemory[3];
    Vec3 *angleDerivatives = (derivatives == NULL) ? NULL : angleDerivativesMemory;
    
    double theta = angleAndDerivative(angleDerivatives,
				      deltaR_ij,
				      deltaR_kj,
				      crossProduct); // required for derivatives
    double lo = margin;
    double up = M_PI - margin;
    
    if((theta >= lo) && (theta <= up)) {
	return 1; 		// derivatives are 0
    }
    if((theta == 0.0) || (theta == M_PI)) {
	return 0.0; 		// derivatives are 0
    }
    
    // compute the angle function
    double innerF = 1.0;
    if(theta < lo) {
	innerF = M_PI * (theta / margin - 1.0);
    } else if(theta > up){
	innerF = M_PI * ((theta - M_PI) / margin + 1.0);
    }
    double fTheta = 0.5 * cos(innerF) + 0.5;

    if(derivatives == NULL) {
	return fTheta;
    }
    
    // compute the angle function derivative using the chain rule
    double outerDerivative = -0.5 * sin(innerF);
    double innerDerivative = M_PI / margin;
    double d_fTheta_d_theta = outerDerivative * innerDerivative;

    derivatives[I] = angleDerivatives[I] * d_fTheta_d_theta;
    derivatives[J] = angleDerivatives[J] * d_fTheta_d_theta;
    derivatives[K] = angleDerivatives[K] * d_fTheta_d_theta;
    
    return fTheta;
}


void
ReferenceNastiTorsionIxn::calculateBondIxn(int* atomIndices,
					   vector<Vec3>& atomCoordinates,
					   double* parameters,
					   vector<Vec3>& forces,
					   double* totalEnergy,
					   double* energyParamDerivs) {
    int i = atomIndices[0];
    int j = atomIndices[1];
    int k = atomIndices[2];
    int l = atomIndices[3];
    
    double referenceDihedral = parameters[0];
    double forceConstant = parameters[1];
    double margin = parameters[2];
    
    // allocate memory and variables
    Vec3 derivativesDihedralEnergy[4]; 	// dihedral derivatives
    Vec3 derivativesAlpha[3];	// angle alpha derivatives
    Vec3 derivativesBeta[3];	// angle beta derivatives

    double deltaRMemory[3*ReferenceForce::LastDeltaRIndex];
    double *deltaR[3];
    deltaR[IJ] = deltaRMemory;
    deltaR[KJ] = deltaRMemory + ReferenceForce::LastDeltaRIndex;
    deltaR[KL] = deltaRMemory + 2*ReferenceForce::LastDeltaRIndex;

    // memory for cross products
    double crossProductMemory[6];
    double* crossProduct[2];
    crossProduct[A] = crossProductMemory;
    crossProduct[B] = crossProductMemory + 3;
    
    // compute delta R, R2 and R between three atom pairs [i,j] [j,k] and [k,l]
    ReferenceForce::getDeltaR(atomCoordinates[j],
			      atomCoordinates[i],
			      deltaR[IJ]);

    ReferenceForce::getDeltaR(atomCoordinates[j],
			      atomCoordinates[k],
			      deltaR[KJ]);

    ReferenceForce::getDeltaR(atomCoordinates[l],
			      atomCoordinates[k],
			      deltaR[KL]);
    
    // compute dihedral related energy and derivatives
    double energyDihedral = dihedralEnergyAndDerivatives(deltaR,
							 referenceDihedral,
							 forceConstant,
							 derivativesDihedralEnergy,
							 crossProduct);
    energyDihedral -= forceConstant;
    
    // compute angle related stuff
    double fAlpha = calcAngleFuncAndDerivatives(derivativesAlpha,
						margin,
						deltaR[IJ],
						deltaR[KJ],
						crossProduct[A]); // required for derivatives
    
    double fBeta = calcAngleFuncAndDerivatives(derivativesBeta,
					       margin,
					       deltaR[KJ],
					       deltaR[KL],
					       crossProduct[B]); // required for derivatives
    double fAlphaBeta = fAlpha * fBeta;

    if(fAlphaBeta != 0.0) {
	// ADD FORCES
	// dihedral derivative
	forces[i] -= derivativesDihedralEnergy[0] * fAlphaBeta;
	forces[j] -= derivativesDihedralEnergy[1] * fAlphaBeta;
	forces[k] -= derivativesDihedralEnergy[2] * fAlphaBeta;
	forces[l] -= derivativesDihedralEnergy[3] * fAlphaBeta;
	// alpha derivative
	double fBetaDih = fBeta * energyDihedral;
	forces[i] -= derivativesAlpha[0] * fBetaDih;
	forces[j] -= derivativesAlpha[1] * fBetaDih;
	forces[k] -= derivativesAlpha[2] * fBetaDih;
	// beta derivative
	double fAlphaDih = -fAlpha * energyDihedral;
	forces[j] -= derivativesBeta[0] * fAlphaDih;
	forces[k] -= derivativesBeta[1] * fAlphaDih;
	forces[l] -= derivativesBeta[2] * fAlphaDih;
    }
    if(totalEnergy){
	*totalEnergy += fAlphaBeta * energyDihedral + 2*forceConstant;
    }
}

void
ReferenceNastiAngleIxn::set_spline_parameters(const vector<double>& intervalBorders,
					      const vector<double>& coeffConst,
					      const vector<double>& coeffLin,
					      const vector<double>& coeffQuad) {
    this->intervalBorders = intervalBorders;
    this->coeffConst = coeffConst;
    this->coeffLin = coeffLin;
    this->coeffQuad = coeffQuad;
}

void
ReferenceNastiAngleIxn::calculateBondIxn(int* atomIndices,
					 std::vector<OpenMM::Vec3>& atomCoordinates,
					 double* parameters,
					 std::vector<OpenMM::Vec3>& forces,
					 double* totalEnergy,
					 double* energyParamDerivs) {
    int i = atomIndices[0];
    int j = atomIndices[1];
    int k = atomIndices[2];

    Vec3 angleDerivatives[3];
    double deltaRMemory[2*ReferenceForce::LastDeltaRIndex];
    double *deltaR[2];
    deltaR[IJ] = deltaRMemory;
    deltaR[KJ] = deltaRMemory + ReferenceForce::LastDeltaRIndex;
    double crossProduct[3];

    ReferenceForce::getDeltaR(atomCoordinates[j],
			      atomCoordinates[i],
			      deltaR[IJ]);

    ReferenceForce::getDeltaR(atomCoordinates[j],
			      atomCoordinates[k],
			      deltaR[KJ]);
    
    SimTKOpenMMUtilities::crossProductVector3(deltaR[IJ],
					      deltaR[KJ],
					      crossProduct);
    
    // compute the angle (and the derivative ?)
    double theta = angleAndDerivative(angleDerivatives,
				      deltaR[IJ],
				      deltaR[KJ],
				      crossProduct);
    
    // figure out (using binary search? - write a function for this !!!) which
    // parameters to use
    int ii = bin_search_energy_index(theta, intervalBorders);

    // forces
    double a = coeffConst[ii];
    double b = coeffLin[ii];
    double c = coeffQuad[ii];

    double dE_dTheta = b + 2*c*theta;
    forces[i] -= angleDerivatives[0] * dE_dTheta;
    forces[j] -= angleDerivatives[1] * dE_dTheta;
    forces[k] -= angleDerivatives[2] * dE_dTheta;

    if(totalEnergy) {
	*totalEnergy += a + b*theta + c*theta*theta;
    }
}


// functions to use in tests

void
NastiKernelStaticExport::deltaCross(double **deltaR,
				    double *crossProduct,
				    Vec3& r_i,
				    Vec3& r_j,
				    Vec3& r_k) {
    ReferenceForce::getDeltaR(r_j,
			      r_i,
			      deltaR[IJ]);

    ReferenceForce::getDeltaR(r_j,
			      r_k,
			      deltaR[KJ]);

    SimTKOpenMMUtilities::crossProductVector3(deltaR[IJ],
					      deltaR[KJ],
					      crossProduct);
}

double
NastiKernelStaticExport::angleDerivative(Vec3* derivatives,
					 Vec3 r_i,
					 Vec3 r_j,
					 Vec3 r_k) {
    
    double deltaRMemory[2*ReferenceForce::LastDeltaRIndex];
    double *deltaR[2];
    deltaR[IJ] = deltaRMemory;
    deltaR[KJ] = deltaRMemory + ReferenceForce::LastDeltaRIndex;
    double crossProduct[3];

    deltaCross(deltaR,
	       crossProduct,
	       r_i,
	       r_j,
	       r_k);
    
    return angleAndDerivative(derivatives,
			      deltaR[IJ],
			      deltaR[KJ],
			      crossProduct);
}

double 
NastiKernelStaticExport::angleFuncDerivative(Vec3* derivatives,
					     double margin,
					     Vec3 r_i,
					     Vec3 r_j,
					     Vec3 r_k) {
    
    double deltaRMemory[2*ReferenceForce::LastDeltaRIndex];
    double *deltaR[2];
    deltaR[IJ] = deltaRMemory;
    deltaR[KJ] = deltaRMemory + ReferenceForce::LastDeltaRIndex;
    double crossProduct[3];

    deltaCross(deltaR,
	       crossProduct,
	       r_i,
	       r_j,
	       r_k);
    
    return calcAngleFuncAndDerivatives(derivatives,
				       margin,
				       deltaR[IJ],
				       deltaR[KJ],
				       crossProduct);
}

double
NastiKernelStaticExport::nastiDihedralDerivative(Vec3* derivatives,
						 double referenceDihedral,
						 double forceConstant,						 
						 Vec3 r_i,
						 Vec3 r_j,
						 Vec3 r_k,
						 Vec3 r_l) {
    // allocate memory and variables
    double deltaRMemory[3*ReferenceForce::LastDeltaRIndex];
    double *deltaR[3];
    deltaR[IJ] = deltaRMemory;
    deltaR[KJ] = deltaRMemory + ReferenceForce::LastDeltaRIndex;
    deltaR[KL] = deltaRMemory + 2*ReferenceForce::LastDeltaRIndex;

    // memory for cross products
    double crossProductMemory[6];
    double* crossProduct[2];
    crossProduct[A] = crossProductMemory;
    crossProduct[B] = crossProductMemory + 3;
    
    // compute delta R, R2 and R between three atom pairs [i,j] [j,k] and [k,l]
    ReferenceForce::getDeltaR(r_j, r_i, deltaR[IJ]);
    ReferenceForce::getDeltaR(r_j, r_k, deltaR[KJ]);
    ReferenceForce::getDeltaR(r_l, r_k, deltaR[KL]);

    double dihEnergy = dihedralEnergyAndDerivatives(deltaR,
						    referenceDihedral,
						    forceConstant,
						    derivatives,
						    crossProduct);
    return dihEnergy;
}
