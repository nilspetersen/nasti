
#include "ReferenceNastiKernelFactory.h"
#include "ReferenceNastiKernels.h"
#include "openmm/reference/ReferencePlatform.h"
#include "openmm/internal/ContextImpl.h"
#include "openmm/OpenMMException.h"

using namespace Nasti;
using namespace OpenMM;

extern "C" OPENMM_EXPORT void registerPlatforms() {
}

extern "C" OPENMM_EXPORT void registerKernelFactories() {

    for (int i = 0; i < Platform::getNumPlatforms(); i++) {

	Platform& platform = Platform::getPlatform(i);

	if (dynamic_cast<ReferencePlatform*>(&platform) != NULL) {
	    ReferenceNastiKernelFactory* factory = new ReferenceNastiKernelFactory();
	    // register kernels
	    platform.registerKernelFactory(CalcNastiTorsionForceKernel::Name(), factory);
	    platform.registerKernelFactory(CalcNastiAngleForceKernel::Name(), factory);
        }
    }
}

extern "C" OPENMM_EXPORT void registerNastiReferenceKernelFactories() {
    registerKernelFactories();
}

KernelImpl*
ReferenceNastiKernelFactory::createKernelImpl(std::string name,
					      const Platform& platform,
					      ContextImpl& context) const {
    
    ReferencePlatform::PlatformData& data = *static_cast<ReferencePlatform::PlatformData*>(context.getPlatformData()); // do I need this?
    
    if (name == CalcNastiTorsionForceKernel::Name()) {
	return new ReferenceCalcNastiTorsionForceKernel(name, platform);
    }
    if (name == CalcNastiAngleForceKernel::Name()) {
	return new ReferenceCalcNastiAngleForceKernel(name, platform);
    }
    
    throw OpenMMException((std::string("Tried to create kernel with illegal kernel name '")+name+"'").c_str());
}
