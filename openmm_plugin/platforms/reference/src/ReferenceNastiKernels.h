#ifndef REFERENCE_NASTI_KERNELS_H_
#define REFERENCE_NASTI_KERNELS_H_

#include "OpenMM.h"
#include "NastiKernels.h"
#include <vector>
#include "ReferenceNastiForceFuncs.h"

namespace Nasti {

/**
 * This kernel is invoked by NastiTorsionForce to calculate the forces 
 * acting on the system and the energy of the system.
 */

class ReferenceCalcNastiTorsionForceKernel : public CalcNastiTorsionForceKernel {
public:
    
    ReferenceCalcNastiTorsionForceKernel(std::string name,
					 const OpenMM::Platform& platform)
	:CalcNastiTorsionForceKernel(name, platform),
	 torsionIndexArray(NULL),
	 torsionParams(NULL)
	{}

    ~ReferenceCalcNastiTorsionForceKernel();
    
    /**
     * Initialize the kernel.
     * 
     * @param system     the System this kernel will be applied to
     * @param force      the NastiTorsionForce this kernel will be used for
     */

    void initialize(const OpenMM::System& system,
		    const NastiTorsionForce& force);
    
    /**
     * Execute the kernel to calculate the forces and/or energy.
     *
     * @param context        the context in which to execute this kernel
     * @param includeForces  true if forces should be calculated
     * @param includeEnergy  true if the energy should be calculated
     * @return the potential energy due to the force
     */
    
    double execute(OpenMM::ContextImpl& context,
		   bool includeForces,
		   bool includeEnergy);
    
    /**
     * Copy changed parameters over to a context.
     *
     * @param context    the context to copy parameters to
     * @param force      the NastiTorsionForce to copy the parameters from
     */
    
    void copyParametersToContext(OpenMM::ContextImpl& context,
				 const NastiTorsionForce& force);
private:
    int numTorsions;
    int **torsionIndexArray;
    double **torsionParams;	// each entry contains: 0: phase, 1: forceConstant, 2: Margin
};

/**
 * This kernel is invoked by NastiAngleForce to calculate the forces 
 * acting on the system and the energy of the system.
 */

class ReferenceCalcNastiAngleForceKernel : public CalcNastiAngleForceKernel {
public:
    ReferenceCalcNastiAngleForceKernel(std::string name,
				       const OpenMM::Platform& platform)
	:CalcNastiAngleForceKernel(name, platform),
	 angleIndexArray(NULL),
	 dummyNull(NULL)
	{}

    ~ReferenceCalcNastiAngleForceKernel();
    /**
     * Initialize the kernel.
     * 
     * @param system     the System this kernel will be applied to
     * @param force      the NastiAngleForce this kernel will be used for
     */

    void initialize(const OpenMM::System& system,
		    const NastiAngleForce& force);
    
    /**
     * Execute the kernel to calculate the forces and/or energy.
     *
     * @param context        the context in which to execute this kernel
     * @param includeForces  true if forces should be calculated
     * @param includeEnergy  true if the energy should be calculated
     * @return the potential energy due to the force
     */
    
    double execute(OpenMM::ContextImpl& context,
		   bool includeForces,
		   bool includeEnergy);
    
    /**
     * Copy changed parameters over to a context.
     *
     * @param context    the context to copy parameters to
     * @param force      the NastiAngleForce to copy the parameters from
     */
    
    void copyParametersToContext(OpenMM::ContextImpl& context,
				 const NastiAngleForce& force);

private:
    ReferenceNastiAngleIxn referenceNastiAngleIxn;
    int numAngles;
    int **angleIndexArray;
    double **dummyNull;
};
    
} // namespace Nasti

#endif /*REFERENCE_NASTI_KERNELS_H_*/
