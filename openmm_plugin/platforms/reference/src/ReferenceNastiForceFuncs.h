#ifndef REFERENCE_NASTI_FORCEFUNCS_H_
#define REFERENCE_NASTI_FORCEFUNCS_H_

#include "OpenMM.h"
#include <vector>
#include "openmm/Vec3.h"
#include "openmm/reference/ReferenceBondIxn.h"

namespace Nasti {


class ReferenceNastiTorsionIxn : public OpenMM::ReferenceBondIxn {
    /**---------------------------------------------------------------------------------------
       Calculate Angle Bond ixn
       
       @param atomIndices      four atom indices
       @param atomCoordinates  atom coordinates
       @param parameters       0: phase, 1: forceConstant, 2: Margin
       @param forces           force array (forces added)
       @param totalEnergy      if not null, the energy will be added to this
       @param energyParamDerivs: will be ignored, use NULL
       --------------------------------------------------------------------------------------- */    
    void calculateBondIxn(int* atomIndices,
			  std::vector<OpenMM::Vec3>& atomCoordinates,
			  double* parameters,
			  std::vector<OpenMM::Vec3>& forces,
			  double* totalEnergy,
			  double* energyParamDerivs);
};

class ReferenceNastiAngleIxn : public OpenMM::ReferenceBondIxn {
public:
    void set_spline_parameters(const std::vector<double>& intervalBorders,
			       const std::vector<double>& coeffConst,
			       const std::vector<double>& coeffLin,
			       const std::vector<double>& coeffQuad);
    
    /**---------------------------------------------------------------------------------------
       Calculate Angle Bond ixn
       
       @param atomIndices      three atom indices
       @param atomCoordinates  atom coordinates
       @param parameters       is ignored, use NULL!
       @param forces           force array (forces added)
       @param totalEnergy      if not null, the energy will be added to this
       @param energyParamDerivs: will be ignored, use NULL
       --------------------------------------------------------------------------------------- */
    
    void calculateBondIxn(int* atomIndices,
			  std::vector<OpenMM::Vec3>& atomCoordinates,
			  double* parameters,
			  std::vector<OpenMM::Vec3>& forces,
			  double* totalEnergy,
			  double* energyParamDerivs);
private:
    std::vector<double> intervalBorders;
    std::vector<double> coeffConst;
    std::vector<double> coeffLin;
    std::vector<double> coeffQuad;
};


/**
 * NastiKernelStaticExport
 * 
 * this class is only used for tests if one wants access
 * to static functions within ReferenceNastiKernels.cpp
 */

class NastiKernelStaticExport {

public:
    static double angleDerivative(OpenMM::Vec3* derivatives,
				  OpenMM::Vec3 r_i,
				  OpenMM::Vec3 r_j,
				  OpenMM::Vec3 r_k);

    static double angleFuncDerivative(OpenMM::Vec3* derivatives,
				      double margin,
				      OpenMM::Vec3 r_i,
				      OpenMM::Vec3 r_j,
				      OpenMM::Vec3 r_k);
    
    static double nastiDihedralDerivative(OpenMM::Vec3* derivatives,
					  double referenceDihedral,
					  double forceConstant,
					  OpenMM::Vec3 r_i,
					  OpenMM::Vec3 r_j,
					  OpenMM::Vec3 r_k,
					  OpenMM::Vec3 r_l);
private:
    static void deltaCross(double **deltaR,
			   double *crossProduct,
			   OpenMM::Vec3& r_i,
			   OpenMM::Vec3& r_j,
			   OpenMM::Vec3& r_k);
};

} // namespace Nasti

#endif /*REFERENCE_NASTI_KERNELS_H_*/
