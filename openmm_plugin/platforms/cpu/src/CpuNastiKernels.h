#ifndef CPU_NASTI_KERNELS_H_
#define CPU_NASTI_KERNELS_H_

#include "OpenMM.h"
#include "NastiKernels.h"
#include <vector>
#include "openmm/cpu/CpuPlatform.h"
#include "openmm/cpu/CpuBondForce.h"
#include "ReferenceNastiForceFuncs.h"


namespace Nasti {

/**
 * This kernel is invoked by NastiTorsionForce to calculate the forces 
 * acting on the system and the energy of the system.
 */

class CpuCalcNastiTorsionForceKernel : public CalcNastiTorsionForceKernel {
public:
    
    CpuCalcNastiTorsionForceKernel(std::string name,
				   const OpenMM::Platform& platform,
				   OpenMM::CpuPlatform::PlatformData& data)
	:CalcNastiTorsionForceKernel(name, platform),
	 platformData(data),
	 torsionIndexArray(NULL),
	 torsionParams(NULL) {}
    
    ~CpuCalcNastiTorsionForceKernel();
    
    /**
     * Initialize the kernel.
     * 
     * @param system     the System this kernel will be applied to
     * @param force      the NastiTorsionForce this kernel will be used for
     */

    void initialize(const OpenMM::System& system,
		    const NastiTorsionForce& force);
    
    /**
     * Execute the kernel to calculate the forces and/or energy.
     *
     * @param context        the context in which to execute this kernel
     * @param includeForces  true if forces should be calculated
     * @param includeEnergy  true if the energy should be calculated
     * @return the potential energy due to the force
     */
    
    double execute(OpenMM::ContextImpl& context,
		   bool includeForces,
		   bool includeEnergy);
    
    /**
     * Copy changed parameters over to a context.
     *
     * @param context    the context to copy parameters to
     * @param force      the NastiTorsionForce to copy the parameters from
     */
    
    void copyParametersToContext(OpenMM::ContextImpl& context,
				 const NastiTorsionForce& force);
private:
    OpenMM::CpuPlatform::PlatformData& platformData;
    int numTorsions;
    int **torsionIndexArray;
    double **torsionParams;	// each entry contains: 0: phase, 1: forceConstant, 2: Margin
    OpenMM::CpuBondForce bondForce;
};

/**
 * This kernel is invoked by NastiAngleForce to calculate the forces 
 * acting on the system and the energy of the system.
 */

class CpuCalcNastiAngleForceKernel : public CalcNastiAngleForceKernel {
public:
    
    CpuCalcNastiAngleForceKernel(std::string name,
				 const OpenMM::Platform& platform,
				 OpenMM::CpuPlatform::PlatformData& data)
	:CalcNastiAngleForceKernel(name, platform),
	 platformData(data),
	 angleIndexArray(NULL),
	 dummyNull(NULL)
	{}
	
    ~CpuCalcNastiAngleForceKernel();
    /**
     * Initialize the kernel.
     * 
     * @param system     the System this kernel will be applied to
     * @param force      the NastiAngleForce this kernel will be used for
     */

    void initialize(const OpenMM::System& system,
		    const NastiAngleForce& force);
    
    /**
     * Execute the kernel to calculate the forces and/or energy.
     *
     * @param context        the context in which to execute this kernel
     * @param includeForces  true if forces should be calculated
     * @param includeEnergy  true if the energy should be calculated
     * @return the potential energy due to the force
     */
    
    double execute(OpenMM::ContextImpl& context,
		   bool includeForces,
		   bool includeEnergy);
    
    /**
     * Copy changed parameters over to a context.
     *
     * @param context    the context to copy parameters to
     * @param force      the NastiAngleForce to copy the parameters from
     */
    
    void copyParametersToContext(OpenMM::ContextImpl& context,
				 const NastiAngleForce& force);
private:
    ReferenceNastiAngleIxn referenceNastiAngleIxn;
    OpenMM::CpuPlatform::PlatformData& platformData;
    int numAngles;
    int **angleIndexArray;
    double **dummyNull;
    OpenMM::CpuBondForce bondForce;
};

} // namespace Nasti

#endif /*CPU_NASTI_KERNELS_H__*/
