
#include "CpuNastiKernelFactory.h"
#include "CpuNastiKernels.h"
#include "openmm/cpu/CpuPlatform.h"
#include "openmm/internal/ContextImpl.h"
#include "openmm/OpenMMException.h"

using namespace Nasti;
using namespace OpenMM;

extern "C" OPENMM_EXPORT void registerPlatforms() {
}

extern "C" OPENMM_EXPORT void registerKernelFactories() {

    for (int i = 0; i < Platform::getNumPlatforms(); i++) {

	Platform& platform = Platform::getPlatform(i);

	if (dynamic_cast<CpuPlatform*>(&platform) != NULL) {
	    CpuNastiKernelFactory* factory = new CpuNastiKernelFactory();
	    // register kernels
	    platform.registerKernelFactory(CalcNastiTorsionForceKernel::Name(), factory);
	    platform.registerKernelFactory(CalcNastiAngleForceKernel::Name(), factory);
        }
    }
}

extern "C" OPENMM_EXPORT void registerNastiCpuKernelFactories() {

    try {
        Platform::getPlatformByName("CPU");
    } catch (...) {
        Platform::registerPlatform(new CpuPlatform());
    }
    registerKernelFactories();
}

KernelImpl*
CpuNastiKernelFactory::createKernelImpl(std::string name,
					const Platform& platform,
					ContextImpl& context) const {

    CpuPlatform::PlatformData& data = CpuPlatform::getPlatformData(context);
    
    if (name == CalcNastiTorsionForceKernel::Name()) {
	return new CpuCalcNastiTorsionForceKernel(name, platform, data);
    }
    if (name == CalcNastiAngleForceKernel::Name()) {
    	return new CpuCalcNastiAngleForceKernel(name, platform, data);
    }
    
    throw OpenMMException((std::string("Tried to create kernel with illegal kernel name '")+name+"'").c_str());
}
