#ifndef OPENMM_CPUNASTIKERNELFACTORY_H_
#define OPENMM_CPUNASTIKERNELFACTORY_H_

#include "openmm/KernelFactory.h"

namespace OpenMM {

/**
 * This KernelFactory creates kernels for the cpu implementation of the Nasti plugin.
 */

class CpuNastiKernelFactory : public KernelFactory {
public:
    KernelImpl* createKernelImpl(std::string name,
				 const Platform& platform,
				 ContextImpl& context) const;
};

} // namespace OpenMM

#endif /*OPENMM_CPUNASTIKERNELFACTORY_H_*/
