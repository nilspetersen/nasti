import shutil
import os
import unittest
from nasti.fffactory import ForceFieldFactory
from nasti.secondaryStructure import SecondaryStructure
import simtk.openmm as mm
import simtk.unit as units

BPSEQ_SIMPLE_CHAIN = """
1 A 0
2 A 0
3 A 0
4 A 0
5 A 0
6 A 0
"""

BPSEQ_STR = """
0 A 0
1 A 0
2 G 18
3 G 17
4 A 0
5 A 0
6 C 16
7 C 15
8 G 13
9 A 0
10 A 0
11 A 0
12 A 0
13 C 8
14 A 0
15 G 7
16 G 6
17 C 3
18 C 2
"""

BPSEQ_TINY_HLX = """
1 G 9
2 G 8
3 G 7
4 A 0
5 A 0
6 A 0
7 C 3
8 C 2
9 C 1
"""


class TestForcefieldFactory(unittest.TestCase):

    """ test the ForceFieldFactory class """

    def setUp(self):

        """ init stuff """

        self.tmpdir = "tmp_fff"
        os.makedirs(self.tmpdir)

    def get_secondary_structure(self, bpseq_str):

        ''' create the secondary structure from a bpseq string '''

        # make the secondary structure
        fn_bpseq = os.path.join(self.tmpdir, "fff.bpseq")
        with open(fn_bpseq, "w") as f:
            f.write(bpseq_str)

        self.sec = SecondaryStructure()
        self.sec.read_bpseq(fn_bpseq)

    def prepare_system(self, bpseq_str):

        ''' prepare the system using the secondary
        structure in BPSEQ_STR '''

        self.get_secondary_structure(bpseq_str)

        fn_paramsfile = "data/fffactory/forcefield.yaml"
        self.fff = ForceFieldFactory(fn_paramsfile)
        self.fff.set_secondary_structure(self.sec)

        self.system = mm.System()
        for _ in self.sec:
            self.system.addParticle(0.325)

        self.fff.create_forcefield(self.system)
        self.forces = self.system.getForces()

    def tearDown(self):

        """ kill stuff """

        shutil.rmtree(self.tmpdir)

    def test_number_of_forces(self):

        """ are there enough forces? """

        self.prepare_system(BPSEQ_STR)
        self.assertEqual(len(self.forces), 12)

    def test_bonds(self):

        """ test the collected lists of distances, angles and dihedrals """

        self.prepare_system(BPSEQ_STR)
        bondforce = self.forces[0]
        self.assertEqual(bondforce.getNumBonds(), 23)

        for ii in range(18):
            i, j, bondlen, force_const = bondforce.getBondParameters(ii)
            bondlen = bondlen.value_in_unit(units.nanometer)
            force_const = force_const.value_in_unit(units.kilojoule / (units.nanometer**2*units.mole))
            self.assertEqual(i+1, j)

            if self.sec.ispaired(i) and self.sec.ispaired(j):
                # 11
                self.assertAlmostEqual(bondlen, 0.577)
                self.assertAlmostEqual(force_const, 3455.842)
            elif self.sec.ispaired(i):
                # 10
                self.assertAlmostEqual(bondlen, 0.579)
                self.assertAlmostEqual(force_const, 1072.806)
            elif self.sec.ispaired(j):
                # 01
                self.assertAlmostEqual(bondlen, 0.585)
                self.assertAlmostEqual(force_const, 802.825)
            else:
                # 00
                self.assertAlmostEqual(bondlen, 0.584)
                self.assertAlmostEqual(force_const, 544.966)

        for ii in range(18, 23):
            i, j, bondlen, force_const = bondforce.getBondParameters(ii)
            bondlen = bondlen.value_in_unit(units.nanometer)
            force_const = force_const.value_in_unit(units.kilojoule / (units.nanometer**2*units.mole))
            self.assertEqual(j, self.sec[i])
            self.assertAlmostEqual(bondlen, 1.362)
            self.assertAlmostEqual(force_const, 651.475)

    def test_simple_chain(self):

        ''' test number of forces for the rna chain without basepairs '''

        self.prepare_system(BPSEQ_SIMPLE_CHAIN)
        # number of forces
        self.assertEqual(self.system.getNumForces(), 5)
        # number of bonds
        self.assertEqual(self.forces[0].getNumBonds(), 5)
        # number of backbone angles
        # self.assertEqual(self.forces[1].getNumAngles(), 4)
        # number of helix angles
        self.assertEqual(self.forces[2].getNumAngles(), 0)
        # number of backbone dihedrals
        # self.assertEqual(self.forces[3].getNumTorsion(), 3)
        # TODO: check the parameters !

    def test_knight(self):

        ''' tiny test on knights '''

        self.get_secondary_structure(BPSEQ_TINY_HLX)

        fn_paramsfile = "data/fffactory/forcefield.yaml"
        self.fff = ForceFieldFactory(fn_paramsfile)
        self.fff.set_secondary_structure(self.sec)

        self.assertEqual(len(self.fff.get_dih_list_knights()), 3)


if __name__ == '__main__':
    unittest.main()
