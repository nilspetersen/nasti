import unittest
from nasti.structlist import StructList
import os
import shutil


class TestRnaC3Struct(unittest.TestCase):

    """ Test cases for the StructList class """

    def setUp(self):

        """ init stuff """

        self.name_tmp_dir = 'tmp'
        os.mkdir(self.name_tmp_dir)

    def tearDown(self):

        """ tidy up """

        shutil.rmtree(self.name_tmp_dir)

    def test_text_io(self):

        """ test reading and writing of text formatted lists """

        fn_bgsu_list = 'data/testlist.csv'
        fn_txt = os.path.join(self.name_tmp_dir, 'structlist.txt')

        structlist1 = StructList()
        structlist1.read_bgsu_list(fn_bgsu_list)
        structlist1.write_txt(fn_txt)

        self.assertEqual(len(structlist1), 6)

        structlist2 = StructList()
        structlist2.read_txt(fn_txt)

        self.assertEqual(len(structlist2), 6)
        self.assertEqual(structlist1, structlist2)


if __name__ == "__main__":
    unittest.main()
