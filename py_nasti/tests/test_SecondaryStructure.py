
import shutil
import os
import numpy as np
import unittest
from nasti.secondaryStructure import SecondaryStructure

HELIX_BPSEQ_STR = """
Filename: rhabarbar.bpseq
Organism: DSSR-derived secondary structure [yuyy]
Accession Number: 1
Citation: Rhabarbar
1 G 29
2 G 28
3 G 27
4 G 26
5 G 25
6 G 24
7 G 23
8 G 22
9 G 21
10 G 20 # a stupid comment
11 G 19
12 G 18
13 A 0
14 A 0
15 A 0
16 A 0
17 A 0
18 C 12
19 C 11
20 C 10
21 C 9
22 C 8
23 C 7
24 C 6
25 C 5
26 C 4
27 C 3
28 C 2
29 C 1
"""

SEC_2_BPSEQ_STR = """
1 A 0
2 G 11
3 G 10
4 C 8
5 A 0
6 A 0
7 A 0
8 G 4
9 A 0
10 C 3
11 C 2
12 A 0
13 G 21
14 G 20
15 G 19
16 A 0
17 A 0
18 A 0
19 C 15
20 C 14
21 C 13
22 A 0
"""


class TestSecondaryStructure(unittest.TestCase):

    """ test class for SecondaryStructure class """

    def setUp(self):

        """ set it up """

        self.tmpdir = "tmp_testsecstruct"
        os.makedirs(self.tmpdir)

    def tearDown(self):

        """ tidy up """

        shutil.rmtree(self.tmpdir)

    def _read_secstruct(self, bpseq_str):

        fn = os.path.join(self.tmpdir, "sec2.bpseq")
        with open(fn, "w") as f:
            f.write(bpseq_str)
        sec = SecondaryStructure()
        sec.read_bpseq(fn)
        return sec

    def test_io(self):

        ''' test reading and writing of rna secondary structures '''

        sec = self._read_secstruct(HELIX_BPSEQ_STR)

        control_arr = np.array([29, 28, 27, 26,
                                25, 24, 23, 22, 21,
                                20, 19, 18, 0, 0, 0,
                                0, 0, 12, 11, 10, 9,
                                8, 7, 6, 5, 4, 3, 2, 1]) - 1

        control_seq = np.array(["G"] * 12 + ['A'] * 5 + ['C'] * 12)
        control_is_paired = np.ones(len(control_arr), dtype=int)
        control_is_paired[12:17] = 0

        for i, j in enumerate(sec):

            self.assertEqual(j, control_arr[i])

        self.assertTrue((sec.as_array() == control_arr).all())
        self.assertTrue((sec.get_sequence() == control_seq).all())
        self.assertTrue((sec.ispaired_int_array() == control_is_paired).all())
        self.assertTrue(sec.ispaired(0))
        self.assertFalse(sec.ispaired(15))

        fn_out = os.path.join(self.tmpdir, "helix_out.bpseq")
        sec.tofile(fn_out)

        sec2 = SecondaryStructure()
        sec2.read_bpseq(fn_out)

        self.assertEqual(sec, sec2)

    def test_helix_assignment(self):

        ''' test the method assign_helices, which gives each nucleotide
        a number for which helix it belongs to '''

        sec = self._read_secstruct(SEC_2_BPSEQ_STR)

        control_helices = np.array([0, 1, 1, 2, 0, 0, 0, 2, 0, 1, 1,
                                    0, 3, 3, 3, 0, 0, 0, 3, 3, 3, 0])
        helices = sec.assign_helices()

        self.assertTrue((helices == control_helices).all())

    def test_basepair_count(self):

        ''' test the count_pairs_in_region method '''

        sec = self._read_secstruct(SEC_2_BPSEQ_STR)

        self.assertEqual(sec.count_pairs_in_region(0, 5), 3)
        self.assertEqual(sec.count_pairs_in_region(12, 3), 3)
        self.assertEqual(sec.count_pairs_in_region(4, 3), 0)
        self.assertEqual(sec.count_pairs_in_region(21, 1), 0)
        self.assertEqual(sec.count_pairs_in_region(21, 15), 0)
        self.assertEqual(sec.count_pairs_in_region(9, 10), 6)


if __name__ == "__main__":
    unittest.main()
