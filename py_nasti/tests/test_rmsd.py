
import numpy as np
import unittest

from nasti.rmsd.rmsdfit import RmsdFit


class TestGeometries(unittest.TestCase):

    """ Test class for the computation of rmsd and gdt_ts """

    def setUp(self):

        """ init stuff """

        pass

    def tearDown(self):

        """ tidy up """

        pass

    def test_equals(self):

        """ make a simple superposition of equal structures """

        a = np.array([[1, 1, 1],
                      [1, 2, 1],
                      [1, 2, 2],
                      [2, 2, 2]], dtype=np.float64)
        b = np.array([[1, 3, 1],
                      [1, 3, 2],
                      [1, 2, 2],
                      [2, 2, 2]], dtype=np.float64)

        fitter = RmsdFit()
        rmsd, fitted_coordinates = fitter.fit(a, b)

        self.assertAlmostEqual((a - fitted_coordinates).sum(), 0)
        self.assertAlmostEqual(rmsd, 0, places=6)
        
        gdt_ts2 = fitter.gdt_ts_fit(a, b)
        self.assertAlmostEqual(gdt_ts2, 1)

    def test_stretch(self):

        """ make a superposition of two unsimilar structures """

        a = np.array([[1, 1, 1],
                      [1, 2, 1],
                      [1, 2, 2],
                      [2, 2, 2]], dtype=np.float64)

        b = a * 1000.

        fitter = RmsdFit()
        rmsd, fitted_coordinates = fitter.fit(a, b)

        gdt_ts2 = fitter.gdt_ts_fit(a, b)
        self.assertAlmostEqual(gdt_ts2, 0)

    def test_gdtts(self):

        a = np.array([[1, 1, 1],
                      [1, 2, 1],
                      [1, 2, 2],
                      [2, 2, 2],
                      [2, 2, 3],
                      [2, 3, 3],
                      [2, 3, 4],
                      [3, 3, 4],
                      [3, 4, 4],
                      [4, 4, 4]], dtype=np.float64)

        b = np.copy(a)

        fitter = RmsdFit()
        gdt_ts2 = fitter.gdt_ts_fit(a, b)
        self.assertAlmostEqual(gdt_ts2, 1.0)

        b[-1] *= 200
        gdt_ts2 = fitter.gdt_ts_fit(a, b)
        self.assertAlmostEqual(gdt_ts2, 0.9)

        b[1] *= 1200
        gdt_ts2 = fitter.gdt_ts_fit(a, b)
        self.assertAlmostEqual(gdt_ts2, 0.8)

        a /= 10
        b /= 10

        gdt_ts2 = fitter.gdt_ts_fit(a, b)
        self.assertAlmostEqual(gdt_ts2, 0.8)


if __name__ == "__main__":
    unittest.main()
