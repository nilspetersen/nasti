
import unittest
from nasti.rnaC3struct import RnaC3Struct
import numpy as np
import os
import shutil


class TestRnaC3Struct(unittest.TestCase):

    """ Test cases for the RnaC3Struct class """

    def setUp(self):

        """ init stuff """

        self.name_tmp_dir = 'tmp'
        os.mkdir(self.name_tmp_dir)

    def tearDown(self):

        """ tidy up """

        shutil.rmtree(self.name_tmp_dir)

    def test_binary_io(self):

        """ test the writing and reading of coordinates in a binary file format """

        pdb_fname = 'data/pdbs/1tra.pdb'
        chain_id = 'A'
        fn = os.path.join(self.name_tmp_dir, 'rna.npz')

        rna = RnaC3Struct()
        rna.parse_pdb(pdb_fname, chain_id)
        # also load secondary structure - with and without secondary structure ???

        rna.save_binary(fn)
        rna2 = RnaC3Struct()
        rna2.load_binary(fn)

        self.assertEqual(rna, rna2)

        fn2 = os.path.join(self.name_tmp_dir, 'rna_sec.npz')
        rna.read_sec_struct_pdb(pdb_fname)
        rna.save_binary(fn2)
        rna3 = RnaC3Struct()
        rna3.load_binary(fn2)
        self.assertEqual(rna, rna3)

    def test_backbone_distance(self):

        """ backbone distances on a simple artificial chain """

        # backbone structure:
        # - or | = 0.5 length here
        #
        # *--*----*
        #         |
        #         | *
        #         |/
        #         *

        rna = RnaC3Struct()
        rna.coords = np.array([
            [0,  0,   0],
            [0,  0,   1],
            [0,  0,   3],
            [0, -1.5, 3],
            [0, -1,   3.5]], dtype=float)

        rna.chain_breaks = np.array([False] * 5)
        rna.is_paired = np.array([0,0,1,1,0])

        expected_dist = [1, 2, 1.5, np.sqrt(2*0.5**2)]
        type_indices = [0, 2, 3, 1]

        distances, labels = rna.bb_distances()

        for i, exp_dist in enumerate(expected_dist):
            dist = distances[type_indices[i]][0]
            self.assertAlmostEqual(dist, exp_dist)

        # introduce a chain breack for the second and the last nucleotide
        rna.chain_breaks[1] = True
        rna.chain_breaks[-1] = True
        distances, labels = rna.bb_distances()

        del expected_dist[1]
        missing_i = type_indices[1]
        del type_indices[1]

        for i, exp_dist in enumerate(expected_dist):
            dist = distances[type_indices[i]][0]
            self.assertAlmostEqual(dist, exp_dist)
        # assert the missing distance is missing
        self.assertEqual(len(distances[missing_i]), 0)

    def test_backbone_angle(self):

        """ backbone angles on a simple artificial chain """

        # backbone structure (it's 3D, # is one layer closer):
        #       *
        #      /|
        #     * *-#-#
        #     |     |
        # *-*-*     #
        #           |
        #           #

        rna = RnaC3Struct()
        rna.coords = np.array([
            [0,0,0],
            [0,0,1],
            [0,0,2],
            [0,1,2],
            [0,2,3],
            [0,1,3],
            [1,1,4],
            [1,1,5],
            [1,0,5],
            [1,-1,5]], dtype=float)

        rna.chain_breaks = np.array([False] * 10)
        rna.is_paired = np.array([0,0,0,1,0,1,1,1,0,0])

        expected_ang = np.radians(np.array([180, 90, 135, 45, 90, 135, 90, 180]))
        type_indices = np.array([0, 4, 2, 5, 6, 7, 3, 1])

        angles, labels = rna.bb_angles()

        for i, exp_ang in enumerate(expected_ang):
            ang = angles[type_indices[i]][0]
            self.assertAlmostEqual(ang, exp_ang)

        # add a chain break, now the second and the third angle should be missing
        rna.chain_breaks[2] = True
        expected_ang = np.radians(np.array([180, 45, 90, 135, 90, 180]))
        type_indices = np.array([0, 5, 6, 7, 3, 1])
        angles, labels = rna.bb_angles()
        # make sure all angles except for the second are still there
        for i, exp_ang in enumerate(expected_ang):
            ang = angles[type_indices[i]][0]
            self.assertAlmostEqual(ang, exp_ang)
        # assert that there no entry for 001 = index 2 anymore!
        self.assertEqual(len(angles[4]), 0)
        self.assertEqual(len(angles[2]), 0)

    def test_backbone_dihedral(self):

        """ backbone dihedrals on a simple artificial chain """

        # backbone structure (z-level: * = 0, # = -1, @ = -2)
        #
        #   @
        #   |
        #   @-@
        #     |
        #     @
        #     A
        #     #-#
        #       A
        # *-* *-*
        #   | |
        #   *-*

        rna = RnaC3Struct()
        rna.coords = np.array([
            [ 0, 0, 0],
            [ 1, 0, 0],
            [ 1,-1, 0],
            [ 2,-1, 0],
            [ 2, 0, 0],
            [ 3, 0, 0],
            [ 3, 0,-1],
            [ 2, 0,-1],
            [ 2, 0,-2],
            [ 2, 1,-2],
            [ 1, 1,-2],
            [ 1, 2, 2]], dtype=float)

        rna.chain_breaks = np.array([False] * 12)
        rna.is_paired = np.array([0,0,0,0,1,1,1,1,0,1,0,1])

        type_indices = [0,8,12,14,15,7,11,5,10]

        expected_dih = [3.1415926535897931,
                        0.0, 3.1415926535897931,
                        1.5707963267948966,
                        0.0, 3.1415926535897931,
                        -1.5707963267948966,
                        -1.5707963267948966,
                        1.8157749899217608]

        # compute dihedrals and check correctness
        dihedrals, labels = rna.bb_dihedrals()
        for i, exp_dih in enumerate(expected_dih):
            dih = dihedrals[type_indices[i]][0]
            self.assertAlmostEqual(dih, exp_dih)

        # retry with chain breaks !
        rna.chain_breaks[3] = True
        rna.chain_breaks[-1] = True
        # remove 3 dihedrals
        missing_i = []
        for i in range(3):
            del expected_dih[1]
            missing_i.append(type_indices[1])
            del type_indices[1]

        # compute dihedrals and check correctness
        dihedrals, labels = rna.bb_dihedrals()
        for i, exp_dih in enumerate(expected_dih):
            dih = dihedrals[type_indices[i]][0]
            self.assertAlmostEqual(dih, exp_dih)

        for i in missing_i:
            self.assertEqual(len(dihedrals[i]), 0)

    def test_helix_geometries(self):

        """ test the computation of angles and dihedrals in helix regions on an artificial helix """

        # simple helix structure
        #    *
        #   / \
        #   * *  no basepair here!
        #   | |
        #   *-*
        #   | |
        #   *-*
        #   | |
        # *-*-*

        rna = RnaC3Struct()
        rna.coords = np.array([
            [ 0,   0, 0],

            [ 1,   0, 0],
            [ 1,   1, 0],
            [ 1,   2, 0],

            [ 1,   3, 0],
            [ 1.5, 3, 0],
            [ 2,   3, 0],

            [ 2, 2, 0],
            [ 2, 1, 0],
            [ 2, 0, 0]], dtype=float)

        rna.chain_breaks = np.array([False] * 10)
        rna.sec = [-1,9,8,7,-1,-1,-1,3,2,1]
        rna.is_paired = np.array([0,1,1,1,0,0,0,1,1,1])

        # compute stuff
        bp_dist, angles, dihedrals, labels = rna.hlx_dist_angles_dihedrals()
        # all distances should be one, there should be 3 distances
        self.assertEqual(len(bp_dist), 3)
        for l in bp_dist:
            self.assertAlmostEqual(l, 1)
        # check if all angles are pi/2
        for ang_list in angles:
            for ang in ang_list:
                self.assertAlmostEqual(ang, np.pi/2)
        # check if all dihedrals are 0
        for dih_list in dihedrals:
            for dih in dih_list:
                self.assertAlmostEqual(dih, 0)

        # there should be:
        # 1*110
        # 2*111
        # 4*11
        # 1*10
        # angles and dihedrals

        # 10 (1)
        self.assertEqual(len(angles[0]), 1)
        self.assertEqual(len(dihedrals[0]), 1)
        # 11 (4)
        self.assertEqual(len(angles[1]),4)
        self.assertEqual(len(dihedrals[1]),4)
        # 110 (1)
        self.assertEqual(len(angles[2]), 1)
        self.assertEqual(len(dihedrals[2]), 1)
        # 111 (2)
        self.assertEqual(len(angles[3]), 2)
        self.assertEqual(len(dihedrals[3]), 2)

        # add a chain break at position 1 (the second nucleotide)
        rna.chain_breaks[1] = True
        rna.chain_breaks[-1] = True
        bp_dist, angles, dihedrals, labels = rna.hlx_dist_angles_dihedrals()
        # all distances should be one, there should be 3 distances
        self.assertEqual(len(bp_dist), 3)
        for l in bp_dist:
            self.assertAlmostEqual(l, 1)
        # check if all angles are pi/2
        for ang_list in angles:
            for ang in ang_list:
                self.assertAlmostEqual(ang, np.pi/2)
        # check if all dihedrals are 0
        for dih_list in dihedrals:
            for dih in dih_list:
                self.assertAlmostEqual(dih, 0)

        # now there should be:
        # 1*110
        # 0*111
        # 2*11
        # 1*10
        # angles and dihedrals

        # 10 (1)
        self.assertEqual(len(angles[0]), 1)
        self.assertEqual(len(dihedrals[0]), 1)
        # 11 (2)
        self.assertEqual(len(angles[1]),2)
        self.assertEqual(len(dihedrals[1]),2)
        # 110 (1)
        self.assertEqual(len(angles[2]), 1)
        self.assertEqual(len(dihedrals[2]), 1)
        # 111 (0)
        self.assertEqual(len(angles[3]), 0)
        self.assertEqual(len(dihedrals[3]), 0)

    def test_pairwise_distances1(self):

        """ test the pairwise distance computation with a minimal example """

        # super simple backbone structure (0 and 1 indicate basepairs)
        # 0-1-1-1-0-1

        rna = RnaC3Struct()
        rna.coords = np.array([
            [0, 0, 0],
            [1, 0, 0],
            [2, 0, 0],
            [3, 0, 0],
            [4, 0, 0],
            [5, 0, 0]], dtype=float)

        rna.chain_breaks = np.array([False] * 6)
        rna.is_paired = np.array([0,1,1,1,0,1])

        dist, labels = rna.pairwise_distances()

        for d in dist:
            self.assertEqual(len(d), 1)

        self.assertAlmostEqual(dist[0][0], 4)
        self.assertAlmostEqual(dist[1][0], 5)
        self.assertAlmostEqual(dist[2][0], 4)

    def test_pairwise_distances2(self):

        """ test the pairwise distance computation with a very small example """

        # super simple backbone structure (0 and 1 indicate basepairs)
        # 0-1-1-1-0-1-1

        rna = RnaC3Struct()
        rna.coords = np.array([
            [0, 0, 0],
            [1, 0, 0],
            [2, 0, 0],
            [3, 0, 0],
            [4, 0, 0],
            [5, 0, 0],
            [6, 0, 0]], dtype=float)

        rna.chain_breaks = np.array([False] * 7)
        rna.is_paired = np.array([0,1,1,1,0,1,1])

        dist, labels = rna.pairwise_distances()

        self.assertEqual(len(dist[0]), 1)
        self.assertEqual(len(dist[1]), 2)
        self.assertEqual(len(dist[2]), 3)

        self.assertAlmostEqual(dist[0][0], 4)
        self.assertAlmostEqual(dist[1][0], 5)
        self.assertAlmostEqual(dist[1][1], 6)
        self.assertAlmostEqual(dist[2][0], 4)
        self.assertAlmostEqual(dist[2][1], 5)
        self.assertAlmostEqual(dist[2][2], 4)


if __name__ == "__main__":
    unittest.main()
