from setuptools import setup


setup(
    name='nasti',
    version='0.1.0',
    packages=['nasti', 'nasti.rmsd'],
    package_data={'nasti': ['nt_map.txt'], 'nasti.rmsd': ['rmsd.so']},
    include_package_data=True,
    description='NAST Improved',
    author='Nils Petersen',
    author_email='petersen@zbh.uni-hamburg.de',
    entry_points={'console_scripts': [
        'nasti = nasti.main_simulation:main',
        'nasti2pdb = nasti.main_utils:nasti2pdb',
        'nastiRmsd = nasti.main_utils:nastiRmsd',
        'nastiC3struct = nasti.main_utils:nastiC3struct',
        'nastiBinStruct = nasti.main_utils:nastiBinStruct'
    ]})
