import nastiplugin
import os
from nasti.simulator import Simulation
from nasti.fffactory import ForceFieldFactory
from nasti.secondaryStructure import SecondaryStructure
from nasti.rnaC3struct import RnaC3Struct
from nasti.simopt import SimOptDict, SimPar
from nasti.ru_main import build_random_unfolded
from nasti.utils import mkdir_safe


def simulate(simopt):

    """ do it """

    par = SimPar(simopt)

    verbose = par.verbose

    if verbose:
        # TODO: printoptions !!!
        pass

    # initializations
    write_trajectory = par.snapshot_interval > 0

    # read the secondary structure
    sec = SecondaryStructure()
    sec.read_bpseq(par.fn_bpseq)

    # create the force field
    if par.ff_type == 'nasti':
        fff = ForceFieldFactory(par.ff_paramsfile)
    else:
        msg = "force field type {} not supported".format(par.ff_type)
        raise RuntimeError(msg)

    fff.set_secondary_structure(sec)
    bondlen = fff.get_bb_bondlen()

    if par.fn_tertiary:
        fff.read_tertiary_interactions(par.fn_tertiary)

    # create the output directory
    mkdir_safe(par.outdir)

    # initialize the structure
    rna = RnaC3Struct()
    rna.init_from_secondary_structure(sec, bondlen, 1)

    if par.start_conformation == 'pdb':
        rna.parse_pdb(par.fn_pdb, par.chain_id)
        start_coords = rna.get_coords()
    elif par.start_conformation == 'random_unfold':
        filenames_unfolded = build_random_unfolded(par.runfold_options)

    # run the simulations
    for seed in par.seeds:

        if verbose:
            print "NASTI SIMULATION WITH SEED={}".format(seed)

        casedir = os.path.join(par.outdir, str(seed))

        if par.start_conformation == 'circle':
            rna.make_circle(bondlen, 0.01, seed)
        elif par.start_conformation == 'pdb':
            rna.set_coords(start_coords)
        elif par.start_conformation == 'random_unfold':
            rna.load_binary(filenames_unfolded[seed])
        else:
            msg = "{} is not a valid start-conformation".format(par.start_conformation)
            raise RuntimeError(msg)

        sim = Simulation(
            rna_c3_struct=rna,
            fff=fff,
            seed=seed,
            outdir=casedir,
            write_trajectory=write_trajectory,
            verbose=verbose,
            cmm_removal_frequency=par.cmm_removal_frequency,
            preferred_platform=par.preferred_platform,
            preferred_gpu=par.gpu_number,
            timestep=par.timestep
        )

        sim.run(
            steps_eq=par.steps_eq,
            steps_iter_eq=par.snapshot_interval,
            steps_prod=par.steps_prod,
            steps_iter_prod=par.snapshot_interval,
            use_thermostat=par.use_thermostat,
            thermo_coupling=par.thermo_coupling
        )


def main():

    import argparse
    parser = argparse.ArgumentParser(prog="NASTI")
    parser.add_argument("config_file")
    parser.add_argument("--seeds__from", help="start seed", type=int)
    parser.add_argument("--seeds__to", help="end seed", type=int)
    args = parser.parse_args()

    simopt = SimOptDict(args)
    simulate(simopt)


if __name__ == '__main__':
    main()
