#!/usr/bin/env python

import numpy as np
import re
import os
import copy

from nasti.geometry import angles, dihedrals
from nasti.secondaryStructure import SecondaryStructure
from nasti.rmsd.rmsdfit import RmsdFit
from nasti.geotypes import Geotypes


class NucleotideMap:

    def __init__(self, fname=None):

        """ starts with reading the nucleotide map file """

        self._mapmap = {
            'tu': 'U', 'u': 'U', 't': 'U',
            'a': 'A', 'g': 'G', 'c': 'C'}
        self.nt_map = {}
        if fname is None:
            fname = os.path.join(os.path.dirname(os.path.realpath(__file__)), "nt_map.txt")
        self._read(fname)

    def _read(self, fname):

        """ reads the nucleotide map file """

        with open(fname) as f:
            for line in f:
                line = line.strip()
                entries = line.split()
                l = len(entries)
                if l != 2:
                    err_msg = "A line in the Nucleotide-Map file {} has {} entries but should have only 2!".format(fname, l)
                    raise RuntimeError(err_msg)
                if entries[1] in self._mapmap:
                    self.nt_map[entries[0]] = self._mapmap[entries[1]]
                else:
                    self.nt_map[entries[0]] = entries[1]

    def __getitem__(self, key):

        """ get the replacement for a special nucleotide """

        return self.nt_map[key]

    def __str__(self):

        """ get a string representation """

        s = ""
        for key in self.nt_map.iterkeys():
            s += "{} {}\n".format(key, self.nt_map[key])
        return s


class TempRes:

    """ tiny helper class for parsing the c3 struct """

    def __init__(self):

        """ init """

        self.p = None
        self.o3 = None
        self.c3 = None
        self.res_id = None
        self.res_type = None


class RnaC3Struct:

    """ class for coarse grained RNA structures as used in NAST """

    def __init__(self):

        """ initialize an empty structure """

        self.seq = None
        self.sec = SecondaryStructure()
        self.is_paired = None
        self.coords = None
        self.res_id = None
        self.res_indices = None  # res_id => sequence-index
        self.chain_breaks = None
        self.chain_id = None
        # trajectory
        self.f_traj_out = None  # file
        self.traj_in = None  # memory map
        self.traj_dt = np.float64
        self.traj_size = None
        self.rmsdfit = None
        self.geo = Geotypes()
        self.atom_name_write = "C3'"
        self.traj_maxlen = None
        self.use_chimera_filewrite_hack = False

    def init_pdb(self, fname_pdb, chain_id, model_i=1):

        """ initialize structure by reading a pdb file and parsing the base pairs """

        self.parse_pdb(fname_pdb, chain_id, model_i)
        self.read_sec_struct_pdb(fname_pdb)

    def set_atom_name_write(self, atom_name_write):

        """ change this to print atoms with a different label, 
        keep in mind that the true atom name is still C3', 
        this is just a hack to view the model-ensembles in chimera """

        self.atom_name_write = atom_name_write

    def write_chimera_style(self):

        """ write a trajectory that allows visualization in chimera,
        qick and dirty hack! will change the sequence the object! """

        self.seq = ['  A'] * len(self)
        self.res_id = [str(i + 1) for i in range(len(self))]
        self.set_atom_name_write("  P")
        self.use_chimera_filewrite_hack = True

    def set_traj_maxlen(self, traj_maxlen):

        """ required for random unfold trajectories, written binary trajectory
        files are filled with [0,0,0] coordinates to traj_max_len """

        self.traj_maxlen = traj_maxlen

    def parse_pdb(self, fname, chain_id, model_i=1):

        """ parse a structure from a pdbfile """

        self.chain_id = chain_id
        chain_break_thresh = 2
        rx_atoms = re.compile("^(((C3)|(O3))[*'])|(P)$")

        with open(fname) as f:

            # find the correct model !
            rx_model = re.compile("MODEL\s+(\d+)")
            if model_i > 1:
                for l in f:
                    match_obj = re.match(rx_model, l)
                    if match_obj:
                        if model_i ==  int(match_obj.groups()[0]):
                            break

            residues = []
            temp_res = TempRes()
            res_set = set()

            # collect the coordinates
            for l in f:

                l = l.strip()

                # break if the next model is reached
                match_obj = re.match(rx_model, l)
                if match_obj:
                    if model_i != int(match_obj.groups()[0]):
                        break

                # only collect atoms
                if len(l) < 53:
                    continue
                if not (l[:4] == "ATOM" or l[:6] == "HETATM"):
                    continue

                # correct chain ?
                chain = l[21:22].strip()
                if chain != chain_id:
                    continue

                # interesting atom ?
                atom = l[12:16].strip()
                mo = re.match(rx_atoms, atom)
                if mo is None:
                    continue

                res = l[22:27].strip()  # residue number with insertion code
                res_type = l[17:20].strip()

                x = float(l[30:38])
                y = float(l[38:46])
                z = float(l[46:54])

                # is this a new residue ?
                if temp_res.res_id != res:
                    if res in res_set:
                        continue
                    res_set.add(res)
                    if temp_res.res_id is not None:
                        residues.append(temp_res)
                        temp_res = TempRes()
                    temp_res.res_id = res
                    temp_res.res_type = res_type

                # which atom is this ?
                if mo.groups()[4]:  # it's P
                    temp_res.p = np.array([x, y, z])
                elif mo.groups()[3]:  # it's O3
                    temp_res.o3 = np.array([x, y, z])
                elif mo.groups()[2]:  # it's C3
                    temp_res.c3 = np.array([x, y, z])

            # add the last residue
            residues.append(temp_res)

        self.seq = []
        self.coords = []
        self.res_id = []
        self.chain_breaks = []

        # iterate through the residues and search for chain breaks !
        for i, temp_res in enumerate(residues):
            # cannot use residues without c3
            if temp_res.c3 is None:
                continue
            self.coords.append(temp_res.c3)
            self.seq.append(temp_res.res_type)
            self.res_id.append(temp_res.res_id)
            # check if there is a chain break
            chainbreak = True
            if i < len(residues)-1:
                res_next = residues[i+1]
                if (temp_res.o3 is not None) and (res_next.p is not None) and (res_next.c3 is not None):
                    dist = np.sqrt(((temp_res.o3 - res_next.p)**2).sum())
                    if dist <= chain_break_thresh:
                        chainbreak = False
            self.chain_breaks.append(chainbreak)

        # make a dictionairy: res_id => chain-index
        self.res_indices = {}
        for i, res_id in enumerate(self.res_id):
            self.res_indices[res_id] = i

        self.seq = np.array(self.seq)
        self.res_id = np.array(self.res_id)
        self.chain_breaks = np.array(self.chain_breaks)
        self.coords = np.array(self.coords)
        self.coords /= 10.  # angstrom to nanometer
        if(len(self.sec) == 0):
            self.sec.set_sequence(self.seq)

    def get_n_chainbreaks(self):

        """ get the number of breaks within the RNA chain """

        return self.chain_breaks.sum()

    def get_coords(self):

        """ get the coordinates as n*3 numpy.array in nanometers """

        return self.coords

    def set_coords(self, new_coords):

        """ set the coordinates in nanometers as n*3 numpy.array """

        self.coords = new_coords

    def remove_centre_of_mass(self):

        """ remove the centre of mass from the coords in the structure """

        com = self.coords.mean(axis=0)
        self.coords = self.coords.copy() - com

    def simplify_sequence(self, fname_nucleotide_map):

        """ map all weird residues in the sequence to simple ones """

        nt_map = NucleotideMap(fname_nucleotide_map)

        for i, base in enumerate(self.seq):
            self.seq[i] = nt_map[base]

    def _is_a_good_basepair(self, bp):

        """ check criterion for a base pair to be considered a pair in this context """

        if bp.Edges in ['+/+', '-/-']:
            return True
        elif bp.Edges == 'W/W' and bp.Orientation == 'cis':
            return True
        return False

    def _set_paired(self, base):

        """ set a sequence position as paired if the base can be found in the chain """

        if self.chain_id != base.ChainId:
            return -1

        i = self.res_indices[base.ResId]
        self.is_paired[i] = 1
        return i

    def _assert_chain_id(self):

        """ makes sure a chain id is given """

        if self.chain_id is None:
            raise RuntimeError("chain_id is None! load a structure first!")

    def rnaview_basepairs(self, fname_pdb):

        """ get bp-interactions by running rnaview and parsing the output,
        requires pycogent ! """

        from cogent.app.rnaview import RnaView
        from cogent.parse.rnaview import RnaviewParser

        basepairs = None
        # hack: change to the directory and
        # back (else rnaview caused a segmentation fault in at least one case)
        path, fn = os.path.split(os.path.abspath(fname_pdb))
        cwd_path = os.getcwd()

        try:
            # change the directory
            os.chdir(path)
            # use rnaview
            rnaview_dict = RnaView()(fn)
            rnaview_bp_file = rnaview_dict["base_pairs"]
            basepairs = RnaviewParser(rnaview_bp_file)["BP"]
            # change go back to original directory
            os.chdir(cwd_path)
        except:
            raise RuntimeError("Failed to excecute and parse "
                               "base pairs from {} using the "
                               "RNAView interface".format(fname_pdb))
        finally:
            os.remove(os.path.join(path, "base_pair_statistics.out"))
            try:
                os.remove(fname_pdb + ".out")
            except OSError:
                os.remove(fname_pdb + "_nmr.pdb")
                os.remove(fname_pdb + "_nmr.pdb.out")

        return basepairs

    def read_sec_struct_pdb(self, fname_pdb):

        """ get a secondary structure from a pdb-file using RNAView """

        self._assert_chain_id()
        basepairs = self.rnaview_basepairs(fname_pdb)

        n = len(self.coords)
        self.is_paired = np.zeros(n, dtype=int)

        for bp in basepairs:
            if self._is_a_good_basepair(bp):

                try:
                    i = self._set_paired(bp.Up)
                except KeyError:
                    i = -1
                    print("Key Error basepair "
                          "_{}_ {} ({} {})".format(bp.Up.ResId,
                                                   bp.Down.ResId,
                                                   fname_pdb,
                                                   self.chain_id))

                try:
                    j = self._set_paired(bp.Down)
                except KeyError:
                    j = -1
                    print("Key Error basepair "
                          "{} _{}_ ({} {})".format(bp.Up.ResId,
                                                   bp.Down.ResId,
                                                   fname_pdb,
                                                   self.chain_id))

                if i > -1 and j > -1:
                    self.sec[i] = j

    def _set_secondary_structure(self, sec):

        """ set the secondary structure """

        self.sec = sec
        self.seq = self.sec.get_sequence()
        self.is_paired = self.sec.ispaired_int_array()
        self.chain_breaks = np.zeros(len(self.seq), dtype=np.bool)

    def init_from_secondary_structure(self, sec, bond_dist, seed, shape="circle"):

        """ initialize structure from SecondaryStructure object
        and set the initial shape """

        self._set_secondary_structure(sec)

        self.chain_breaks = np.zeros(len(self.seq), dtype=np.bool)

        self.coords = np.zeros((len(self.seq), 3))
        if shape == "circle":
            self.make_circle(bond_dist, 0.01, seed)
        else:
            raise RuntimeError("shape '{}' not supported".format(shape))
        self.res_id = np.arange(1, len(self) + 1).astype(np.string_)

    def set_secondary_structure_and_coords(self, sec, coords):

        """ set the secondary structure and some coordinates simulatenously """

        if len(sec) != len(coords):
            msg = "lengths of secondary structure and coordinates don't match"
            raise RuntimeError(msg)

        self._set_secondary_structure(sec)
        self.set_coords(coords)
        self.res_id = np.arange(len(self)).astype(np.string_)

    def make_circle(self, bond_distance, sigma_noise, rand_seed):

        """ arange particles in a circle with some noise """

        n = self.coords.shape[0]
        if not self.coords.flags.writeable:
            self.coords = np.zeros(self.coords.shape)
        nf = float(n)
        circle_radius = (bond_distance * n) / (2 * np.pi)

        for i in range(n):
            self.coords[i][0] = circle_radius * np.cos(2 * np.pi * i / nf)
            self.coords[i][1] = circle_radius * np.sin(2 * np.pi * i / nf)
            self.coords[i][2] = 0.

        if sigma_noise > 0.:
            np.random.seed(rand_seed)
            self.coords += np.random.normal(scale=sigma_noise,
                                            size=self.coords.shape)

    def get_sec(self):

        """ get the secondary structure as numpy array """

        return self.sec

    def _pdb_line(self, i, index_shift=0):

        coords = self.coords * 10
        pdb_string = ""
        try:
            resnum = int(self.res_id[i])
            icode = " "
        except ValueError:
            resnum = int(self.res_id[i][:-1])
            icode = self.res_id[i][-1]
        pdb_string += "ATOM   {:4d}  {:3s} {:3s}  {:4d}{}   ".format(i + 1 - index_shift,
                                                                     self.atom_name_write,
                                                                     self.seq[i],
                                                                     resnum - index_shift,
                                                                     icode)
        if len(self) > 500:
            pdb_string += "{:8.2f}{:8.2f}{:8.2f}\n".format(*coords[i])
        else:
            pdb_string += "{:8.3f}{:8.3f}{:8.3f}\n".format(*coords[i])
        if self.chain_breaks[i]:
            pdb_string += "TER\n"
        return pdb_string


    def _pdb_string(self, model_number=None):

        """ get a .pdb string """

        pdb_string = ""
        if self.use_chimera_filewrite_hack:
            pdb_string += self._pdb_line(0, 1)
        pdb_string += ''.join(self._pdb_line(i) for i in range(len(self)))
        if model_number is not None:
            pdb_string = "MODEL {:8d}\n{}ENDMDL\n".format(model_number, pdb_string)

        return pdb_string

    def save_pdb(self, fname):

        """ write the structure to a .pdb file """

        with open(fname, "w") as f:
            f.write(self._pdb_string())

    def _write_pdb_traj(self, fname, interval=1):

        """ write a trajectory in .pdb format """

        with open(fname, "w") as f:
            for i in range(0, self.get_trajectory_size(), interval):
                self.load_trajectory_snapshot(i)
                self.remove_centre_of_mass()
                f.write(self._pdb_string(i + 1))

    def _get_psf_string(self, mass):

        """Make X-PLOR format PSF string. """
        # TODO: add basepairs ?

        n_particles = len(self)

        psf_str = "PSF\n\n"
        psf_str += "       1 !NTITLE\n"
        psf_str += " REMARKS generated by nasti\n\n"

        psf_str += "%8d !NATOM\n" % n_particles

        for i in range(n_particles):
            psf_str += "%8d MAIN %-4d %3s %3s %17.6f %14.6f  0\n" % (i + 1,
                                                                     i + 1,
                                                                     self.seq[i],
                                                                     self.atom_name_write,
                                                                     0.0,
                                                                     mass)

        block_str = ""
        line_str = ""
        n_bonds = 0

        for i in range(n_particles-1):
            if self.chain_breaks[i]:
                continue

            line_str += " {:7d} {:7d}".format(i+1, i+2)
            n_bonds += 1

            if len(line_str.split()) >= 8:
                block_str += line_str+"\n"
                line_str = ""

        if len(line_str.split()) > 0:
            block_str += line_str + "\n"

        psf_str += "\n{:8d} !NBOND: bonds\n".format(n_bonds)
        psf_str += block_str
        psf_str += "\n\n"

        return psf_str

    def _write_psf_file(self, fname, mass=325.):

        """ write a psf file for a trajectory """

        with open(fname, "w") as f:
            f.write(self._get_psf_string(mass))

    def write_pdb_trajectory(self, name, outdir, interval=1):

        """ write a trajectory in pdb style """

        self._write_psf_file(os.path.join(outdir, name + ".psf"))
        self._write_pdb_traj(os.path.join(outdir, name + ".pdb"), interval)

    def __eq__(self, other):

        """ compare two structures if they are equal """

        return (
            (self.seq == other.seq).all() and
            (self.res_id == other.res_id).all() and
            (self.coords == other.coords).all() and
            (self.chain_breaks == other.chain_breaks).all() and
            (self.sec == other.sec) and
            (self.is_paired == other.is_paired).all()
        )

    def __len__(self):

        """ number of residues """

        return len(self.coords)

    def __str__(self):

        """ for printing """

        s = "COORDINATES\n"
        for i, xyz in enumerate(self.coords):
            s += "{} {} {:.3f} {:.3f} {:.3f}".format(self.res_id[i],
                                                     self.seq[i],
                                                     self.coords[i][0],
                                                     self.coords[i][1],
                                                     self.coords[i][2])
            if self.chain_breaks[i]:
                s += " break"
            s += "\n"

        s += "SECONDARY STRUCTURE:\n"
        s += str(self.sec)

        return s

    def save_binary(self, fname):

        """ uses the numpy binary file format .npz to save this structure """

        # save compressed?
        with open(fname, 'wb') as f:
            np.savez(f,
                     coords=self.coords,
                     chain_breaks=self.chain_breaks,
                     seq=self.seq,
                     res_id=self.res_id,
                     basepairs=self.sec.as_array(),
                     is_paired=self.is_paired)

    def load_binary(self, fname):

        """ load a structure form .npz file """

        with open(fname, 'rb') as f:
            dat = np.load(f)
            self.coords = dat['coords']
            self.chain_breaks = dat['chain_breaks']
            self.seq = dat['seq']
            self.res_id = dat['res_id']
            # another hack !!! # TODO: remove it once it's obsolete
            if self.res_id[0] == '0':
                self.res_id = [str(int(resid) + 1) for resid in self.res_id]
            # end of this terrible hack
            self.sec.set(dat['seq'], dat['basepairs'])
            self.is_paired = dat['is_paired']

    def init_trajectory_out(self, fname):

        """ start a new trajectory, where coordinates can be written to """

        if self.f_traj_out:
            self.f_traj_out.close()
        self.f_traj_out = open(fname, "wb")

    def write_trajectory(self):

        """ write the current coordinates to a trajectory """

        if not self.f_traj_out:
            raise RuntimeError("No trajectory opened")

        flat_coords = self.coords.flatten().astype(self.traj_dt)

        if self.traj_maxlen is not None:
            # only relevant for random unfold !
            zeros = np.zeros(self.traj_maxlen*3 - len(flat_coords),
                             dtype=self.traj_dt)
            flat_coords = np.concatenate((flat_coords, zeros))

        flat_coords.tofile(self.f_traj_out)

    def close_trajectory_out(self):

        """ close an opened writing stream of a trajectory """

        if self.f_traj_out:
            self.f_traj_out.close()
        self.f_traj_out = None

    def load_trajectory_in(self, fname):

        """ load a new trajectory to read coordinates from """

        self.traj_in = np.memmap(fname, self.traj_dt, mode="r")
        self.traj_size = len(self.traj_in) / (len(self) * 3)
        self.load_trajectory_snapshot(0)

    def get_trajectory_size(self):

        """ get the number of snapshots of an input trajectory """

        return self.traj_size

    def load_trajectory_snapshot(self, i):

        """ load snapshot number i """

        if i > self.traj_size:
            m = "Index {} is too large for trajectory of size {}".format(i, self.traj_size)
            raise IndexError(m)
        n = len(self)
        s = i * n * 3
        e = s + n * 3
        self.coords = self.traj_in[s:e].reshape((n, 3))

    def _rmsd(self, other, fragstart=0, fraglen=None):

        """ compute rmsd (in Angstrom) between two RNAs """

        if self.rmsdfit is None:
            self.rmsdfit = RmsdFit()

        if fraglen is None:
            rmsd, new_coords = self.rmsdfit.fit(other.coords, self.coords)
        else:
            rmsd, new_coords = self.rmsdfit.fit(other.coords[fragstart: fragstart + fraglen],
                                                self.coords[fragstart: fragstart + fraglen])

        rmsd *= 10  # nm to angstrom

        return rmsd, new_coords

    def rmsd(self, other):

        """ compute rmsd between two RNAs """

        return self._rmsd(other)[0]

    def _get_nfrags(self, fraglen):

        """ get the number of overlapping fragments of
        size fraglen present in this structure """

        return len(self) - fraglen + 1

    def rmsd_fragments(self, other, fraglen):

        """ compute the RMSDs for fragments of fixed length """

        return np.array([self._rmsd(other, start_i, fraglen)[0]
                         for start_i in range(self._get_nfrags(fraglen))])

    def rmsd_fragments_trajectory(self, reference, fraglen):

        """ compute fragment wise RMSDs for a whole trajectory """

        nsnapshots = self.get_trajectory_size()
        rmsds = np.zeros((nsnapshots, self._get_nfrags(fraglen)))
        for i in range(nsnapshots):
            self.load_trajectory_snapshot(i)
            rmsds[i] = self.rmsd_fragments(reference, fraglen)
        return rmsds

    def gdt_ts(self, other):

        """ compute gdt-ts between two RNAs """

        if self.rmsdfit is None:
            self.rmsdfit = RmsdFit()

        return self.rmsdfit.gdt_ts_fit(other.coords, self.coords)

    def gdt_ts_matches(self, other):

        """ compute gdt-ts between two RNAs and get the matching
        positions for the 4 resolution levels """

        if self.rmsdfit is None:
            self.rmsdfit = RmsdFit()

        return self.rmsdfit.gdt_ts_fit_get_matches(other.coords, self.coords)

    def rmsd_gdtts_with_structure(self, other):

        """ compute rmsd and return structure superimposed onto other """

        rmsd, new_coords = self._rmsd(other)
        superimposed = copy.deepcopy(self)
        superimposed.coords = new_coords
        gdt_ts = self.gdt_ts(other)
        return rmsd, gdt_ts, superimposed

    def rmsd_gdtts_traj(self, other):

        """ compute all rmsds and gdt_ts for a trajectory,
        other is the reference structure """

        n = self.get_trajectory_size()
        rmsds = np.zeros(n)
        gdt_ts = np.zeros(n)
        for i in range(n):
            self.load_trajectory_snapshot(i)
            rmsds[i], _ = self._rmsd(other)
            gdt_ts[i] = self.gdt_ts(other)
        return rmsds, gdt_ts

    def _distances(self, r_i, r_j):

        """ compute a list of distances """

        return np.sqrt(((r_i - r_j)**2).sum(axis=1))

    def _distances_index(self, indices):

        """ get the distances from a list of indices
        input: [[i,j], ...] """

        if len(indices) == 0:
            return np.array([])

        ij = np.array(indices)
        c = self.coords
        return self._distances(c[ij[:,0]], c[ij[:,1]])

    def write_contacts_file(self, contact_indices, outfilename, force_constant):

        """ contacts is a list of contacts, each given 
        with (i, j), i and j indices start with one as in pdbfiles """

        distances = self._distances_index(np.array(contact_indices) - 1)
        with open(outfilename, 'w') as f:
            for i, dist in enumerate(distances):
                f.write("{} {} {} {}\n".format(contact_indices[i][0], 
                                               contact_indices[i][1], 
                                               dist, force_constant))

    def _angles(self, ri, rj, rk):

        """ compute angles for lists of atom coordinates """

        return angles(ri, rj, rk)

    def _angles_index(self, indices):

        """ get the angles from a list of indices
        input: [[i,j,k], ...] """

        if len(indices) == 0:
            return np.array([])

        i = np.array(indices)
        c = self.coords
        return self._angles(c[i[:,0]],c[i[:,1]],c[i[:,2]])

    def _dihedrals(self, r_i, r_j, r_k, r_l):

        """
        compute a list of dihedrals
        IN: ri, rj, rk, rl: lists of atom coordinates
        """

        return dihedrals(r_i, r_j, r_k, r_l)

    def _dihedrals_index(self, indices):

        """ get the dihedrals from a list of indices
        input: [[i,j,k,l], ...] """

        if len(indices) == 0:
            return np.array([])

        i = np.array(indices)
        c = self.coords
        return self._dihedrals(c[i[:, 0]], c[i[:, 1]], c[i[:, 2]], c[i[:, 3]])

    def bb_distances(self):

        """ backbone distances:
        00 0 = no basepairs,
        10 1 = first is paired,
        01 2 = second is paired,
        11 3 = both are paired """

        labels = ['00', '10', '01', '11']
        # make a binary code for the different distance types
        p = self.is_paired[:-1] + self.is_paired[1:] * 2  # type of bond [00=0, 10=1, 01=2, 11=3]
        # compute the distances
        d = self._distances(self.coords[:-1], self.coords[1:])
        # remove all distances on chain breaks !
        cb = self.chain_breaks[:-1] == False
        p = p[cb]
        d = d[cb]
        # separate the different distance types
        dist = [d[p==i] for i in range(4)]
        return dist, labels

    def bb_angles(self):

        """ compute angles:
        results are sorted in binary coding 0=no basepair 1=basepair
        0 000 = no basepair
        1 100 = only the first nucleotide is paired
        2 010
        3 110
        4 001
        5 101
        6 011
        7 111 = all three nucleotides are paired
        returns a list of 4 arrays, the index in the list corresponds to the basepair-code """

        labels = ['000', '100', '010', '110', '001', '101', '011', '111']
        p = self.is_paired[:-2] + self.is_paired[1:-1] * 2 + self.is_paired[2:] * 4
        # compute the angles
        ang = self._angles(self.coords[:-2], self.coords[1:-1], self.coords[2:])
        # remove chain breaks (from p and ang)
        cb = self.chain_breaks == False
        cb = cb[:-2] * cb[1:-1]
        p = p[cb]
        ang = ang[cb]
        # separate the different angle types
        angles = [ang[p==i] for i in range(8)]
        return angles, labels

    def bb_dihedrals(self):

        """ compute the dihedrals for the backbone """

        labels = [
            '0000',
            '1000',
            '0100',
            '1100',
            '0010',
            '1010',
            '0110',
            '1110',
            '0001',
            '1001',
            '0101',
            '1101',
            '0011',
            '1011',
            '0111',
            '1111'
        ]
        # make binary code for different dihedrals types (paired-unpaired pattern)
        p = self.is_paired[:-3] + self.is_paired[1:-2] * 2 + self.is_paired[2:-1] * 4 + self.is_paired[3:] * 8
        # compute the dihedrals
        dih = self._dihedrals(self.coords[:-3], self.coords[1:-2], self.coords[2:-1], self.coords[3:])
        # remove chain breaks
        cb = self.chain_breaks == False
        cb = cb[:-3] * cb[1:-2] * cb[2:-1]
        p = p[cb]
        dih = dih[cb]
        # separate different dihedral types
        dihedrals = [dih[p==i] for i in range(16)]
        return dihedrals, labels

    def hlx_dist_angles_dihedrals(self):

        """ get all distances, angles and dihedrals for the helix areas """

        labels = ['10', '11', '110', '111']

        # Part 1: collect the indices
        ij_dist = []
        ijk_ang_10 = []
        ijk_ang_11 = []
        ijkl_dih_10 = []
        ijkl_dih_11 = []
        ijk_ang_110 = []
        ijk_ang_111 = []
        ijkl_dih_110 = []
        ijkl_dih_111 = []

        l = len(self.sec)
        for i, j in enumerate(self.sec):
            # check if the position is paired
            if j == -1:
                continue
            # add a distance
            if i < j:
                ij_dist.append([i, j])
            # check if the chain ends
            i1 = i+1
            j1 = j-1
            if j1 < 0 or i1 >= l:
                continue
            if self.chain_breaks[i] or self.chain_breaks[j1]:
                continue

            if self.sec[i1] == j1:
                # single u-turns
                ijk_ang_11.append([i1, i, j])
                ijkl_dih_11.append([i1, i, j, j1])

                # double u-turns
                i2 = i+2
                j2 = j-2

                if j2 < 0 or i2 >= l:
                    continue
                if self.chain_breaks[i1] or self.chain_breaks[j2]:
                    continue

                if self.sec[i2] == j2:
                    ijk_ang_111.append([i2, i, j])
                    ijkl_dih_111.append([i2, i, j, j2])
                else:
                    ijk_ang_110.append([i2, i, j])
                    ijkl_dih_110.append([i2, i, j, j2])

            else:
                ijk_ang_10.append([i1, i, j])
                ijkl_dih_10.append([i1, i, j, j1])

        # Part 2: compute the values
        bp_dist = self._distances_index(ij_dist)

        ang_10 = self._angles_index(ijk_ang_10)
        ang_11 = self._angles_index(ijk_ang_11)
        ang_110 = self._angles_index(ijk_ang_110)
        ang_111 = self._angles_index(ijk_ang_111)

        dih_10 = self._dihedrals_index(ijkl_dih_10)
        dih_11 = self._dihedrals_index(ijkl_dih_11)
        dih_110 = self._dihedrals_index(ijkl_dih_110)
        dih_111 = self._dihedrals_index(ijkl_dih_111)

        angles = [ang_10, ang_11, ang_110, ang_111]
        dihedrals = [dih_10, dih_11, dih_110, dih_111]

        return bp_dist, angles, dihedrals, labels

    def hlx_additional_angles(self):

        ''' compute the missing angles for the helix '''

        labels = ['11']
        ijk_ang_11 = []
        for i, j in enumerate(self.sec):
            if j == -1:
                continue
            l = len(self.sec)
            i1 = i + 1
            j1 = j - 1
            if i1 >= l or j1 < 0:
                continue
            if self.chain_breaks[i] or self.chain_breaks[j1]:
                continue
            if j1 == self.sec[i1]:
                ijk_ang_11.append([i, j, j1])

        ang_11 = self._angles_index(ijk_ang_11)
        angles = [ang_11]
        return labels, angles

    def hlx_dih_knight(self):

        ''' compute a guard dihedral as in nast '''

        labels = ['010', '110', '011', '111']
        ijkl_dih_010 = []
        ijkl_dih_110 = []
        ijkl_dih_011 = []
        ijkl_dih_111 = []

        l = len(self.sec)

        for i, j in enumerate(self.sec[1:-1]):

            i += 1

            if j <= 0 or j == l-1:
                # i is unpaired (j is -1)
                # or j is the first nucleotide (j is 0)
                # or j is the last nucleotide (j is l-1)
                continue

            i0 = i - 1
            j0 = j + 1
            i1 = i + 1
            j1 = j - 1

            if self.chain_breaks[i0] or self.chain_breaks[i] or self.chain_breaks[j1] or self.chain_breaks[j]:
                continue

            ijkl = [j0, i0, i, i1]

            if j0 == self.sec[i0] and j1 == self.sec[i1]:
                ijkl_dih_111.append(ijkl)
            elif j0 == self.sec[i0]:
                ijkl_dih_110.append(ijkl)
            elif j1 == self.sec[i1]:
                ijkl_dih_011.append(ijkl)
            else:
                ijkl_dih_010.append(ijkl)

        dih_010 = self._dihedrals_index(ijkl_dih_010)
        dih_110 = self._dihedrals_index(ijkl_dih_110)
        dih_011 = self._dihedrals_index(ijkl_dih_011)
        dih_111 = self._dihedrals_index(ijkl_dih_111)

        dihedrals = [dih_010, dih_110, dih_011, dih_111]

        return labels, dihedrals

    def pairwise_distances(self, gapsize=3):

        """ compute all pairwise distances
        gapsize = sequence distance where no distances are computed
        out:
        labels = 0, 10 or 11 nucleotides paired
        """

        g = gapsize
        labels = ['0', '10', '11']
        n = len(self.coords)

        # indices
        i = np.zeros((n-g)*(n-g-1)/2, dtype=int)
        j = np.zeros((n-g)*(n-g-1)/2, dtype=int)
        s = 0
        for k in xrange(n-g-1):
            new_j = np.arange(k+1+g, n)
            l = len(new_j)
            i[s:s+l] = k
            j[s:s+l] = new_j
            s += l

        p = self.is_paired[i] + self.is_paired[j]
        d = np.sqrt(((self.coords[i] - self.coords[j])**2).sum(axis=1))

        d0 = d[p==0]
        d1 = d[p==1]
        d2 = d[p==2]

        return [d0, d1, d2], labels

    def compute_geometries(self, gapsize=3):

        """ compute all parameters (distances, angles, dihedrals)
        OUT: a dictionairy with arrays of data as values """

        geometries = {}

        bb_dist, bb_dist_lab = self.bb_distances()
        geometries["dist_bb"] = {}
        for i, lab in enumerate(bb_dist_lab):
            geometries["dist_bb"][lab] = bb_dist[i]

        bb_ang, bb_ang_lab = self.bb_angles()
        geometries["ang_bb"] = {}
        for i, lab in enumerate(bb_ang_lab):
            geometries["ang_bb"][lab] = bb_ang[i]

        bb_dih, bb_dih_lab = self.bb_dihedrals()
        geometries["dih_bb"] = {}
        for i, lab in enumerate(bb_dih_lab):
            geometries["dih_bb"][lab] = bb_dih[i]

        bp_dist, hlx_ang, hlx_dih, hlx_lab = self.hlx_dist_angles_dihedrals()
        geometries["dist_hlx"] = {}
        geometries["dist_hlx"]["11"] = bp_dist
        geometries["ang_hlx"] = {}
        geometries["dih_hlx"] = {}
        for i, lab in enumerate(hlx_lab):
            geometries["ang_hlx"][lab] = hlx_ang[i]
            geometries["dih_hlx"][lab] = hlx_dih[i]

        nb_dist, nb_lab = self.pairwise_distances()
        geometries["dist_nb"] = {}
        for i, lab in enumerate(nb_lab):
            geometries["dist_nb"][lab] = nb_dist[i]

        # new angles and dihedrals !
        hlx_new_lab, hlx_new_ang = self.hlx_additional_angles()
        geometries["ang_hlx2"] = {}
        for i, lab in enumerate(hlx_new_lab):
            geometries["ang_hlx2"][lab] = hlx_new_ang[i]

        dih_knight_lab, hlx_knight = self.hlx_dih_knight()
        geometries["dih_knight"] = {}
        for i, lab in enumerate(dih_knight_lab):
            geometries["dih_knight"][lab] = hlx_knight[i]

        return geometries

    def _save_geometries(self, geometries, fname):

        """ save geometry dictionairy in a .npz file """

        with open(fname, 'wb') as f:
            np.savez(f,
                     # backbone
                     dist_bb_00=geometries['dist_bb']['00'],
                     dist_bb_10=geometries['dist_bb']['10'],
                     dist_bb_01=geometries['dist_bb']['01'],
                     dist_bb_11=geometries['dist_bb']['11'],

                     ang_bb_000=geometries["ang_bb"]['000'],
                     ang_bb_100=geometries["ang_bb"]['100'],
                     ang_bb_010=geometries["ang_bb"]['010'],
                     ang_bb_110=geometries["ang_bb"]['110'],
                     ang_bb_001=geometries["ang_bb"]['001'],
                     ang_bb_101=geometries["ang_bb"]['101'],
                     ang_bb_011=geometries["ang_bb"]['011'],
                     ang_bb_111=geometries["ang_bb"]['111'],

                     dih_bb_0000=geometries["dih_bb"]['0000'],
                     dih_bb_1000=geometries["dih_bb"]['1000'],
                     dih_bb_0100=geometries["dih_bb"]['0100'],
                     dih_bb_1100=geometries["dih_bb"]['1100'],

                     dih_bb_0010=geometries["dih_bb"]['0010'],
                     dih_bb_1010=geometries["dih_bb"]['1010'],
                     dih_bb_0110=geometries["dih_bb"]['0110'],
                     dih_bb_1110=geometries["dih_bb"]['1110'],

                     dih_bb_0001=geometries["dih_bb"]['0001'],
                     dih_bb_1001=geometries["dih_bb"]['1001'],
                     dih_bb_0101=geometries["dih_bb"]['0101'],
                     dih_bb_1101=geometries["dih_bb"]['1101'],

                     dih_bb_0011=geometries["dih_bb"]['0011'],
                     dih_bb_1011=geometries["dih_bb"]['1011'],
                     dih_bb_0111=geometries["dih_bb"]['0111'],
                     dih_bb_1111=geometries["dih_bb"]['1111'],

                     # helix
                     dist_hlx_11=geometries['dist_hlx']['11'],

                     ang_hlx_10=geometries['ang_hlx']['10'],
                     ang_hlx_11=geometries['ang_hlx']['11'],
                     ang_hlx_110=geometries['ang_hlx']['110'],
                     ang_hlx_111=geometries['ang_hlx']['111'],

                     dih_hlx_10=geometries["dih_hlx"]['10'],
                     dih_hlx_11=geometries["dih_hlx"]['11'],
                     dih_hlx_110=geometries["dih_hlx"]['110'],
                     dih_hlx_111=geometries["dih_hlx"]['111'],

                     # non bonded
                     dist_nb_0=geometries["dist_nb"]['0'],
                     dist_nb_10=geometries["dist_nb"]['10'],
                     dist_nb_11=geometries["dist_nb"]['11'],

                     # new
                     ang_hlx2_11=geometries['ang_hlx2']['11'],
                     dih_knight_010=geometries['dih_knight']['010'],
                     dih_knight_011=geometries['dih_knight']['011'],
                     dih_knight_110=geometries['dih_knight']['110'],
                     dih_knight_111=geometries['dih_knight']['111'])

    def compute_and_save_geometries(self, fname, gapsize=3):

        """ compute geometries and save them in a .npz file """

        geometries = self.compute_geometries(gapsize)
        self._save_geometries(geometries, fname)

    def compute_and_save_geometries_trajectory(self, outdir, gapsize=3):

        """
        save the geometries to a directory (outdir)
        with binary files for each parameter, this can be loaded
        by Datafit object
        """

        # open files
        outfiles = {}
        for d in self.geo.datatypes:
            fn = os.path.join(outdir, d + ".bin")
            outfiles[d] = open(fn, "wb")
        # iterate trajectory
        for i in range(self.get_trajectory_size()):
            self.load_trajectory_snapshot(i)
            # compute geometries
            geometries = self.compute_geometries(gapsize)
            # save geometries
            for d in self.geo.datatypes:
                ds = d.split('_')
                geo = "_".join(ds[:2])
                code = ds[2]
                geometries[geo][code].astype(self.geo.dt).tofile(outfiles[d])
        # close files
        for f in outfiles.itervalues():
            f.close()


def plot_params_test():

    """ quick and dirty trial function """

    fn_pdb = '../tests/data/pdbs/4v9b/4v9b-pdb-bundle1.pdb'
    rna = RnaC3Struct()
    rna.parse_pdb(fn_pdb, "A", 1)
    rna.read_sec_struct_pdb(fn_pdb)
    print(rna)
    print("size = {}".format(len(rna)))

    # test the distances
    from matplotlib import pyplot as plt

    # backbone distances
    dist, d_label = rna.bb_distances()
    fig = plt.figure()
    for i, lab in enumerate(d_label):
        sub = fig.add_subplot(2, 2, i+1)
        sub.hist(dist[i], bins=25)
        sub.set_xlabel(lab)

    # backbone angles
    angles, a_label = rna.bb_angles()
    fig2 = plt.figure()
    for i, lab in enumerate(a_label):
        sub = fig2.add_subplot(4, 2, i+1)
        sub.hist(angles[i], bins=25)
        sub.set_xlim([0,np.pi])
        sub.set_xlabel(lab)

    # backbone dihedrals
    dihedrals, dih_label = rna.bb_dihedrals()
    fig3 = plt.figure()
    for i, lab in enumerate(dih_label):
        sub = fig3.add_subplot(4, 4, i+1)
        sub.hist(dihedrals[i], bins=25)
        sub.set_xlim([-np.pi, np.pi])
        sub.set_xlabel(lab)

    # helix
    bp_dist, ang, dih, hlx_labels = rna.hlx_dist_angles_dihedrals()
    # plot the distance
    fig4 = plt.figure()
    sub = fig4.add_subplot(111)
    sub.hist(bp_dist, bins=25)
    sub.set_xlabel("helix distance")
    # plot the angles and dihedrals
    fig5 = plt.figure()
    for i, lab in enumerate(hlx_labels):
        sub = fig5.add_subplot(4, 2, i*2+1)
        sub.hist(ang[i], bins=25)
        sub.set_xlabel(lab)
        sub.set_xlim([0, np.pi])

        sub = fig5.add_subplot(4,2,i*2+2)
        sub.hist(dih[i], bins=25)
        sub.set_xlabel(lab)
        sub.set_xlim([-np.pi, np.pi])

    fig5 = plt.figure()
    dist, nb_labels = rna.pairwise_distances()
    for i, lab in enumerate(nb_labels):
        sub = fig5.add_subplot(3, 1, i+1)
        sub.hist(dist[i], bins=100)
        sub.set_xlabel(lab)

    plt.show()


if __name__ == "__main__":

    plot_params_test()
