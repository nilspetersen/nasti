import numpy as np
from scipy import stats


def bootstrap_mean_distribution_fast(data, n_samples):

    return np.random.choice(data, (n_samples, data.size)).mean(axis=1)


def bootstrap_mean_distribution_slow(data, n_samples):

    return np.sort([np.random.choice(data, data.size).mean()
                    for _ in xrange(n_samples)])


def two_sides_interval(data, p):

    """ compute a bounds of a two sided interval from sampled distribution
    p is the percentile on both sides, e.g. 0.025 results in a 95% interval,
    since 2.5 % are taken from both sides """

    if not (0.0 <= p < 0.5):
        raise RuntimeError("interval has to be within [0., 0.5]")

    data_sorted = np.sort(data)
    n = data_sorted.size

    i = int(round(n * p))
    j = n - 1 - i

    lower = data_sorted[i]
    upper = data_sorted[j]

    return lower, upper


def bootstrap_confidence_interval(data, percentile=0.025, n_samples=10000):

    """ computes a 'bootstrap percentile confidence interval' """

    sampled_means = bootstrap_mean_distribution_slow(data, n_samples)
    lower, upper = two_sides_interval(sampled_means, percentile)

    return sampled_means.mean(), lower, upper


def permutation_meandiff_distribution(dataA, dataB, n_samples):

    samples = np.zeros(n_samples, dtype=np.float64)
    n_a = dataA.size
    merged = np.concatenate((dataA, dataB), axis=0)

    for i in xrange(n_samples):

        np.random.shuffle(merged)
        samples[i] = merged[:n_a].mean() - merged[n_a:].mean()

    return samples


def permutation_meandiff_p(mdiff, samples):

    """ compute the p value of having a difference mdiff or larger
    based on the permuation_samples values """

    p = float((np.abs(samples) >= np.abs(mdiff)).sum()) / float(samples.size)
    return p


def permutation_ttest_p(mdiff, samples):

    """ two sided t-test using values from permutation samples """
    t = np.abs(mdiff - samples.mean()) / samples.std()
    p = 2 * (1 - stats.t.cdf(t, samples.size - 1))

    return p


def permutation_test(dataA, dataB, n_samples=10000):

    """ permutation test to check for unsimilar means """

    mdiff = dataA.mean() - dataB.mean()
    samples = permutation_meandiff_distribution(dataA, dataB, n_samples)
    p = permutation_meandiff_p(mdiff, samples)
    p_t = permutation_ttest_p(mdiff, samples)

    return p, p_t
