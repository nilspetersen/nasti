import os

from nasti.fffactory import ForceFieldFactory
from nasti.secondaryStructure import SecondaryStructure
from nasti.random_unfold import RandomUnfold
from nasti.simopt import RunFoldPar, SimOptDict


def build_random_unfolded(par):

    """ build a set of random unfolded structure """

    verbose = par.verbose

    # read the secondary structure
    sec = SecondaryStructure()
    sec.read_bpseq(par.fn_bpseq)

    # create the force field factory
    fff = None
    if par.ff_type == 'nasti':
        fff = ForceFieldFactory(par.ff_paramsfile)
    else:
        raise RuntimeError("Forcefield type {} not supported".format(par.ff_type))

    final_bin = {}

    for seed in par.seeds:

        if verbose:
            print "BUILDING RANDOM INITIAL STRUCTURE FOR CASE {}".format(seed)

        casedir = os.path.join(par.outdir, "{}".format(seed))
        ru = RandomUnfold(fff, sec, seed, casedir)
        ru.build_molecule(par.steps_per_iter,
                          par.snapshot_interval,
                          par.preferred_platform,
                          par.gpu_number,
                          verbose)

        final_bin[seed] = ru.get_fn_final_structure()
    return final_bin


def main():

    """ the mighty main """

    import argparse
    parser = argparse.ArgumentParser(prog="NASTI_UNFOLD")
    parser.add_argument("config_file")
    args = parser.parse_args()

    # try:

    opt_dict = SimOptDict(args)
    runfold_par = RunFoldPar(opt_dict)

    build_random_unfolded(runfold_par)
    # except Exception as e:
    #     print "Error: {}".format(str(e))
    #     raise e


if __name__ == "__main__":

    main()
