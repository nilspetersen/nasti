
import yaml


class SimOptMissingError(RuntimeError):

    def __init__(self, optname):

        """ initialize """

        msg = "{} is missing".format(optname)
        super(SimOptMissingError, self).__init__(msg)


class SimOptDict:

    """ parameters for a nasti simulation """

    def __init__(self, args):

        """ initialize: parse cmd args ! """

        arg_dict = vars(args)
        fn_config = arg_dict['config_file']
        with open(fn_config, "r") as f:
            self._opt_dict = yaml.load(f)

        for key in arg_dict:
            if key == "config_file":
                continue
            if arg_dict[key] is not None:
                self._addopt(key.split("__"), arg_dict[key])

    def _addopt(self, keys, val):

        """ add an option with multiple keys to the multi-layer dictionairy """

        d = self._opt_dict
        for k in keys[:-1]:
            if k not in d:
                d[k] = {}
            d = d[k]
        d[keys[-1]] = val

    def getopt(self, key1, *args):

        """ get an option """

        keys = [str(key1)] + [str(element) for element in args]
        d = self._opt_dict
        opt = None
        for opt in keys:
            try:
                d = d[opt]
            except KeyError:
                raise SimOptMissingError(":".join(keys))
        return d

    def getopt_if_exists(self, key1, *args):

        """ get an option, but return None if it's not here """

        try:
            opt = self.getopt(key1, *args)
            return opt
        except SimOptMissingError:
            return None

    def exists(self, key1, *args):

        """ check whether an entry exists """

        try:
            self.getopt(key1, *args)
            return True
        except SimOptMissingError:
            return False

    def getopt_with_default(self, default, key1, *args):

        """ get an option, return default if it does not exist """

        opt = self.getopt_if_exists(key1, *args)
        if opt is None:
            return default
        return opt


class RunFoldPar:

    """ Parameters for RandomUnfold main """

    def __init__(self, simopt):

        """ initialize from SimOptDict object """

        # mandatory
        self.outdir = simopt.getopt('output', 'directory')
        self.fn_bpseq = simopt.getopt('input', 'bpseq')
        self.seeds = range(simopt.getopt('seeds', 'from'),
                           simopt.getopt('seeds', 'to') + 1)

        self.snapshot_interval = simopt.getopt('random_unfold_simulation',
                                               'snapshot_interval')
        self.steps_per_iter = simopt.getopt('random_unfold_simulation',
                                            'steps_per_iteration')

        # optional
        self.preferred_platform = simopt.getopt_if_exists('platform', 'name')
        self.gpu_number = simopt.getopt_if_exists('platform', 'gpu_number')

        self.ff_type = simopt.getopt_with_default('nasti',
                                                  'forcefield',
                                                  'type')
        if self.ff_type == 'nasti':
            self.ff_paramsfile = simopt.getopt_if_exists('forcefield',
                                                         'filename')

        self.verbose = simopt.getopt_with_default(True,
                                                  'output',
                                                  'verbose')


class SimPar:

    """ parameters for Nast/Nasti simulation main """

    def __init__(self, simopt):

        """ initialize """

        # must
        self.steps_eq = int(float(simopt.getopt('steps', 'equilibration')))
        self.steps_prod = int(float(simopt.getopt('steps', 'production')))
        self.snapshot_interval = int(float(simopt.getopt('steps',
                                                         'snapshot_interval')))

        self.outdir = simopt.getopt('output', 'directory')

        self.fn_bpseq = simopt.getopt('input', 'bpseq')
        self.fn_tertiary = simopt.getopt_if_exists('input', 'tertiary')

        self.seeds = range(simopt.getopt('seeds', 'from'),
                           simopt.getopt('seeds', 'to') + 1)

        self.start_conformation = simopt.getopt('simulation',
                                                'start_conformation')
        self.cmm_removal_frequency = simopt.getopt('simulation',
                                                   'cmm_removal_frequency')
        self.use_thermostat = simopt.getopt('simulation', 'thermostat')
        self.thermo_coupling = simopt.getopt_if_exists('simulation',
                                                       'thermostat_collision_frequency')
        if(self.thermo_coupling):
            self.thermo_coupling = float(self.thermo_coupling)

        # can
        self.preferred_platform = simopt.getopt_if_exists('platform', 'name')
        self.gpu_number = simopt.getopt_if_exists('platform', 'gpu_number')
        self.timestep = simopt.getopt_if_exists('simulation', 'timestep')

        self.ff_type = simopt.getopt_with_default('nasti',
                                                  'forcefield',
                                                  'type')
        if self.ff_type == 'nasti':
            self.ff_paramsfile = simopt.getopt_if_exists('forcefield',
                                                         'filename')

        self.verbose = simopt.getopt_with_default(True,
                                                  'output',
                                                  'verbose')

        # conditional
        if self.start_conformation == 'pdb':
            self.fn_pdb = simopt.getopt('input', 'pdb', 'filename')
            self.chain_id = simopt.getopt('input', 'pdb', 'chain')
        elif self.start_conformation == 'random_unfold':
            self.runfold_options = RunFoldPar(simopt)
