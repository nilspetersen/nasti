
import os
import numpy as np


class EnergyVsRmsdGdtts:

    def __init__(self, directory, dist_type="rmsd"):

        self.directory = directory
        self.energy = self._load("e_pot.bin")
        self.vals = self._load(dist_type+".bin")
        self.dist_type = dist_type
        self._sort_by_energy()

    def _load(self, fn):

        return np.fromfile(os.path.join(self.directory, fn))

    def _reset_arrays(self, index_list):

        """ index_list is either a list of bool-values or
        a list of indices, all 3 arrays (rmsd, gdt_ts and energy)
        will be reassinged to this order """

        self.energy = self.energy[index_list]
        self.vals = self.vals[index_list]

    def _sort_by_energy(self):

        order = np.argsort(self.energy)
        self._reset_arrays(order)

    def rm_outliers(self):

        """ remove all entries where the energy is above the
        1.5 interquartile range level """

        l = len(self.energy)
        i = l / 4
        q1 = self.energy[i]
        q3 = self.energy[i*3]
        iqr = q3 - q1
        lower_iqr = self.energy < q3 + 1.5 * iqr
        self._reset_arrays(lower_iqr)
        return l - lower_iqr.sum()

    def rm_upper_half(self):

        l = self.vals.size / 2
        self.energy = self.energy[:l]
        self.vals = self.vals[:l]

    def get_vals(self):

        return self.vals
