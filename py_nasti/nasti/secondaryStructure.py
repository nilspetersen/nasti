
import numpy as np


class SecondaryStructure:

    """ class for RNA secondary structures """

    def __init__(self):

        """ initialize """

        self._bp = None
        self._seq = None
        self._is_paired = None  # array with 0 or 1

    def assign_helices(self):

        ''' assign a helix number to each nucleotide, 0 means loop '''

        helices = np.zeros(len(self._bp), dtype=int)

        last_i = -1000
        last_j = -1000
        helix_number = 0

        for i, j in enumerate(self._bp):
            if j < i:
                continue
            if not (i == last_i+1 and j == last_j-1):
                helix_number += 1
            last_i = i
            last_j = j
            helices[i] = helix_number
            helices[j] = helix_number
        return helices

    def __eq__(self, other):

        """ equality for all """

        if not isinstance(other, SecondaryStructure):
            return False
        return (
            (self._seq == other._seq).all() and
            (self._bp == other._bp).all()
        )

    def __getitem__(self, i):

        """ get basepair at position i """

        return self._bp[i]

    def __len__(self):

        """ returns the length of the sequence """

        if self._bp is None:
            return 0
        return len(self._bp)

    def __setitem__(self, i, j):

        """ set basepair """

        self._bp[i] = j
        self._bp[j] = i
        self._is_paired[i] = 1
        self._is_paired[j] = 1

    def __str__(self):

        """ return bpseq string """

        bpseq_str = ""
        for i, j in enumerate(self):
            bpseq_str += "{} {} {}\n".format(i+1, self._seq[i], j+1)
        return bpseq_str

    def _set_paired(self):

        """ set the _is_paired array according to the _bp array """

        self._is_paired = (self._bp > -1).astype(np.int)

    def set(self, sequence, basepairs, is_paired_arr=None):

        """ set sequence and basepairs """

        if len(basepairs) != len(sequence):
            raise RuntimeError("basepairs and sequence do not match")

        self._bp = basepairs
        self._seq = sequence
        if is_paired_arr is None:
            self._set_paired()
        else:
            self._is_paired = is_paired_arr

    def set_sequence(self, sequence):

        """ set the sequence, works only if basepairs are not set yet """

        if self._bp is not None:
            raise RuntimeError("sequence of secondary structure "
                               "can not be set after basepairs")

        self._seq = sequence
        self._bp = np.ones(len(sequence), dtype=int) * -1
        self._set_paired()

    def tofile(self, filename):

        """ write to a .bpseq file """

        with open(filename, 'w') as f:
            f.write(str(self))

    def read_bpseq(self, filename):

        """ read the secondary structure from a .bpseq file
        - sequence has to be in order """

        with open(filename, "r") as f:
            seq = []
            bp = []
            for line in f:
                l = line.strip().split()
                if l and l[0].isdigit():
                    seq.append(l[1])
                    bp.append(int(l[2])-1)
        self.set(np.array(seq), np.array(bp))

    def get_sequence(self):

        """ get the sequence """

        return self._seq

    def as_array(self):

        """ return an array of basepairs """

        return self._bp

    def ispaired_int_array(self):

        """ returns an array with integers 0 and 1 to tell
        which positions are paired """

        return self._is_paired

    def ispaired(self, i):

        """ returns 1 if nucleotide at position i is paired, else 0 """

        return self._is_paired[i]

    def count_pairs_in_region(self, i, n):

        """ count the number of pairs of a sequence piece of length n
        starting at position i """

        return (self._bp[i:i+n] > -1).sum()

    def get_substructure(self, start_i, end_i):

        """ get a substructure of the secondary structure,
        basepairs reaching out of that region are removed """

        new_seq = self._seq[start_i:end_i+1].copy()
        new_bp = self._bp[start_i:end_i+1].copy()
        new_is_paired = self._is_paired[start_i:end_i+1].copy()

        new_bp[new_bp < start_i] = -1
        new_bp[new_bp > end_i] = -1
        new_bp[new_bp >= 0] -= start_i

        new = SecondaryStructure()
        new.set(new_seq, new_bp, new_is_paired)

        return new

    def get_patterns_fragments(self, fraglen):

        nfrags = len(self) - fraglen + 1
        patterns = np.ones((nfrags, fraglen), dtype=np.int) * -1
        for i in range(nfrags):
            patterns[i] = self._is_paired[i: i + fraglen]
        return patterns
