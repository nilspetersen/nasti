from nasti import default_ff
import yaml


class ForceFieldParameters:

    ''' Force field parameter structure '''

    def __init__(self):

        ''' initialize '''

        self.par = {'gauss': {}, 'splines': {},
                    'nonbond': {'hlx': {}, 'no_hlx': {}}}
        # set nonbonded to default values
        self.par['nonbond']['hlx']['sigma'] = 11.92
        self.par['nonbond']['hlx']['epsilon'] = 8.69e-15
        self.par['nonbond']['no_hlx']['sigma'] = 9.13
        self.par['nonbond']['no_hlx']['epsilon'] = 4.32e-14
        self.par['nonbond']['activated'] = True

    def _get_bool(self, key):
        try:
            return self.par[key]
        except:
            return False

    def use_custom_angle(self):

        return self._get_bool('use_custom_angle')

    def use_custom_dihedral(self):

        return self._get_bool('use_custom_dihedral')

    def use_custom_nonbond(self):

        return self._get_bool('use_custom_nonbond')

    def __getitem__(self, key):

        ''' get and item from the par dictionairy '''

        return self.par[key]

    def get_nonbond_sig_nohlx(self):

        ''' getter for nonbonded non-helix sigma '''

        return self.par['nonbond']['no_hlx']['sigma']

    def get_nonbond_eps_nohlx(self):

        ''' getter for nonbonded non-helix epsilon '''

        return self.par['nonbond']['no_hlx']['epsilon']

    def get_nonbond_sig_hlx(self):

        ''' getter for nonbonded helix sigma '''

        return self.par['nonbond']['hlx']['sigma']

    def get_nonbond_eps_hlx(self):

        ''' getter for nonbonded helix epsilon '''

        return self.par['nonbond']['hlx']['epsilon']

    def _add_gaussparam(self, name, mean, k):

        ''' add force field parameters for an energy term
        derivated from gauss or von mises function '''

        s = name.split('_')
        geo = s[0]   # dist, hlx
        sec = s[1]  # hlx or bb
        code = s[2]

        if sec == 'nb':
            return

        if sec not in self.par['gauss']:
            self.par['gauss'][sec] = {}
        if geo not in self.par['gauss'][sec]:
            self.par['gauss'][sec][geo] = {}
        if code not in self.par['gauss'][sec][geo]:
            self.par['gauss'][sec][geo][code] = {}

        self.par['gauss'][sec][geo][code]['activated'] = True
        self.par['gauss'][sec][geo][code]['opt'] = mean
        self.par['gauss'][sec][geo][code]['k'] = k

    def read_gauss_params(self, fn_gaussparams):

        ''' read parameters from gauss and von mises distributions '''

        # read gauss parameters
        with open(fn_gaussparams, 'r') as f:
            for line in f:
                l = line.split()
                name = l[0].strip()
                self._add_gaussparam(name, float(l[1]), float(l[2]))

    def _add_spline_formula(self,
                            name,
                            spline_param_dict):

        ''' add a spline energy term '''

        s = name.split('_')
        geo = s[0]   # dist, hlx
        sec = s[1]  # hlx or bb
        code = s[2]

        if sec not in self.par['splines']:
            self.par['splines'][sec] = {}
        if geo not in self.par['splines'][sec]:
            self.par['splines'][sec][geo] = {}
        if code not in self.par['splines'][sec][geo]:
            self.par['splines'][sec][geo][code] = {}

        self.par['splines'][sec][geo][code]['activated'] = True
        for spline_par, param_list in spline_param_dict.iteritems():
            self.par['splines'][sec][geo][code][spline_par] = param_list

    def read_spline_params(self, fn_splineparams):

        ''' read formulas of splines '''

        param_key_dict = {'a': 'const',
                          'b': 'lin',
                          'c': 'quad',
                          't': 'intervals'}
        angleterms = {}
        with open(fn_splineparams, "r") as f:
            case = None
            for line in f:
                line = line.strip()
                if line[:5] == "#case":
                    case = line.split("=")[1]
                    angleterms[case] = {}
                    angleterms[case]["formula"] = ""
                elif not line or line[0] == "#":
                    continue
                else:
                    angleterms[case]["formula"] += line
                    if line[0] in "abct":
                        param_key = param_key_dict[line[0]]
                        if param_key not in angleterms[case]:
                            angleterms[case][param_key] = []
                        val = float(line.split("=")[-1].replace(";", ""))
                        angleterms[case][param_key].append(val)
        for name, spline_param_dict in angleterms.iteritems():
            self._add_spline_formula(name, spline_param_dict)

    def write_yaml(self, fn_out):

        ''' write a yaml file '''

        with open(fn_out, 'w') as f:
            yaml.dump(self.par, f, default_flow_style=False)

    def use_default_params(self):

        ''' get the default parameters stored in default_ff.py '''

        self.par = default_ff.default_ff

    def read_yaml(self, fn_in):

        ''' read a yaml file '''

        with open(fn_in, 'r') as f:
            self.par = yaml.load(f)

    def deactivate_backbone_harmonic_angles(self):

        ''' deactivate all harmonic angle terms '''

        for code in self.par['gauss']['bb']['ang'].iterkeys():
            self.par['gauss']['bb']['ang'][code]['activated'] = False

    def create_mixed_spline_forcefield(self, fn_gaussparams,
                                       fn_splineparams,
                                       fn_out):

        ''' create a nasti forcefield with gauss parameters
        and spline parameters '''

        self.read_gauss_params(fn_gaussparams)
        self.read_spline_params(fn_splineparams)
        self.deactivate_backbone_harmonic_angles()
        self.write_yaml(fn_out)
