
import nastiplugin
import simtk.openmm as mm
import simtk.unit as units
from nasti.ff_params import ForceFieldParameters

_DIHEDRAL_FORCE_FUNC = """fa*fb*(U_dih - 2*k*s) + 2*k*s;

U_dih = k*(1+cos(theta-R_theta));
theta = dihedral(p1, p2, p3, p4);

fa = 1 + step(pa-alpha) * caa + step(alpha-pb) * cab;
caa = 0.5 * cos(pi / pa * (alpha - pa)) - 0.5;
cab = 0.5 * cos(pi / pa * (alpha - pb)) - 0.5;
alpha = angle(p1, p2, p3);

fb = 1 + step(pa-beta) * cba + step(beta-pb) * cbb;
cba = 0.5 * cos(pi / pa * (beta - pa)) - 0.5;
cbb = 0.5 * cos(pi / pa * (beta - pb)) - 0.5;
beta = angle(p2, p3, p4);

pb = pi - pa;
pa = border_width;
pi = 3.141592653589793
"""

_REPULSION_FUNC = """ step(cutoff - r) * (4 * epsilon * ((sigma/r)^(2*n) - (sigma/r)^n) + epsilon);
cutoff = 2^(1.0/n) * sigma;
sigma=0.5*(sigma1+sigma2);
epsilon=sqrt(epsilon1*epsilon2);
"""


def make_dihedral_force_custom(border_width, s):

    """ Make a custom dihedral force. """

    # custom dihedral term
    # s is the shift, has to be between 0 and 1
    dihedral = mm.CustomCompoundBondForce(4, _DIHEDRAL_FORCE_FUNC)

    # s=1.0 means a 180 degrees angle results in maximum energy,
    # s=0.0 means it is an optimum, not sure which option is better yet!
    dihedral.addGlobalParameter("s", s)

    # maybe we can use an even smaller border_width !
    # we should observe when we start to get energy jumps
    dihedral.addGlobalParameter("border_width", border_width * units.radians)

    # the force constant, derive from kappa
    dihedral.addPerBondParameter("k")

    # if k is positive, then R_theta is not the optimum but the worst energy,
    # shifted from the optimum (mean of the von mises distribution) by pi
    # this is also the nast value at the moment, so we should change it
    # to our own measurement
    dihedral.addPerBondParameter("R_theta")

    return dihedral


def repulsion(max_sig=None, exp=6):

    """ Make a custom repulsion force. """

    rep = mm.CustomNonbondedForce(_REPULSION_FUNC)

    rep.addGlobalParameter("n", exp)
    rep.addPerParticleParameter("sigma")
    rep.addPerParticleParameter("epsilon")

    if max_sig is not None:
        rep.setCutoffDistance(2 ** (1.0 / exp) * max_sig)

    return rep


class ForceFieldFactory:

    """ create a force field """

    def __init__(self, fn_paramsfile):

        """ initialization """

        # dihedral specific params
        self._dih_s = 1.0
        self._dih_bw = 0.1   # dihedral border width

        self.par = ForceFieldParameters()
        if fn_paramsfile:
            self.par.read_yaml(fn_paramsfile)
        else:
            self.par.use_default_params()

        self.sec = None  # SecondaryStructure object
        self.tertiary = []

        self.nb_sig_h = self.par.get_nonbond_sig_hlx()
        self.nb_eps_h = self.par.get_nonbond_eps_hlx()

        self.nb_sig_nh = self.par.get_nonbond_sig_nohlx()
        self.nb_eps_nh = self.par.get_nonbond_eps_nohlx()

        # use the custom forces from the first nasti version,
        # else the new nastiplugin function and standard LJ potential
        # are used
        self._use_custom_dih = self.par.use_custom_dihedral()
        self._use_custom_ang = self.par.use_custom_angle()
        self._use_custom_lj = self.par.use_custom_nonbond()

    def set_secondary_structure(self, sec):

        """ set the secondary structure """

        self.sec = sec

    def read_tertiary_interactions(self, fname):

        ''' read a file with tertiary interactions and add
        the tertiary interactions, you can read and add more than one '''

        with open(fname, 'r') as f:
            for line in f:
                # remove comments and split the line
                l = line.split('#')[0].strip().split()
                if len(l) != 4:
                    continue
                # append: i, j, distance, force constant
                i = int(l[0]) - 1
                j = int(l[1]) - 1
                opt = float(l[2])
                k = float(l[3])
                self.tertiary.append([i, j, opt, k])

    def set_dihedral_params(self, s, bw):

        """ set the parameters specific to nasti-dihedrals """

        self._dih_s = s
        self._dih_bw = bw

    # 1) bonds

    def get_bond_force(self):

        """ get all bond forces """

        bondforce = mm.HarmonicBondForce()
        bondlist = self.get_bondlist_bb() + self.get_bondlist_hlx()
        bondlist += self.tertiary
        for i, j, opt, k in bondlist:
            bondforce.addBond(i, j, opt, k)
        return bondforce

    def get_bondlist_bb(self):

        """ get a list with all bonds of the backbone """

        # different parameters for 00, 10, 01, 11 !
        bondlist = []
        for i in range(len(self.sec)-1):
            bp = "{}{}".format(self.sec.ispaired(i),
                               self.sec.ispaired(i+1))
            par = self.par['gauss']['bb']['dist'][bp]
            if par['activated']:
                opt = par['opt']
                k = par['k']
                bondlist.append([i, i+1, opt, k])
        return bondlist

    def get_bondlist_hlx(self):

        """ get a list with all helix-bonds """

        par = self.par['gauss']['hlx']['dist']['11']

        if not par['activated']:
            return []

        bondlist = []
        opt = par['opt']
        k = par['k']

        for i, j in enumerate(self.sec):
            if i < j:
                bondlist.append([i, j, opt, k])
        return bondlist

    def get_bb_bondlen(self):

        """ get the optimum of the backbone length for non-paired residues """

        return self.par['gauss']['bb']['dist']['00']['opt']

    # 2) angles splinefitted
    def _get_spline_force_object(self, spline_par):

        if self._use_custom_ang:
            formula = spline_par['formula']
            return mm.CustomAngleForce(formula)

        return nastiplugin.NastiAngleForce(spline_par['const'],
                                           spline_par['lin'],
                                           spline_par['quad'],
                                           spline_par['intervals'])

    def get_angles_splines_forces(self):

        """ get force for fitted splines """

        ang_par = self.par['splines']['bb']['ang']
        ang_forces = []
        ang_dict = self.get_angles_backbone()
        for bp_code in ang_dict.keys():
            if not ang_par[bp_code]['activated']:
                continue
            force = self._get_spline_force_object(ang_par[bp_code])
            for (i, ii, iii) in ang_dict[bp_code]:
                force.addAngle(i, ii, iii)
            ang_forces.append(force)
        return ang_forces

    def get_angles_backbone(self):

        """ get all angles with typespecific params on the backbone """

        ang_dict = {}
        for i in range(len(self.sec) - 2):
            bp_code = "{}{}{}".format(self.sec.ispaired(i),
                                      self.sec.ispaired(i+1),
                                      self.sec.ispaired(i+2))
            if bp_code not in ang_dict:
                ang_dict[bp_code] = []
            ang_dict[bp_code].append([i, i+1, i+2])
        return ang_dict

    # 3) backbone dihedrals and uturn dihedrals and harmonic angles 

    def get_list_angles_dihedrals_helix(self):

        """ get all helix angles and dihedrals """

        ang_par = self.par['gauss']['hlx']['ang']
        dih_par = self.par['gauss']['hlx']['dih']

        angles = []
        dihedrals = []
        l = len(self.sec)

        for i, j in enumerate(self.sec[:-2]):

            bp1 = ''
            bp2 = ''

            if j == -1:
                continue

            i1 = i+1
            j1 = j-1

            if j1 < 0 or i1 >= l:
                continue

            if self.sec[i1] == j1:
                # append 11 case
                bp1 = '11'

                # check if there are 110 or 111 cases
                i2 = i+2
                j2 = j-2

                if j2 > 0 and i2 < l:
                    if self.sec[i2] == j2:
                        bp2 = '111'
                    else:
                        bp2 = '110'
            else:
                # append 10 cases
                bp1 = '10'

            # - k is divided by two, because it is
            # - assigned twice (in both directions
            # - of the helix)
            if ang_par[bp1]['activated']:
                opt = ang_par[bp1]['opt']
                k = ang_par[bp1]['k']
                angles.append([i1, i, j, opt, k/2])

            if dih_par[bp1]['activated']:
                opt = dih_par[bp1]['opt']
                k = dih_par[bp1]['k']
                dihedrals.append([i1, i, j, j1, opt, k/2])

            if bp2 and ang_par[bp2]['activated']:
                opt = ang_par[bp2]['opt']
                k = ang_par[bp2]['k']
                angles.append([i2, i, j, opt, k/2])

            if bp2 and dih_par[bp2]['activated']:
                opt = dih_par[bp2]['opt']
                k = dih_par[bp2]['k']
                dihedrals.append([i2, i, j, j2, opt, k/2])

        return angles, dihedrals

    def get_dih_list_knights(self):

        ''' get knight dihedral list '''

        par = self.par['gauss']['knight']['dih']
        dihedrals = []
        l = len(self.sec)

        for i, j in enumerate(self.sec[1:-1]):

            i += 1

            if j <= 0 or j == l-1:
                # i is unpaired (j is -1)
                # or j is the first nucleotide (j is 0)
                # or j is the last nucleotide (j is l-1)
                continue

            i0 = i - 1
            j0 = j + 1
            i1 = i + 1
            j1 = j - 1

            bp_code = '{}1{}'.format(int(j0 == self.sec[i0]),
                                     int(j1 == self.sec[i1]))

            params = par[bp_code]

            if params['activated']:
                opt = params['opt']
                k = params['k']
                dihedrals.append([j0, i0, i, i1, opt, k])

        return dihedrals

    def get_dih_list_backbone(self):

        """ get all dihedrals from the backbone """

        dih_list = []
        p = self.sec.ispaired_int_array()
        for i in range(len(p) - 3):
            bp = "{}{}{}{}".format(p[i], p[i+1], p[i+2], p[i+3])
            par = self.par['gauss']['bb']['dih'][bp]
            if par['activated']:
                opt = par['opt']
                k = par['k']
                dih_list.append([i, i+1, i+2, i+3, opt, k])
        return dih_list

    def get_harmonic_angles_dihedrals_forces(self):

        """ get backbone dihedral force and all
        helix-specific angle and dihedral forces """

        anglist, dih_list = self.get_list_angles_dihedrals_helix()
        ang_force = mm.HarmonicAngleForce()
        for i, j, k, opt, ang_k in anglist:
            ang_force.addAngle(i, j, k, opt, ang_k)

        dih_list += self.get_dih_list_backbone()
        dih_list += self.get_dih_list_knights()

        if self._use_custom_dih:
            dih_force = make_dihedral_force_custom(self._dih_bw, self._dih_s)
            for i, j, k, l, opt, dih_k in dih_list:
                dih_force.addBond([i, j, k, l],
                                  [dih_k * units.kilojoules_per_mole,
                                   opt * units.radians])
        else:
            dih_force = nastiplugin.NastiTorsionForce()
            for i, j, k, l, opt, dih_k in dih_list:
                dih_force.addTorsion(i, j, k, l, opt, dih_k, self._dih_bw)
        return ang_force, dih_force

    # nonbonded
    def _get_nonbonded_params(self, is_paired):

        """ returns sigma and epsilon """

        if is_paired:
            return self.nb_sig_h, self.nb_eps_h
        return self.nb_sig_nh, self.nb_eps_nh

    def get_nonbond_list(self, gapsize):

        """ get list of non-bonded interactions and exceptions """

        nb_list = []
        exceptions = []
        is_paired_arr = self.sec.ispaired_int_array()
        n = len(is_paired_arr)
        hlx_num = self.sec.assign_helices()
        for i, is_paired in enumerate(is_paired_arr):
            sig, eps = self._get_nonbonded_params(is_paired)
            nb_list.append([sig, eps])
            # add gapsize exceptions
            for j in range(i+1, min(i+1+gapsize, n)):
                exceptions.append((i, j))
            # add same-helix exceptions
            for j in range(i+gapsize+1, n):
                if hlx_num[i] != 0 and hlx_num[i] == hlx_num[j]:
                    exceptions.append((i, j))
        return nb_list, exceptions

    def get_nonbond_force_custom(self, gapsize):

        """ get non-bonded force using a customized potential """

        nb_force = repulsion(max_sig=max(self.nb_sig_h, self.nb_sig_nh))
        nb_list, exceptions = self.get_nonbond_list(gapsize)
        for sig, eps in nb_list:
            nb_force.addParticle([sig, eps])
        for i, j in exceptions:
            nb_force.addExclusion(i, j)
        return nb_force

    def get_nonbond_force_classic_lj(self, gapsize):

        """ get nonbonded interactions as classic LJ potential
        as in NAST """

        nb_force = mm.NonbondedForce()
        nb_list, exceptions = self.get_nonbond_list(gapsize)
        for sig, eps in nb_list:
            # the first parameter 0.0 is the charge
            nb_force.addParticle(0.0, sig, eps)
        for i, j in exceptions:
            nb_force.addException(i, j, 0.0, 1.0, 0.0)
        return nb_force

    def get_nonbond_force(self, gapsize=3):

        """ get non-bonded force """

        if self._use_custom_lj:
            return self.get_nonbond_force_custom(gapsize)
        return self.get_nonbond_force_classic_lj(gapsize)

    def create_forcefield(self, system):

        """ get all forces and add them to the openmm system """

        if self.sec is None:
            msg = "Need a secondary structure to create a forcefield"
            raise RuntimeError(msg)

        bondforce = self.get_bond_force()
        system.addForce(bondforce)

        ang_forces = self.get_angles_splines_forces()
        for angle_force in ang_forces:
            system.addForce(angle_force)

        harmonic_ang_force, dih_force = self.get_harmonic_angles_dihedrals_forces()
        system.addForce(harmonic_ang_force)
        system.addForce(dih_force)

        if self.par['nonbond']['activated']:
            nb_force = self.get_nonbond_force()
            system.addForce(nb_force)
