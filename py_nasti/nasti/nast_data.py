
import numpy as np


def read_write(fn_nast_traj,
               fn_traj_out,
               fn_thermodata_out,
               omit_first_n,
               stop_at=None,
               write_bin_structure=False):

    steps = []
    kin_e = []
    pot_e = []
    coords = []

    with open(fn_nast_traj, 'r') as f:

        ends_counted = 0

        for line in f:

            line = line.strip()

            if not line:
                continue

            if line == 'END':
                ends_counted += 1
                continue

            if ends_counted < omit_first_n:
                continue

            if stop_at and ends_counted >= stop_at:
                break

            l = line.split()
            if l[0] == 'REMARK':

                if l[1] == 'STEP':

                    steps.append(int(l[2]))

                elif l[1] == 'ENERGIES:':

                    kin_e.append(float(l[4]))
                    pot_e.append(float(l[7]))

                else:

                    raise RuntimeError('REMARK = {}'.format(l[1]))

            elif l[0] == 'ATOM':
                if write_bin_structure:
                    coords.append(float(line[30:38]) / 10.0)
                    coords.append(float(line[38:46]) / 10.0)
                    coords.append(float(line[46:54]) / 10.0)

            else:
                raise RuntimeError('line = {}'.format(line))

    if write_bin_structure:
        with open(fn_traj_out, 'wb') as f_out:
            np.array(coords).tofile(f_out)

    steps = np.array(steps)
    kin_e = np.array(kin_e)
    pot_e = np.array(pot_e)
    temp = np.ones(len(steps))

    thermodata = np.stack((steps, kin_e, pot_e, temp), axis=1)
    np.savetxt(fn_thermodata_out, thermodata)
