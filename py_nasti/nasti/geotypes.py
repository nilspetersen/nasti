
import numpy as np


def tex_curly(input_str):
    return "{" + input_str + "}"


class Geotypes:

    ''' this class contains the different types of angles, distances
    and dihedrals used in Nasti '''

    def __init__(self):

        ''' initialize '''

        self.dt = np.float64
        self.datatypes = [
            'dist_bb_00', 'dist_bb_10', 'dist_bb_01', 'dist_bb_11',

            'ang_bb_000', 'ang_bb_100', 'ang_bb_010', 'ang_bb_110',
            'ang_bb_001', 'ang_bb_101', 'ang_bb_011', 'ang_bb_111',

            'dih_bb_0000', 'dih_bb_1000', 'dih_bb_0100', 'dih_bb_1100',
            'dih_bb_0010', 'dih_bb_1010', 'dih_bb_0110', 'dih_bb_1110',
            'dih_bb_0001', 'dih_bb_1001', 'dih_bb_0101', 'dih_bb_1101',
            'dih_bb_0011', 'dih_bb_1011', 'dih_bb_0111', 'dih_bb_1111',

            'dist_hlx_11',
            'ang_hlx_10', 'ang_hlx_11', 'ang_hlx_110', 'ang_hlx_111',
            'dih_hlx_10', 'dih_hlx_11', 'dih_hlx_110', 'dih_hlx_111',

            'dist_nb_0', 'dist_nb_10', 'dist_nb_11',

            # complementary helix-angle
            'ang_hlx2_11',

            # knights
            'dih_knight_010', 'dih_knight_011',
            'dih_knight_110', 'dih_knight_111'
        ]

        self._tex_symbols_dict = {"dist": "r",
                                  "ang": "\\theta",
                                  "dih": "\\phi"}

        self.iteration = 0

        self._geolabel_dict = {"dist": "Distance [$\\AA$]",
                               "ang": "Angle [rad]",
                               "dih": "Dihedral [rad]"}

        self._scaling_factor = {"dist": 10,
                                "ang": 1,
                                "dih": 1}

    def __len__(self):

        return len(self.datatypes)

    def __iter__(self):
        return self

    def get_scale(self, geotype):
        return self._scaling_factor[geotype]

    def get_latex_str_base(self, geometry, location, basepair_pattern=None):

        tex_str = "$" + self._tex_symbols_dict[geometry] + "_" + tex_curly(location)
        if basepair_pattern:
            tex_str += "^" + tex_curly(basepair_pattern)
        tex_str += "$"
        return tex_str

    def get_latex_str(self, geokey, use_basepair_pattern):

        l = int(use_basepair_pattern) + 2
        return self.get_latex_str_base(*geokey.split("_")[:l])

    def get_plotlabel(self, geokey):

        geo, location, basepair_pattern = geokey.split("_")
        geoinfo = self._geolabel_dict[geo]
        tex_str = self.get_latex_str_base(geo, location, basepair_pattern)
        return " ".join([tex_str, geoinfo])

    def get_datatype(self):
        return self.datatypes[self.iteration]

    def next(self):

        if self.iteration < len(self):
            datatype = self.get_datatype()
            self.iteration += 1
            return datatype
        else:
            self.iteration = 0
            raise StopIteration()

    def iter_with_plotlabels(self):

        for dt in self.datatypes:
            yield dt, self.get_plotlabel(dt)
