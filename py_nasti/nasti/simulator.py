
import copy
import math
import os
from scipy.constants import gas_constant

import simtk.openmm as mm
import simtk.unit as units

from nasti.utils import mkdir_safe


class Simulation:

    """ nasti simulation class """

    def __init__(self,
                 rna_c3_struct,
                 fff,    # force field factory
                 seed,
                 outdir,
                 write_trajectory,
                 verbose=False,
                 cmm_removal_frequency=1000,
                 preferred_platform=None,
                 preferred_gpu=None,
                 timestep=None):

        """ initialize """

        # create a context
        if timestep is None:
            timestep = 0.005
        timestep *= units.picosecond

        particle_mass = 325. * units.amu
        temperature = 300 * units.kelvin

        self.write_trajectory = write_trajectory
        self.verbose = verbose
        self.steps = 0

        # make output directory
        mkdir_safe(outdir)
        self.outdir = outdir

        self.rna = rna_c3_struct
        # save the initial structure to load trajectories later !
        self.rna.save_binary(os.path.join(outdir, "rna.bin"))

        # create a system !
        system = mm.System()
        for _ in range(len(self.rna)):
            system.addParticle(particle_mass)

        # add forces to the system
        fff.create_forcefield(system)

        # add center of mass removal
        if cmm_removal_frequency:
            system.addForce(mm.CMMotionRemover(cmm_removal_frequency))

        # choose a platform
        platform = self.get_platform(preferred_platform, preferred_gpu)

        self.integrator = mm.VerletIntegrator(timestep)
        self.context = mm.Context(system, self.integrator, platform)

        # transfer the positions from the structure to the system
        self.context.setPositions(
            units.Quantity(self.rna.get_coords(), units.nanometer))

        # thermostat
        self.default_thermo_coupling = 1.
        self.thermostat = mm.AndersenThermostat(temperature,
                                                self.default_thermo_coupling / units.picoseconds)
        self.thermostat_idx = 0
        self.thermostat.setRandomNumberSeed(seed)
        self.thermostat_is_on = False

        # initialize velocities
        self.context.setVelocitiesToTemperature(temperature * units.kelvin,
                                                seed)

    def get_platform(self, preferred_platform, preferred_gpu):

        ''' get the preferred platform, or the fastest platform
        if preferred_platform is None '''

        # choose a platform
        platform_dict = {}
        for i in range(mm.Platform.getNumPlatforms()):
            p = mm.Platform.getPlatform(i)
            speed = p.getSpeed()
            platform_dict[speed] = p
        platform = None

        # if self.verbose:
        #     print "Available Platforms:"
        for speed in sorted(platform_dict.keys()):
            p = platform_dict[speed]
            name = p.getName()
            if name == preferred_platform:
                platform = p
            # if self.verbose:
            #     print"  {} (Speed={})".format(name, speed)
        del platform_dict

        if platform is None:
            platform = p
        platform_name = platform.getName()
        if self.verbose:
            print "Using {} Platform".format(platform_name)

        # set GPU number
        if preferred_gpu is not None:
            if 'Cuda' == platform_name:
                platform.setPropertyDefaultValue('CudaDevice',
                                                 preferred_gpu)
            if 'OpenCL' == platform_name:
                platform.setPropertyDefaultValue('OpenCLDeviceIndex',
                                                 preferred_gpu)

        return platform

    def _add_thermostat(self):
        thermostat = copy.deepcopy(self.thermostat)
        state = self.context.getState(getVelocities=True, getEnergy=True,
                                      getPositions=True, getForces=True)

        self.context.getSystem().addForce(thermostat)
        self.thermostat_idx = self.context.getSystem().getNumForces() - 1

        self.context.reinitialize()
        self.context.setState(state)
        self.thermostat_is_on = True

    def _remove_thermostat(self):

        state = self.context.getState(getVelocities=True,
                                      getEnergy=True,
                                      getPositions=True,
                                      getForces=True)
        self.context.getSystem().removeForce(self.thermostat_idx)
        self.context.reinitialize()
        self.context.setState(state)
        self.thermostat_is_on = False

    def thermostat_off(self):

        """ deactivate the thermostat """

        if self.thermostat_is_on:
            self._remove_thermostat()

    def thermostat_on(self, coupling=None):

        """ activate the thermostat """

        if (coupling is not None) and (coupling != self.thermostat.getDefaultCollisionFrequency()):
            self.thermostat.setDefaultCollisionFrequency(coupling / units.picoseconds)
            self.thermostat_off()
        if not self.thermostat_is_on:
            self._add_thermostat()

    def minimize(self):

        """ local energy minimization """

        mm.LocalEnergyMinimizer.minimize(self.context)

    def get_coords_from_context(self):

        """ get the coordinates of the simulation context """

        state = self.context.getState(getPositions=True, getEnergy=False)
        return state.getPositions(asNumpy=True).value_in_unit(units.nanometer)

    def print_out(self, f_thermo):

        """ print/write trajectory and thermodata """

        if not self.verbose and f_thermo is None and not self.write_trajectory:
            return
        state = self.context.getState(getPositions=self.write_trajectory, getEnergy=True)
        eK = state.getKineticEnergy().value_in_unit(units.kilojoule_per_mole)
        eP = state.getPotentialEnergy().value_in_unit(units.kilojoule_per_mole)
        degrees_of_freedom = 3 * len(self.rna) - 6
        temperature = 2. * eK / gas_constant / degrees_of_freedom * 1000.
        if self.verbose:
            print "{} {:.2f} {:.2f} {:.2f}".format(self.steps, eK, eP, temperature)
        if f_thermo is not None:
            line = "{} {} {} {}".format(self.steps, eK, eP, temperature)
            f_thermo.write(line + "\n")
        if self.write_trajectory:
            self.rna.set_coords(state.getPositions(asNumpy=True).value_in_unit(units.nanometer))
            self.rna.write_trajectory()

    def simulate(self, steps_total, steps_iter, f_thermo):

        """ simulate 'step_total' steps and write
        energies in 'steps_iter' intervals """

        self.print_out(f_thermo)

        if steps_iter > steps_total or steps_iter < 1:
            steps_iter = steps_total
            iterations = 1
        else:
            iterations = int(math.ceil(steps_total / steps_iter))

        for i in range(iterations):
            self.integrator.step(steps_iter)
            self.steps += steps_iter
            self.print_out(f_thermo)

    def _thermodat_header(self):

        return "Step Ekin Epot Temp"

    def sim_phase(self,
                  steps_tot,
                  steps_iter,
                  phase_name,
                  use_thermostat,
                  thermo_coupling):

        """ a phase of the simulation (usually equilibration or production) """

        if use_thermostat:
            self.thermostat_on(thermo_coupling)
        else:
            self.thermostat_off()

        # open thermodata stream
        with open(os.path.join(self.outdir, phase_name + ".thermodat"), "w") as f_thermo:
            f_thermo.write("# " + self._thermodat_header() + "\n")
            # set output trajectory
            self.rna.init_trajectory_out(os.path.join(self.outdir, phase_name + ".traj"))
            # simulate
            self.simulate(steps_tot, steps_iter, f_thermo)
            # close trajectory
            self.rna.close_trajectory_out()

    def run(self,
            steps_eq,
            steps_iter_eq,
            steps_prod,
            steps_iter_prod,
            use_thermostat,
            thermo_coupling):

        """ run a simulation with equilibration and production """

        if self.verbose:
            print "EQUILIBRATION"
            print self._thermodat_header()
        self.sim_phase(steps_eq,
                       steps_iter_eq,
                       "eq",
                       True,
                       self.default_thermo_coupling)
        if self.verbose:
            print "PRODUCTION"
            print self._thermodat_header()
        self.sim_phase(steps_prod,
                       steps_iter_prod,
                       "prod",
                       use_thermostat,
                       thermo_coupling)

    def write_pdb_trajectories(self):

        """ write a pdb trajectory for the written trajectories """

        for phase_name in ["eq", "prod"]:
            self.rna.load_trajectory_in(os.path.join(self.outdir, phase_name+".traj"))
            self.rna.write_pdb_trajectory(phase_name, self.outdir)

    def write_trajectory_geometries(self):

        """ compute and save the geometries of the trajectories """

        for phase_name in ["eq", "prod"]:
            self.rna.load_trajectory_in(os.path.join(self.outdir, phase_name+".traj"))
            geodir = self.get_dir_geometries(phase_name)
            mkdir_safe(geodir)
            self.rna.compute_and_save_geometries_trajectory(geodir)

    def get_dir_geometries(self, phase_name):

        """ get the name of the directory where the trajectory geometries are saved in """

        return os.path.join(self.outdir, "geo_" + phase_name)

    def get_fn_thermodata(self, phase_name):

        """ get the filename of a thermodata-file """

        return os.path.join(self.outdir, phase_name + ".thermodat")
