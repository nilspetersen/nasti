
from nasti.rnaC3struct import RnaC3Struct, NucleotideMap
import numpy as np
import argparse
import os

""" This script contains a collection of tiny
executables to help handling nasti data """


def getstruct():

    """ a tiny application to get the secondary structure and
    a binary file of the 3D-C3-chaintrace """

    parser = argparse.ArgumentParser(prog="GETSTRUCT")
    parser.add_argument("infile")
    parser.add_argument("chain_id")
    args = parser.parse_args()

    fname_pdb = args.infile
    chain_id = args.chain_id

    fname_prefix = os.path.splitext(fname_pdb)[0]
    fname_bpseq = fname_prefix + '.bpseq'
    fname_bin = fname_prefix + '.bin'
    fname_pdb_out = fname_prefix + '_C3.pdb'

    rna = RnaC3Struct()
    rna.init_pdb(fname_pdb, chain_id)

    rna.save_pdb(fname_pdb_out)
    rna.save_binary(fname_bin)
    rna.get_sec().tofile(fname_bpseq)


def simplify_pdb():

    """ a tiny application to read a pdb structure and write
    one with mapped residues """

    parser = argparse.ArgumentParser(prog="SIMPLIFY PDB")
    parser.add_argument("infile")
    parser.add_argument("chain_id")
    parser.add_argument("mapfile")
    args = parser.parse_args()

    chain_id = args.chain_id
    fn_infile = args.infile
    fn_mapfile = args.mapfile

    rna = RnaC3Struct()
    rna.parse_pdb(fn_infile, chain_id)
    rna.simplify_sequence(fn_mapfile)

    fn_infile_prefix, fn_infile_suffix = os.path.splitext(fn_infile)
    fn_out = "{}_simplified{}".format(fn_infile_prefix, fn_infile_suffix)

    rna.save_pdb(fn_out)


def simplify_pdb_c3(fn_pdb_c3):

    """ read a coarse grained nast structure from a file (with only C3') 
    and replace weird residue names with simple nucleotides a,c,g,u """

    nt_map = NucleotideMap()

    fn_infile_prefix, fn_infile_suffix = os.path.splitext(fn_pdb_c3)
    fn_out = "{}_simplified{}".format(fn_infile_prefix, fn_infile_suffix)

    with open(fn_pdb_c3, "r") as f_in, open(fn_out, "w") as f_out:

        for line in f_in:

            if (line[:4] == "ATOM" or line[:6] == "HETATM") and \
               line[12:16].strip() == "C3'":

                # replace the residue in the line
                res_type = line[17:20].strip()
                new_res = nt_map[res_type]
                newline = "{}{}  {}".format(line[:17], new_res, line[20:])
                f_out.write(newline)


def simplify_c3():

    """ another main function, specifically for C3' structures use in NAST """

    parser = argparse.ArgumentParser(prog="SIMPLIFY PDB")
    parser.add_argument("infile")
    parser.add_argument("mapfile", nargs="?")
    args = parser.parse_args()

    fn_infile = args.infile
    fn_mapfile = args.mapfile

    simplify_pdb_c3(fn_infile)


def nasti2pdb():

    """ takes a nasti trajectory and writes a PDB
    trajectory that can be loaded in vmd """

    parser = argparse.ArgumentParser()
    parser.add_argument('trajectory_file')
    parser.add_argument("--chimera_hack", "-c",
                        help="chimera hack: label the atoms with P (they're still C3'!) ",
                        action='store_true')
    parser.add_argument("--interval", "-i",
                        help="write only every n-th structure from the ensemble",
                        default=1,
                        type=int)
    args = parser.parse_args()

    fn_bin_traj = args.trajectory_file
    path, fn_in = os.path.split(fn_bin_traj)
    outname = os.path.splitext(fn_in)[0]

    rna = RnaC3Struct()
    rna.load_binary(os.path.join(path, 'rna.bin'))
    if args.chimera_hack:
        rna.write_chimera_style()
    rna.load_trajectory_in(fn_bin_traj)
    rna.write_pdb_trajectory(outname, path, args.interval)


def nastiRmsd():

    """ computes RMSD and GDT_TS values for a trajectory and saves
    them in a text file """

    parser = argparse.ArgumentParser()
    parser.add_argument('trajectory_file')
    parser.add_argument('reference_structure')
    args = parser.parse_args()

    fn_bin_traj = args.trajectory_file
    path, fn_in = os.path.split(fn_bin_traj)
    fn_ref = args.reference_structure

    rna_ref = RnaC3Struct()
    rna_ref.load_binary(fn_ref)

    rna = RnaC3Struct()
    rna.load_binary(os.path.join(path, 'rna.bin'))
    rna.load_trajectory_in(fn_bin_traj)

    outname = os.path.splitext(fn_bin_traj)[0] + '.gdtts.rmsd'
    rmsds, gdt_ts = rna.rmsd_gdtts_traj(rna_ref)
    np.savetxt(outname, np.stack([rmsds, gdt_ts], axis=1),
               header="RMSD GDT_TS")
    print "wrote RMSD and GDT_TS scores to {}".format(outname)


def nastiC3struct():

    """ get C3-chaintrace from pdb file and save as pdb file """

    parser = argparse.ArgumentParser(prog="getC3struct")
    parser.add_argument("infile")
    parser.add_argument("chain_id")
    parser.add_argument("--chimera_hack", "-c",
                        help="chimera hack: label the atoms with P (they're still C3'!) ",
                        action='store_true')
    parser.add_argument("--outfile", "-o", help="name of the output file")
    args = parser.parse_args()

    fname_pdb = args.infile
    chain_id = args.chain_id

    if args.outfile:
        fname_pdb_out = args.outfile
    else:
        fname_prefix = os.path.splitext(fname_pdb)[0]
        fname_pdb_out = fname_prefix + '_C3.pdb'

    rna = RnaC3Struct()
    rna.parse_pdb(fname_pdb, chain_id)
    if args.chimera_hack:
        rna.write_chimera_style()
    rna.save_pdb(fname_pdb_out)
    print "wrote C3 coordinates to {}".format(fname_pdb_out)


def nastiBinStruct():

    """ get RNA structure in binary NASTI format """

    parser = argparse.ArgumentParser(prog="getC3struct")
    parser.add_argument("infile")
    parser.add_argument("chain_id")
    args = parser.parse_args()

    fname_pdb = args.infile
    chain_id = args.chain_id
    fname_bin = os.path.splitext(fname_pdb)[0] + '.bin'

    rna = RnaC3Struct()
    rna.parse_pdb(fname_pdb, chain_id)
    rna.save_binary(fname_bin)
    print "saved NASTI binary coordinates in {}".format(fname_bin)
