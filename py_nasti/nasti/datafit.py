
import os
import itertools
import numpy as np
import matplotlib.pyplot as plt
from scipy.constants import gas_constant
from scipy import stats

from nasti.geotypes import Geotypes


def get_bins(bin_width, mini, maxi):

    """ get the histogram bins for a given range and bin_width """

    nbins = int(round((maxi - mini) / bin_width))
    return np.linspace(mini, maxi, nbins + 1)


def get_common_bins(*datafits):

    """ others are a list of other datafit objects """

    valranges = [df.get_range() for df in datafits]
    mini = min((m[0] for m in valranges))
    maxi = max((m[1] for m in valranges))
    bw = max((df.get_bin_width() for df in datafits))
    return get_bins(bw, mini, maxi)


def fit_gaussian(data, temperature, percentile=0.):

    """ simple gaussian fit
        percentile defines how much outliers are removed """

    if percentile > 0.:
        data.sort()
        l = data.size
        cut = round(data.size * percentile)
        data = data[cut:l-cut]

    mean = data.mean()
    sigma = data.std()
    force_constant = gas_constant * temperature / (2 * sigma**2)
    force_constant /= 1000.  # kJ
    return mean, sigma, force_constant


def fit_von_mises(data, temperature):

    """ fit a von mises distribution,
    we use R since scipys version seemed to fail """

    import rpy2.robjects as ro
    from rpy2.robjects.packages import importr

    circ_stats = importr("CircStats")
    r_data = ro.r.matrix(ro.FloatVector(data))
    kappa = circ_stats.est_kappa(r_data)[0]
    mean = circ_stats.circ_mean(r_data)[0]
    force_constant = - gas_constant * temperature * kappa
    force_constant /= 1000.  # kJ
    return mean, kappa, force_constant


class DataFit:

    def __init__(self, datadir):

        """ initialize the object, datadir is the directory
        where geometry data are stored """

        self._truncate_dist_4fit = True
        self._datadir = datadir
        self._geo = Geotypes()
        self.temp = 300
        # currently loaded state
        self._data = None
        self._datatype = None
        self._geotype = None  # geometry type: dist, ang or dih
        self._plotlabel = None
        self._labelsize = 15
        self._do_set_xlabel = True

    def do_set_xlabels(self):
        self._do_set_xlabel = True

    def dont_set_xlabels(self):
        self._do_set_xlabel = False

    def set_labelsize(self, fontisize):
        self._labelsize = fontisize

    def set_plotlabel(self, label):

        """ it will be used when plotting a histogram """

        self._plotlabel = label

    def _filename(self, datatype, datadir=None):

        """ get the name of a binary file
        for a certain datatype with directory """

        if datadir is None:
            datadir = self._datadir
        return os.path.join(datadir, datatype + ".bin")

    def _assert_datatype(self, datatype):

        """ raises a RuntimeError if the datatype doesn't exist """

        if datatype not in self._geo.datatypes:
            err_msg = "{} is not a valid datatype in the DataFit class".format(datatype)
            raise RuntimeError(err_msg)

    def _assert_loaded(self):

        """ make sure something is loaded """

        if self._data is None or self._datatype is None or self._geotype is None:
            err_msg = "No data loaded in DataFit structure"
            raise RuntimeError(err_msg)

    def read(self, datatype, datadir=None, use_memmap=False):

        """ read data from the given directory for a certain datatype """

        self._assert_datatype(datatype)
        fn = self._filename(datatype, datadir)
        if use_memmap:
            try:
                return np.memmap(fn, self._geo.dt)
            except ValueError:
                return np.array([])
        return np.fromfile(fn, self._geo.dt)

    def load(self, datatype, use_memmap=False):

        """ load data into this datastructure """

        self._datatype = datatype
        self._data = self.read(datatype, use_memmap=use_memmap)
        self._geotype = datatype.split("_")[0]

    def _no_outliers(self, data_in, xtimes_iqr):

        """ remove the outliers in the currently loaded data,
        uses x-times interquartile range"""

        data = np.sort(data_in)
        i = data.size / 4

        quartile_1 = data[i]
        quartile_3 = data[i*3]

        iqr = quartile_3 - quartile_1
        iqr_range = xtimes_iqr * iqr
        lower_bound = quartile_1 - iqr_range
        upper_bound = quartile_3 + iqr_range

        data = data[data < upper_bound]
        data = data[data > lower_bound]

        return data

    def remove_outliers_if_dist(self, xtimes_iqr):

        """ removes outliers from a dataset if the loaded
        type is a distance """

        if self._geotype == "dist":
            self._data = self._no_outliers(self._data, xtimes_iqr)

    def _open_outfiles(self):

        """ open an outfile for each datatype
        and return them as a dictionairy """

        # make all files
        files = {}
        for datatype in self._geo:
            files[datatype] = open(self._filename(datatype), 'wb')
        return files

    def _close_oufiles(self, file_dict):

        """ close files in a dictionairy """

        for key in file_dict.iterkeys():
            file_dict[key].close()

    def merge(self, fnames):

        """ merge npz files of distance, angle and dihedral values """

        files = self._open_outfiles()
        for fn in fnames:
            # load each file and append to datafiles
            dat = np.load(fn)
            for datatype in self._geo:
                dat[datatype].astype(self._geo.dt).tofile(files[datatype])
        self._close_oufiles(files)

    def merge_bin(self, directories):

        """ merge geodata from many directories
        (where each datatype is saved in .bin format) """

        outfiles = self._open_outfiles()
        for directory in directories:
            for datatype in self._geo:
                self.read(datatype, datadir=directory).tofile(outfiles[datatype])
        self._close_oufiles(outfiles)

    def get_fit(self):

        """ get gauss/vonmises fit for the loaded data """

        self._assert_loaded()

        if self._geotype == "dih":
            return fit_von_mises(self._data, self.temp)
        if self._truncate_dist_4fit and \
           ("dist_bb" in self._datatype or "dist_hlx" in self._datatype):
            return fit_gaussian(self._no_outliers(self._data, 1.5), self.temp)

        return fit_gaussian(self._data, self.temp)

    def get_n_datapoints(self):

        """ get the number of datapoints for the loaded datatype """

        self._assert_loaded()
        return len(self._data)

    def has_data(self):

        """ check if there is at least one datum in the structure """

        self._assert_loaded()
        return len(self._data) > 0

    def get_min(self):

        """ get the minimum of the loaded data """

        return self._data.min()

    def get_max(self):

        """ get the maximum of the loaded data """

        return self._data.max()

    def _get_scale(self):

        """ get factor for currently loaded data to scale to the proper units """

        return self._geo.get_scale(self._geotype)

    def _get_data(self):

        """ access to loaded data with distances rescaled to angstrom """

        return self._data * self._get_scale()

    def get_range(self):

        """ get the range of values for the loaded data,
        min to max for distances, 0 to pi for angles
        and -pi to pi for dihedrals """

        if self._geotype == "ang":
            return [0, np.pi]
        elif self._geotype == "dih":
            return [-np.pi, np.pi]
        # distance nm to Angstrom
        mini = self.get_min() * self._get_scale()
        maxi = self.get_max() * self._get_scale()
        return mini, maxi

    def get_bin_width(self):

        """ get the bin width, uses the parameters from the fits """

        mean, sk, _ = self.get_fit()
        stddev = sk
        if self._geotype == "dih":
            stddev = stats.vonmises.std(sk, mean, scale=1)
        bin_width = 3.49 * stddev / len(self._data)**(1./3.)
        return bin_width * self._get_scale()

    def get_histogram(self, bins):

        """ get a histogram as an array, bins is an array with bin_borders """

        self._assert_loaded()
        return np.histogram(self._get_data(), bins, density=True)[0]

    def sample_histogram(self, bins, n_samples):

        """ sample a histogram from the loaded values """

        return np.histogram(np.random.choice(self._get_data(), n_samples),
                            bins, normed=True)

    def _get_texlabel(self):

        """ get latex style label for the datatype that is currently loaded """

        return self._geo.get_plotlabel(self._datatype)

    def hist(self, subplot, bins, **plotopts):

        """ make a histogram with the loaded values """

        self._assert_loaded()
        subplot.hist(self._get_data(),
                     label=self._plotlabel,
                     bins=bins,
                     **plotopts)
        if self._do_set_xlabel:
            subplot.set_xlabel(self._get_texlabel(), fontsize=self._labelsize)
        if plotopts and 'normed' in plotopts and plotopts['normed']:
            subplot.set_ylabel("Probability", fontsize=self._labelsize)
        else:
            subplot.set_ylabel("Counts", fontsize=self._labelsize)

    def densplot(self, subplot):

        val_range = self.get_range()
        lnspc = np.linspace(val_range[0], val_range[1], 1000)
        # sk is sigma for gaussians and kappa for dihedrals
        mean, sk, _ = self.get_fit()
        if self._geotype in ["dist", "ang"]:
            dens = stats.norm.pdf(lnspc, mean, sk)
        elif self._geotype == "dih":
            dens = stats.vonmises.pdf(lnspc, sk, mean)
        subplot.plot(lnspc, dens, color='r')

    def _get_bins(self):

        return get_bins(self.get_bin_width, *self.get_range())

    def _check_filetype(self, filetype):

        if filetype not in ["pdf", "png"]:
            raise RuntimeError("{} is not a valid filetype for a plot figure".format(filetype))

    def write_histograms(self,
                         outdir,
                         filetype="pdf",
                         normed=True,
                         plot_density_func=True):

        """ make a bunch of histograms (one for each datatype)
        and store them in files """

        plotopts = {'normed': normed, 'histtype': 'bar', 'linewidth': 2}
        self._check_filetype(filetype)

        for datatype in self._geo:

            self.load(datatype)

            if not self.get_n_datapoints():
                print "WARING: no data for {} so there won't be any histograms".format(datatype)
                continue

            fig = plt.figure()
            sub = fig.add_subplot(111)

            self.hist(sub, self._get_bins(), plotopts)
            if plot_density_func:
                self.densplot(sub)

            fn = os.path.join(outdir, datatype + "." + filetype)
            plt.tight_layout()
            fig.savefig(fn)
            plt.close(fig)

    def write_histograms_multi(self,
                               datafit2,
                               outdir,
                               filetype="pdf"):

        """ write histograms of two different datasets """

        fontisize_legend = 16
        self.set_labelsize(20)

        plotopts_gen = {'normed': True,
                        'histtype': 'bar',
                        'linewidth': 2,
                        'alpha': 0.5}

        plotopts_spec = [{'color': 'b'},
                         {'color': 'r'}]

        self._check_filetype(filetype)

        datafit_list = [self, datafit2]

        fig = plt.figure()
        sub = fig.add_subplot(111)

        for datatype in self._geo:
            print datatype

            if "dist_nb" in datatype:
                continue

            datafit_histograms_subplot(sub,
                                       datafit_list,
                                       datatype,
                                       plotopts_gen,
                                       plotopts_spec)

            sub.legend(fontsize=fontisize_legend)

            fn = os.path.join(outdir, datatype + "." + filetype)
            plt.tight_layout()
            fig.savefig(fn)
            sub.cla()
        plt.close(fig)

    def write_ff_params(self, fn_out):

        """ returns a dictionairy with force
        constant and optimum for each datatype """

        datatype_start = self._datatype

        with open(fn_out, "w") as f:
            for datatype in self._geo:
                self.load(datatype)
                opt, _,  k = self.get_fit()
                f.write("{} {:.3f} {:.3f}\n".format(datatype, opt, k))

        if datatype_start is not None:
            self.load(datatype_start)

    def histogram_distances(self, *others):

        """ compare to histograms of other datafit objects for all datatypes
        'self' ist the reference set
        returns a list of dictionairies, the list is in order of others,
        the keys are the datatypes"""

        ret = {}
        datafits = [self] + list(others)

        for t in self._geo.datatypes:

            if "dist_nb" in t:
                continue

            for df in datafits:
                df.load(t)
                df.remove_outliers_if_dist(10.)

            bins = get_common_bins(*datafits)

            ret[t] = [(np.abs(self.get_histogram(bins) - other.get_histogram(bins)).sum() / len(bins))
                      for other in others]

        return ret


def merge_dicts(dict_1, *other_dicts):

    ret = dict_1.copy()
    for d in other_dicts:
        ret.update(d)
    return ret


def datafit_histograms_subplot(subplot,
                               datafit_list,
                               datatype,
                               plotopts_general,
                               plotopts_specific):

    for df in datafit_list:
        df.load(datatype)

    if not sum((df.has_data() for df in datafit_list)):
        print("WARNING: no data for "
              "{} so there won't be any histograms".format(datatype))
        return

    bins = get_common_bins(*datafit_list)

    for datafit, plotopts_spec in itertools.izip(datafit_list,
                                                 plotopts_specific):
        datafit.remove_outliers_if_dist(10.)
        if datafit.has_data():
            datafit.hist(subplot,
                         bins,
                         **merge_dicts(plotopts_general,
                                       plotopts_spec))
