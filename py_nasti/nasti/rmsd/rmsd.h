
/*
 * original code from wurst by Andrew Torda, 
 * Nils stole this code to make a python plugin
 *
 * Fitting code was originally written by Wilfred van Gunsteren
 */


#ifndef _XOPEN_SOURCE
#   define _XOPEN_SOURCE 500    /* Necessary to get maths constants */
#endif

#ifndef _RMSD_H
#define _RMSD_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct { double x,y,z; } RPoint;

void 
print_points(const size_t n_points, const RPoint *p);


void 
rmsd_fit_routine(const size_t n_points, 
		 const RPoint *a, const RPoint *b, 
		 RPoint *out, double *rmsd);

double
gdt_ts(const size_t n_atoms, const RPoint *r1, const RPoint *r2);

#endif
