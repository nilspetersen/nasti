
from numpy.ctypeslib import ndpointer
import ctypes
import numpy as np
import os


class RmsdFit:

    def __init__(self):

        """ initialize """

        _src_dir = os.path.dirname(__file__)
        if _src_dir == "":
            _src_dir = os.getcwd()
        self._lib = ctypes.cdll.LoadLibrary(_src_dir+'/rmsd.so')
        self._lib_rmsd_fit = self._lib.rmsd_fit_routine_wrapper
        self._lib_rmsd_fit.restype = ctypes.c_double
        self._lib_rmsd_fit.argtypes = [ctypes.c_size_t,
                                       ndpointer(ctypes.c_double),
                                       ndpointer(ctypes.c_double),
                                       ndpointer(ctypes.c_double)]
    
        self._lib_gdt_ts_fit = self._lib.gdt_ts_fit_wrapper
        self._lib_gdt_ts_fit.restype = ctypes.c_double
        self._lib_gdt_ts_fit.argtypes = [ctypes.c_size_t,
                                         ndpointer(ctypes.c_double),
                                         ndpointer(ctypes.c_double)]

        # methods to compute the gdt-ts
        # and get the superpositions at the 4 levels
        self._lib_gdtts_getsup = self._lib.gdt_ts_fit_wrapper_get_superpositions
        self._lib_gdtts_getsup.restype = ctypes.c_double
        self._lib_gdtts_getsup.argtypes = [ctypes.c_size_t,
                                           ndpointer(ctypes.c_double),
                                           ndpointer(ctypes.c_double),
                                           # superposition indices (return)
                                           ndpointer(ctypes.c_int),
                                           ndpointer(ctypes.c_int),
                                           ndpointer(ctypes.c_int),
                                           ndpointer(ctypes.c_int)]

    def _check_input_get_length(self, coords_a, coords_b):

        """ check if both coordinate-arrays are 3D and the same length """

        n_a, dim_a = coords_a.shape
        n_b, dim_b = coords_b.shape

        if n_a != n_b:
            raise ValueError('dimension mismatch')
        if dim_a != 3 or dim_b != 3:
            raise ValueError('Not 3D!')

        return n_a

    def fit(self, coords_a, coords_b):

        """ fit coordinates to lowest rmsd """

        n = self._check_input_get_length(coords_a, coords_b)

        fitted = np.zeros(3*n, dtype=np.float64)
        rmsd = self._lib_rmsd_fit(n,
                                  coords_a.flatten(),
                                  coords_b.flatten(),
                                  fitted)

        return rmsd, fitted.reshape(n, 3)

    def gdt_ts_fit(self, coords_a, coords_b):

        """ find the best gdt-ts """

        n = self._check_input_get_length(coords_a, coords_b)
        return self._lib_gdt_ts_fit(n,
                                    coords_a.flatten(),
                                    coords_b.flatten())

    def gdt_ts_fit_get_matches(self, coords_a, coords_b):

        """ find the best gdt-ts """

        n = self._check_input_get_length(coords_a, coords_b)

        sup_indices = [np.ones(n, dtype=np.int32) * -1
                       for _ in range(4)]

        gdt_ts = self._lib_gdtts_getsup(n,
                                        coords_a.flatten(),
                                        coords_b.flatten(),
                                        *sup_indices)

        return gdt_ts, sup_indices
