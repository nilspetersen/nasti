
#include "rmsd.h"

/* ---------------- constants ---------------------------------
 * We use the square root of two so often, we can help the compiler
 * by saying it is just one value we re-use.
 */
static const double m_sqrt2 = M_SQRT2;

static void
err_printf(const char *sub, const char *message)
{
    fprintf(stderr,"%s: %s\n", sub, message);
}



void
print_points(const size_t n_points, const RPoint *p)
{
    size_t i;
    for(i=0; i<n_points; ++i) {
	printf("%.3f %.3f %.3f\n", p[i].x, p[i].y, p[i].z);
    }
}

/* ---------------- eigen -------------------------------------
 * This calculates eigenvalues and corresponding eigenvectors.
 * It came from Thomas Huber, who took it from the GROMOS
 * source code...
 CCCCC W.F. VAN GUNSTEREN, CAMBRIDGE, JUNE 1979 CCCCCCCCCCCCCCCCCCCCCCCC
 C
 SUBROUTINE EIGEN (A,R,N,MV)                                      C
 C
 EIGEN COMPUTES EIGENVALUES AND EIGENVECTORS OF THE REAL      C
 SYMMETRIC N*N MATRIX A, USING THE DIAGONALIZATION METHOD         C
 DESCRIBED IN "MATHEMATICAL METHODS FOR DIGITAL COMPUTERS", EDS.  C
 A.RALSTON AND H.S.WILF, WILEY, NEW YORK, 1962, CHAPTER 7.        C
 IT HAS BEEN COPIED FROM THE IBM SCIENTIFIC SUBROUTINE PACKAGE.   C
 C
 A(1..N*(N+1)/2) = MATRIX TO BE DIAGONALIZED, STORED IN SYMMETRIC C
 STORAGE MODE, VIZ. THE I,J-TH ELEMENT (I.GE.J) C
 IS STORED AT THE LOCATION K=I*(I-1)/2+J IN A;  C
 THE EIGENVALUES ARE DELIVERED IN DESCENDING    C
 ORDER ON THE DIAGONAL, VIZ. AT THE LOCATIONS   C
 K=I*(I+1)/2                                    C
 R(1..N,1..N) = DELIVERED WITH THE CORRESPONDING EIGENVECTORS     C
 STORED COLUMNWISE                                 C
 N = ORDER OF MATRICES A AND R                                    C
 MV = 0 : EIGENVALUES AND EIGENVECTORS ARE COMPUTED               C
 = 1 : ONLY EIGENVALUES ARE COMPUTED                           C
 C
 CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
*/
static void
eigen(float *a, float *r__, int *n, int *mv)
{
    int i__1, i__2, i__3;
    float d__1;

    float cosx, sinx, cosx2, sinx2;
    int i__, j, k, l, m;
    float x, y, anorm, sincs, anrmx;
    int ia, ij, il, im, ll, lm, iq, mm, jq, lq, mq, ind, ilq, imq, ilr, imr;
    float thr;
    static const float range = (float) 1e-12;

    /* Parameter adjustments */
    --r__;
    --a;

    if (*mv != 1) {
	iq = -(*n);
	i__1 = *n;
	for (j = 1; j <= i__1; ++j) {
	    iq += *n;
	    i__2 = *n;
	    for (i__ = 1; i__ <= i__2; ++i__) {
		ij = iq + i__;
		r__[ij] = 0.;
		if (i__ - j == 0)
		    r__[ij] = 1.;
	    }
	}
    }

    /****COMPUTE INITIAL AND FINAL NORMS (ANORM AND ANRMX) */
    anorm = 0.0;
    i__2 = *n;
    for (i__ = 1; i__ <= i__2; ++i__) {
	i__1 = *n;
	for (j = i__; j <= i__1; ++j) {
	    if (i__ - j != 0) {
		ia = i__ + (j * j - j) / 2;
		anorm += a[ia] * a[ia];
	    }
	}
    }
    if (anorm > 0.0) {
	anorm = sqrt(anorm) * m_sqrt2;
	anrmx = anorm * range / (float) (*n);

	/*****INITIALIZE INDICATORS AND COMPUTE THRESHOLD, THR */
	thr = anorm;

	do {                       /* ----- while (thr - anrmx > 0.0) ------ */
	    thr /= (float) (*n);
	    ind = 1 /*EXIT_SUCCESS */ ;
	    while (ind) {
		ind = 0 /*EXIT_FAILURE */ ;
		l = 1;
		for (l = 1; l <= *n - 1; l++) {
		    for (m = l + 1; m <= *n; m++) {
			/*****COMPUT SIN AND COS */
			mq = (m * m - m) / 2;
			lq = (l * l - l) / 2;
			lm = l + mq;
			d__1 = a[lm];
			if (fabs(d__1) - thr >= 0.0) {
			    ind = 1 /*EXIT_SUCCESS */ ;
			    ll = l + lq;
			    mm = m + mq;
			    x = (a[ll] - a[mm]) * (float) 0.5;
			    y = -a[lm] / sqrt(a[lm] * a[lm] + x * x);
			    if (x < 0.0)
				y = -y;
			    sinx =
				y / sqrt((sqrt(1.0 - y * y) + 1.0) * 2.0);
			    sinx2 = sinx * sinx;
			    cosx = sqrt(1.0 - sinx2);
			    cosx2 = cosx * cosx;
			    sincs = sinx * cosx;

			    /* *****ROTATE L AND M COLUMNS */
			    ilq = *n * (l - 1);
			    imq = *n * (m - 1);
			    i__1 = *n;
			    for (i__ = 1; i__ <= i__1; ++i__) {
				iq = (i__ * i__ - i__) / 2;
				if (i__ - l != 0) {
				    i__2 = i__ - m;
				    if (i__2 != 0) {
					if (i__2 < 0)
					    im = i__ + mq;
					else
					    im = m + iq;
					if (i__ - l >= 0)
					    il = l + iq;
					else
					    il = i__ + lq;
					x = a[il] * cosx - a[im] * sinx;
					a[im] =
					    a[il] * sinx + a[im] * cosx;
					a[il] = x;
				    }     /* ---- (i__2 != 0) ---- */
				}
				/* ------ if (i__ - l != 0) ---- */
				if (*mv != 1) {
				    ilr = ilq + i__;
				    imr = imq + i__;
				    x = r__[ilr] * cosx - r__[imr] * sinx;
				    r__[imr] =
					r__[ilr] * sinx + r__[imr] * cosx;
				    r__[ilr] = x;
				}
			    }
			    x = a[lm] * 2. * sincs;
			    y = a[ll] * cosx2 + a[mm] * sinx2 - x;
			    x = a[ll] * sinx2 + a[mm] * cosx2 + x;
			    a[lm] =
				(a[ll] - a[mm]) * sincs + a[lm] * (cosx2 -
								   sinx2);
			    a[ll] = y;
			    a[mm] = x;
			}
			/* *****TESTS FOR COMPLETION */
			/* *****TEST FOR M = LAST COLUMN */
		    }    /* ---- for (m = l+1; m <= *n; m++)  ---- */
		    /* *****TEST FOR L = SECOND FROM LAST COLUMN */
		}        /* ----- for (l = 1; l < *n-1; l++) ------ */
	    }               /* --- while (ind) --- */
	} while (thr > anrmx); /* *****COMPARE THRESHOLD WITH FINAL NORM */
    }
    /* ---- if (anorm > 0) ------ */
    /* *****SORT EIGENVALUES AND EIGENVECTORS */
    iq = -(*n);
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	iq += *n;
	ll = i__ + (i__ * i__ - i__) / 2;
	jq = *n * (i__ - 2);
	i__2 = *n;
	for (j = i__; j <= i__2; ++j) {
	    jq += *n;
	    mm = j + (j * j - j) / 2;
	    if (a[ll] - a[mm] < 0.0) {
		x = a[ll];
		a[ll] = a[mm];
		a[mm] = x;
		if (*mv != 1) {
		    i__3 = *n;
		    for (k = 1; k <= i__3; ++k) {
			ilr = iq + k;
			imr = jq + k;
			x = r__[ilr];
			r__[ilr] = r__[imr];
			r__[imr] = x;
		    }
		}
	    }
	}
    }
}

/* ---------------- apply_rot  ----------------------------------
 * apply rotation matrix rmat to structure of size n_points.
 */
static void 
apply_rot(float rmat[3][3], RPoint *r1, size_t n_points)
{
    size_t i;
    RPoint dr;
    for (i = 0; i < n_points; i++) {
	dr.x =
	    rmat[0][0] * r1[i].x + rmat[0][1] * r1[i].y +
	    rmat[0][2] * r1[i].z;
	dr.y =
	    rmat[1][0] * r1[i].x + rmat[1][1] * r1[i].y +
	    rmat[1][2] * r1[i].z;
	dr.z =
	    rmat[2][0] * r1[i].x + rmat[2][1] * r1[i].y +
	    rmat[2][2] * r1[i].z;
	r1[i].x = dr.x;
	r1[i].y = dr.y;
	r1[i].z = dr.z;
    }
}

/* ---------------- lsq_fit  ----------------------------------
 * Least square fit routine to determine the rotation matrix to
 * superimpose coordinates r1 onto coordinates r2.
 * omega is a symmetric matrix in symmetric storage mode:
 * The element [i][j] is stored at position [i*(i+1)/2+j] with i>j
 * Thanks to Wilfred and Thomas Huber.
 */
static int
lsq_fit(const int nr_atoms, const RPoint *r1,
	const RPoint *r2, float R[3][3])
{

    int i, j, ii, jj, n;
    float U[3][3], det_U, sign_detU, sigma;
    float H[3][3], K[3][3];     /*, R[3][3]; */
    float omega[21], eve_omega[36], eva_omega[6];
    static const float SMALL = (float) 1.e-5;
    static const char *this_sub = "lsq_fit";

    /* ----- CALCULATE THE MATRIX U AND ITS DETERMINANT ----- */
    for (i = 0; i < 3; i++) {
	for (j = 0; j < 3; j++) {
	    U[i][j] = 0.0;
	}
    }
    for (n = 0; n < nr_atoms; n++) {
	U[0][0] += r1[n].x * r2[n].x;
	U[0][1] += r1[n].x * r2[n].y;
	U[0][2] += r1[n].x * r2[n].z;
	U[1][0] += r1[n].y * r2[n].x;
	U[1][1] += r1[n].y * r2[n].y;
	U[1][2] += r1[n].y * r2[n].z;
	U[2][0] += r1[n].z * r2[n].x;
	U[2][1] += r1[n].z * r2[n].y;
	U[2][2] += r1[n].z * r2[n].z;
    }
    det_U = U[0][0] * U[1][1] * U[2][2] + U[0][2] * U[1][0] * U[2][1] +
	U[0][1] * U[1][2] * U[2][0] - U[2][0] * U[1][1] * U[0][2] -
	U[2][2] * U[1][0] * U[0][1] - U[2][1] * U[1][2] * U[0][0];

    sign_detU = det_U < 0.0 ? -1.0 : 1.0;

    /* ----- CONSTRUCT OMEGA, DIAGONALIZE IT AND DETERMINE H AND K --- */
    for (i = 0; i < 6; i++) {
	for (j = i; j < 6; j++) {
	    omega[(j * (j + 1) / 2) + i] = 0.0;
	}
    }
    for (j = 3; j < 6; j++) {
	jj = j * (j + 1) / 2;
	for (i = 0; i < 3; i++) {
	    ii = jj + i;
	    omega[ii] = U[i][j - 3];
	}
    }

    i = 6; /* dimension of omega matrix */
    j = 0; /* both, eigenvalues and eigenvectors are calculated */
    eigen(omega, eve_omega, &i, &j);

    for (i = 0; i < 6; i++) {
	eva_omega[i] = omega[i * (i + 1) / 2 + i];
    }
    if (det_U < 0.0) {
	if (fabs(eva_omega[1] - eva_omega[2]) < SMALL) {
	    err_printf(this_sub, "determinant of U < 0 && degenerated eigenvalues\n");
	    return EXIT_FAILURE;
	}
    }
    for (i = 0; i < 3; i++){
	for (j = 0; j < 3; j++) {
	    H[i][j] = m_sqrt2 * eve_omega[j * 6 + i];
	    K[i][j] = m_sqrt2 * eve_omega[j * 6 + i + 3];
	}
    }
    sigma = (H[1][0] * H[2][1] - H[2][0] * H[1][1]) * H[0][2] +
	(H[2][0] * H[0][1] - H[0][0] * H[2][1]) * H[1][2] +
	(H[0][0] * H[1][1] - H[1][0] * H[0][1]) * H[2][2];
    if (sigma <= 0.0) {
	for (i = 0; i < 3; i++) {
	    H[i][2] = -H[i][2];
	    K[i][2] = -K[i][2];
	}
    }
    /* --------- DETERMINE R AND ROTATE X ----------- */
    for (i = 0; i < 3; i++){
	for (j = 0; j < 3; j++) {
	    R[j][i] = K[j][0] * H[i][0] + K[j][1] * H[i][1] + sign_detU * K[j][2] * H[i][2];
	}
    }
    return EXIT_SUCCESS;
}

/* ---------------- calc_RMSD ------------------------------------
 * Calculates the RMSD between nr_atoms Points in arrays r1 and r2
 */
static float
calc_RMSD(const size_t nr_atoms, const RPoint *r1, const RPoint *r2)
{
    double rmsd = 0.0;
    double dr_sqrlength = 0.0;
    RPoint dr;
    int n;
    for (n = 0; n < nr_atoms; n++) {
	dr.x = r1[n].x - r2[n].x;
	dr.y = r1[n].y - r2[n].y;
	dr.z = r1[n].z - r2[n].z;
	dr_sqrlength = dr.x * dr.x + dr.y * dr.y + dr.z * dr.z;
	rmsd += dr_sqrlength;
    }
    rmsd /= nr_atoms;
    rmsd = sqrt(rmsd);
    return (rmsd);
}

double
gdt_ts(const size_t n_atoms, const RPoint *r1, const RPoint *r2)
{
    /* assumes the coordinates are in NANOMETER !*/
    double gdt_ts = 0.0;
    double d;
    size_t i;
    RPoint dr;
    for(i = 0; i < n_atoms; ++i) {
	dr.x = r1[i].x - r2[i].x;
	dr.y = r1[i].y - r2[i].y;
	dr.z = r1[i].z - r2[i].z;
	d = sqrt(dr.x * dr.x + dr.y * dr.y + dr.z * dr.z);
	if(d < 0.1)
	    gdt_ts += 1.0;
	else if(d < 0.2)
	    gdt_ts += 0.75;
	else if(d < 0.4)
	    gdt_ts += 0.5;
	else if(d < 0.8)
	    gdt_ts += 0.25;
    }
    return gdt_ts / n_atoms;
}


static RPoint
calc_CM(const size_t nr_atoms, const RPoint *r1)
{
    unsigned int i;
    float total_mass1 = nr_atoms;
    RPoint cm1;

    cm1.x = 0.0;
    cm1.y = 0.0;
    cm1.z = 0.0;

    for (i = 0; i < nr_atoms; i++) {
	cm1.x += r1[i].x;
	cm1.y += r1[i].y;
	cm1.z += r1[i].z;
    }
    cm1.x /= total_mass1;
    cm1.y /= total_mass1;
    cm1.z /= total_mass1;

    return cm1;
}

/* ---------------- apply_trans -------------------------------
 * Apply the transformation specified by the vector trans to structure
 * of size nr_atoms
 */
static void
apply_trans(const RPoint *trans, RPoint *structure, size_t nr_atoms)
{
    RPoint *r1;
    size_t i;
    const RPoint cm1 = *trans;
    r1 = structure;
    for (i = 0; i < nr_atoms; i++) {
	r1[i].x -= cm1.x;
	r1[i].y -= cm1.y;
	r1[i].z -= cm1.z;
    }
}


void
rmsd_fit_routine(const size_t n_points,
		 const RPoint *a, const RPoint *b,
		 RPoint *out, double *rmsd)
{
    RPoint *a_temp;
    RPoint *b_temp;
    float rotation_matrix[3][3];
    RPoint com_a, com_b, com_a_neg;

    // make copies of both atom-lists
    size_t msize = n_points * sizeof(RPoint);
    a_temp = malloc(msize);
    b_temp = malloc(msize);
    memcpy(a_temp, a, msize);
    memcpy(b_temp, b, msize);

    // remove centre of mass
    com_a = calc_CM(n_points, a_temp);
    apply_trans(&com_a, a_temp, n_points);
    com_b = calc_CM(n_points, b_temp);
    apply_trans(&com_b, b_temp, n_points);

    // least squares fit to get rotation matrix
    // to superimpose b onto a
    lsq_fit(n_points, b_temp, a_temp, rotation_matrix);

    // rotate
    apply_rot(rotation_matrix, b_temp, n_points);

    // rmsd
    *rmsd = calc_RMSD(n_points, b_temp, a_temp);

    // add centre of mass
    com_a_neg.x = -com_a.x;
    com_a_neg.y = -com_a.y;
    com_a_neg.z = -com_a.z;
    apply_trans(&com_a_neg, b_temp, n_points);

    // return stuff
    memcpy(out, b_temp, msize);

    // free memory
    free(a_temp);
    free(b_temp);
}

static RPoint*
get_rpoints(const size_t n_points, 
	    const double *src)
{
    RPoint *r;
    size_t i;
    r = malloc(n_points * sizeof(RPoint));
    for(i = 0; i < n_points; ++i) {
	r[i].x = src[i*3+0];
	r[i].y = src[i*3+1];
	r[i].z = src[i*3+2];
    }
    return r;
}

double
rmsd_fit_routine_wrapper(const size_t n_points,
			 const double *a_,
			 const double *b_,
			 double *out_)
{
    
    RPoint *a, *b, *out;
    double rmsd;
    size_t i;

    out = malloc(n_points * sizeof(RPoint));
    /* copy points from a_ and b_ to RPoint structure arrays */
    a = get_rpoints(n_points, a_);
    b = get_rpoints(n_points, b_);
    
    rmsd_fit_routine(n_points, a, b, out, &rmsd);

    /* copy output to out_ */
    for(i = 0; i < n_points; ++i) {
	out_[i*3+0] = out[i].x;
	out_[i*3+1] = out[i].y;
	out_[i*3+2] = out[i].z;
    }    

    // free memory
    free(a);
    free(b);
    free(out);

    return rmsd;
}

/* 
 * GDT-TS
 */

static double
rpoint_dist(const RPoint *r1, const RPoint *r2) 
{
    RPoint dr;
    dr.x = r1->x - r2->x;
    dr.y = r1->y - r2->y;
    dr.z = r1->z - r2->z;
    return sqrt((dr.x * dr.x) + (dr.y * dr.y) + (dr.z * dr.z));
}

static void
swap_coords(RPoint *bb_target, size_t i, size_t j)
{
    RPoint swap;
    swap = bb_target[i];
    bb_target[i] = bb_target[j];
    bb_target[j] = swap;
}

static void
swap_indices(int *index_list, size_t i, size_t j)
{
    int swap;
    swap = index_list[i];
    index_list[i] = index_list[j];
    index_list[j] = swap;
}

static double
gdt_ts_fit(const size_t n_points,
	   const RPoint *a,
	   const RPoint *b,
	   int **superpos_matches)
{
    static const char *this_sub = "gdt_ts_fit";
    RPoint *a_temp, *b_temp;
    float rotation_matrix[3][3];
    RPoint com_a, com_b;
    
    const double THRESHOLDS[] = {0.8, 0.4, 0.2, 0.1}; /* NANOMETER ! */
    const size_t N_THRESH=4;
    size_t thresh_i=0, i;
    double dist, dist_temp;
    size_t  distmax_i, matched=0;
    size_t n_points_left=n_points;
    int e;
    int *index_list=NULL;

    /* create the index list if neccessary */
    if(superpos_matches) {
	index_list = malloc(n_points * sizeof(*index_list));
	for(i = 0; i < n_points; ++i) {
	    index_list[i] = i;
	}
	for(i = 0; i < N_THRESH; ++i) {
	    memset(superpos_matches[i], -1, n_points * sizeof(**superpos_matches));
	}
    }
    
    /* make copies of both atom-lists */
    size_t msize = n_points * sizeof(RPoint);
    a_temp = malloc(msize);
    b_temp = malloc(msize);
    memcpy(a_temp, a, msize);
    memcpy(b_temp, b, msize);

    while(n_points_left > 3 && thresh_i < N_THRESH) {

	/* remove centre of mass */
	com_a = calc_CM(n_points_left, a_temp);
	apply_trans(&com_a, a_temp, n_points_left);
	com_b = calc_CM(n_points_left, b_temp);
	apply_trans(&com_b, b_temp, n_points_left);
	
	/* least squares fit to get rotation matrix */
	e = lsq_fit(n_points_left, b_temp, a_temp, rotation_matrix);
	if(e) {
	    err_printf(this_sub, "Could not find rotation matrix\n");
	    /* matched = 0; */
	    goto exit;
	}
	
	/* superimpose b onto a */
	apply_rot(rotation_matrix, b_temp, n_points_left);
	
	/* find the largest distance */
	dist = rpoint_dist(a_temp, b_temp);
	distmax_i = 0;
	for(i = 1; i < n_points_left; ++i) {
	    dist_temp = rpoint_dist(a_temp + i, b_temp + i); 
	    if(dist_temp > dist) {
		dist = dist_temp;
		distmax_i = i;
	    }
	}
	
	/* is a new_threshold reached? (increase index until a threshold is not fulfilled) */
	while(thresh_i < N_THRESH) {
	    if(dist > THRESHOLDS[thresh_i])
		break;
	    matched += n_points_left;
	    
	    /* copy the indices  */
	    if(superpos_matches) {
		memcpy(superpos_matches[thresh_i],
		       index_list,
		       n_points_left * sizeof(**superpos_matches));
	    }
	    
	    thresh_i++;
	}

	/* swap */
	--n_points_left;
	swap_coords(a_temp, distmax_i, n_points_left);
	swap_coords(b_temp, distmax_i, n_points_left);
	if(index_list) {
	    swap_indices(index_list, distmax_i, n_points_left);
	}
    }
    
exit:
    
    /* free memory */
    free(a_temp);
    free(b_temp);
    if(index_list) {
	free(index_list);
    }
    return (double) matched / (4.0 * n_points);
}

double
gdt_ts_fit_wrapper(const size_t n_points,
		   const double *a_,
		   const double *b_)
{
    RPoint *a, *b;
    double gdt_ts;
    
    /* copy points from a_ and b_ to RPoint structure arrays */
    a = get_rpoints(n_points, a_);
    b = get_rpoints(n_points, b_);
    
    gdt_ts = gdt_ts_fit(n_points, a, b, NULL);
    
    /* free memory */
    free(a);
    free(b);

    return gdt_ts;
}


double
gdt_ts_fit_wrapper_get_superpositions(const size_t n_points,
				      const double *a_,
				      const double *b_,
				      int *out_indices_1,
				      int *out_indices_2,
				      int *out_indices_3,
				      int *out_indices_4)
{
    RPoint *a, *b;
    double gdt_ts;
    int *out_indices[4] = {out_indices_1,
			   out_indices_2,
			   out_indices_3,
			   out_indices_4};
    
    /* copy points from a_ and b_ to RPoint structure arrays */
    a = get_rpoints(n_points, a_);
    b = get_rpoints(n_points, b_);
    
    gdt_ts = gdt_ts_fit(n_points, a, b, out_indices);
    
    /* free memory */
    free(a);
    free(b);

    return gdt_ts;
}
