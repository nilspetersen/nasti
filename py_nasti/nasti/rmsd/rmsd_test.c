
#include "rmsd.h"

void test() {
  RPoint a[4],b[4],out[4];
  double rmsd;
  const size_t n = 4;

  // first structure
  a[0].x=1;
  a[0].y=1;
  a[0].z=1;
  
  a[1].x=1;
  a[1].y=2;
  a[1].z=1;
  
  a[2].x=1;
  a[2].y=2;
  a[2].z=2;

  a[3].x=2;
  a[3].y=2;
  a[3].z=2;

  // second structure
  b[0].x=1;
  b[0].y=3;
  b[0].z=1;
  
  b[1].x=1;
  b[1].y=3;
  b[1].z=2;
  
  b[2].x=1;
  b[2].y=2;
  b[2].z=2;

  b[3].x=2;
  b[3].y=2;
  b[3].z=2;
  
  rmsd_fit_routine(n, a, b, out, &rmsd);  
  printf("A:\n");
  print_points(n, a);
  printf("B:\n");
  print_points(n, b);
  printf("New B:\n");
  print_points(n, out);
  printf("RMSD=%.3f\n", rmsd);
}

int main() {
  test();
  return EXIT_SUCCESS;
}
