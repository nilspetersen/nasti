#!/usr/bin/env python

import rmsdfit
import numpy as np

def read_coords_from_pdb(fname):
    coords = []
    with open(fname) as f:
        for l in f:
            if not (l[0:4]=="ATOM" or l[0:6]=="HETATM"):
                continue
            if not(l[13:16] == "C3'" or l[13:16] == "C3*"):
                continue
            coords.append([float(l[30:38]), float(l[38:46]), float(l[46:54])])
    return coords

def write_pdbfile(coords, fname):
    with open(fname, "w") as f:
        for i, r in enumerate(coords):
            line ="ATOM  {0:5d}  C3'   G  {0:4d}    {1:8.3f}{2:8.3f}{3:8.3f}\n".format(i+1, r[0], r[1], r[2])
            f.write(line)

def fname_overlay(orig_fname, identifier="A"):
    return orig_fname.split("/")[-1].split(".pdb")[0] + "_fit" + identifier + ".pdb"

def main():
    
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("pdbfile1")
    parser.add_argument("pdbfile2")
    args = parser.parse_args()
    
    fin_a = args.pdbfile1
    fin_b = args.pdbfile2
    coords_a = np.array(read_coords_from_pdb(fin_a))
    coords_b = np.array(read_coords_from_pdb(fin_b))
    min_rmsd, fitted_coords = rmsdfit.RmsdFit().rmsd_fit(coords_a, coords_b)
    
    write_pdbfile(coords_a, fname_overlay(fin_a, "A"))
    write_pdbfile(fitted_coords, fname_overlay(fin_b, "B"))
    print(min_rmsd)


if __name__ == "__main__":
    main()
