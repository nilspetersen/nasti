from numpy import loadtxt, histogram, floor, arange, linspace
from numpy import concatenate, log, ones, zeros, copy, float64
from numpy import array, pi
from scipy.interpolate import UnivariateSpline, splrep
from math import isnan

class BSplineFit:
        
    def __init__(self, x, y, degree, smoothing, weights=None):
        t, c, k = splrep(x, y, s=smoothing, k=degree, w=weights)
        self.t = t # knots
        self.c = c # b-spline coefficients
        self.k = k # spline degree
        self.has_angle_extension=False
        self.bin_params=None
        if self.k == 2:
            self.compute_bin_params()
        
    def nofknots(self):
        return self.t.size

    def find_bin(self, x):
        for i in range(len(self.t)-1):
            if x >= self.t[i] and x < self.t[i+1]:
                return i
        return None

    def alt_spline(self, x, i, l):
        # alternative spline computation
        # from de Boor paper (on calculating with B-Splines)
        # computes a normalised B-Spline
        t_i  = self.t[i]
        t_il = self.t[i+l]
        if l > 1:
            f1 = (x - t_i) / (self.t[i+l-1] - t_i) 
            f2 = (t_il - x) / (self.t[i+l] - self.t[i+1])
            s1 = self.alt_spline(x, i,l-1)
            s2 = self.alt_spline(x, i+1,l-1)
            if s1 == 0:
                f1s1 = 0
            else:
                f1s1 = f1*s1
            if s2 == 0:
                f2s2 = 0
            else:
                f2s2 = f2*s2
            return f1s1 + f2s2
        if x >= t_i and x < t_il:
            return 1
        return 0

    def alt_spline_vals(self,x):
        bin = self.find_bin(x)
        if bin is None:
            print("Warning: value {} has no bin".format(x))
            return 0
        ret = 0
        for j in range(self.k+1):
            i = bin-j
            ret += self.c[i] * self.alt_spline(x, i, self.k+1)
        return ret

    def alt_evaluate(self, xvals):
        y = zeros(xvals.size)
        for i,x in enumerate(xvals):
            y[i] = self.alt_spline_vals(x)
        return y
        
    # get the parameters
    def assert_k_eq_2(self):
        if self.k != 2:
            raise RuntimeError("k has to be two here!")
    
    def part_1(self, i):
        t_i = self.t[i]
        t_i1 = self.t[i+1]
        t_i2 = self.t[i+2]
        c = (t_i2-t_i) * (t_i1-t_i)
        quad = 1. / c
        lin = -2*t_i / c
        const = t_i**2 / c
        return array([const, lin, quad])
    
    def part_2(self, i):
        t_i = self.t[i]
        t_i1 = self.t[i+1]
        t_i2 = self.t[i+2]
        t_i3 = self.t[i+3]
        # part A
        c_a = (t_i2-t_i)*(t_i2-t_i1)
        quad_a = -1. / c_a
        lin_a = (t_i+t_i2) / c_a
        const_a = -t_i*t_i2 / c_a
        # part B
        c_b = (t_i3-t_i1)*(t_i2-t_i1)
        quad_b = -1. / c_b
        lin_b = (t_i1+t_i3) / c_b
        const_b = -t_i1*t_i3 / c_b
        # merge A and B
        quad = quad_a + quad_b
        lin = lin_a + lin_b
        const = const_a + const_b
        return array([const, lin, quad])
    
    def part_3(self, i):
        t_i1 = self.t[i+1]
        t_i2 = self.t[i+2]
        t_i3 = self.t[i+3]
        c = (t_i3-t_i1) * (t_i3-t_i2)
        quad = 1. / c
        lin = -2*t_i3 / c
        const = t_i3**2 / c
        return array([const, lin, quad])

    def compute_bin_params(self):
        self.assert_k_eq_2()
        n_knots = len(self.t)
        n_bins = n_knots - 5
        self.bin_params = zeros((n_bins,3), dtype=float64)
        for i in range(n_bins):
            ii = i + 2
            self.bin_params[i] += self.c[ii] * self.part_1(ii)
            self.bin_params[i] += self.c[ii-1] * self.part_2(ii-1)
            self.bin_params[i] += self.c[ii-2] * self.part_3(ii-2)

    def add_angle_extension(self):
        if self.bin_params is None:
            raise RuntimeError("Bin-parameters have to be computed before computing the extension for large angles")
        if self.has_angle_extension:
            raise RuntimeError("angle extension can only be computed once!")
        f = self.bin_params[-1]
        x0 = self.t[-1]
        x1 = pi
        df_x0 = f[1] + 2*f[2]*x0
        c = df_x0 / (2*(x0-x1))
        b = -2*c*x1
        f_x0 = f[0] + f[1]*x0 + f[2]*x0**2
        a = f_x0 - b*x0 - c*x0**2
        self.bin_params = concatenate((self.bin_params, array([[a,b,c]])))
        self.has_angle_extension = True

    def evaluate_with_bins(self, xvals):
        yvals = zeros(len(xvals), dtype=float64)
        for i,x in enumerate(xvals):
            bin = self.find_bin(x)
            if bin is None:
                if self.has_angle_extension:
                    if x >= self.t[-1] and x <= pi:
                        bin = -1
                    else:
                        bin = 2 # expand the first bin
            if bin is None:
                print("Warning in 'evaluate_with_bins': value {:.5f} has no bin".format(x))
                continue
            if bin != -1:
                bin = bin - 2
            bin_par = self.bin_params[bin]
            yvals[i] = bin_par[0] + bin_par[1]*x + bin_par[2]*x**2
        return yvals

    def openmm_energy_formula(self):
        formula=""
        npar = len(self.bin_params)
        for i in range(npar):
            if i>0:                
                formula += "step(theta-t{})*".format(i)
            formula += "(a{0}+b{0}*theta+c{0}*theta^2)".format(i)
            if i<npar-1:
                formula += "*step(t{}-theta)".format(i+1)
                formula += "+"
        for i, p in enumerate(self.bin_params / 1000.): # /1000 to get kJ
            formula += ";\na{0}={1};\nb{0}={2};\nc{0}={3}".format(i, p[0], p[1], p[2])
        for i in range(npar):
            formula += ";\nt{0}={1}".format(i,self.t[i+2])
        return formula
