
import functools
import os
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
from matplotlib import rc
from scipy import stats

from nasti.rnaC3struct import RnaC3Struct
from nasti.datafit import DataFit, datafit_histograms_subplot
from nasti.utils import mkdir_safe, make_pathjoinfunc


class BoxPlot:

    """ for making boxplots of rmsd and gdt_ts scores """

    def __init__(self, groupsize=2):

        self.data = []
        self.labels = []
        self.groupsize = groupsize
        self.colors = ['#751aff', '#990000', '#ff99ff', '#9999ff']
        self.ylim = None
        self.ylabel = None
        self.grouplabels = None

    def _assert_length(self, *args):

        if len(args) != self.groupsize:
            msg = ("Boxplot groupsize ({}) does not".format(len(args)) +
                   " fit number of parameters {}".format(self.groupsize))
            raise RuntimeError(msg)

    def set_grouplabels(self, *args):

        self._assert_length(*args)
        self.grouplabels = list(args)

    def add_data_group(self, label, *data):

        """ add a group of data with a label, each parameter in *data will
        result be plotted as a box and should be an array. """

        self._assert_length(*data)
        self.labels.append(label)
        for d in data:
            self.data.append(d)

    def set_colors(self, *colors):

        self._assert_length(*colors)
        self.colors = list(colors)

    def set_ylim(self, lo, up):

        self.ylim = [lo, up]

    def set_ylabel(self, ylabel):

        self.ylabel = ylabel

    def _get_color(self, i):

        return self.colors[i % self.groupsize]

    def plot(self, outname=None, ax=None):

        show_or_save = False
        if ax is None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            show_or_save = True

        # compute positions of the boxplots
        n_groups = len(self.labels)
        s_g = self.groupsize
        pos = [i * (s_g + 1) + 1 + j
               for i in range(n_groups)
               for j in range(s_g)]

        # compute the tick-positions
        tick_pos = [float(sum(pos[i*s_g:i*s_g+s_g])) / float(s_g)
                    for i in range(n_groups)]

        bp = ax.boxplot(self.data,
                        positions=pos,
                        widths=0.6)

        ax.set_xticklabels(self.labels)
        ax.set_xticks(tick_pos)

        # remove ticks
        ax.get_xaxis().tick_bottom()
        ax.get_yaxis().tick_left()

        # set the colors
        for element in ['boxes', 'medians']:
            for i, e in enumerate(bp[element]):
                e.set(color=self._get_color(i), linewidth=1)

        for element in ['whiskers', 'caps']:
            for i, e in enumerate(bp[element]):
                e.set(color=self._get_color(i/2), linewidth=1)

        # set the face color? how?
        # box.set(facecolor=color) # why does this not work ???
        for i, flier in enumerate(bp['fliers']):
            flier.set(color=self._get_color(i), alpha=0.2, marker='.',
                      markeredgecolor=self._get_color(i))

        if self.ylim:
            ax.set_ylim(self.ylim)

        if self.ylabel:
            ax.set_ylabel(self.ylabel)

        if self.grouplabels:

            handles = [matplotlib.patches.Patch(color=self._get_color(i))
                       for i in range(self.groupsize)]

            ax.legend(handles=handles,
                      labels=self.grouplabels,
                      loc=2,
                      fancybox=True,
                      framealpha=0.5)

        if show_or_save:
            if outname is None:
                plt.show()
            else:
                plt.savefig(outname, bbox_inches='tight')
                plt.close(fig)


def plot_thermodata(simdir, fname_out=None, phase="both"):

    """ plot thermodata from a simulation
    IN:
    simdir = output directory of a nasti simulation
    phase = e, p, b (equilibration, production, both)
    """

    # load data
    if phase not in ["both", "eq", "prod"]:
        raise RuntimeError("{} is not a valid parameter for phase".format(phase))

    data = None
    fn_eq = os.path.join(simdir, "eq.thermodat")
    fn_prod = os.path.join(simdir, "prod.thermodat")

    if phase == "eq":
        data = np.loadtxt(fn_eq)
    elif phase == "prod":
        data = np.loadtxt(fn_prod)
    elif phase == "both":
        data = np.loadtxt(fn_eq)
        data = np.concatenate((data, np.loadtxt(fn_prod)))

    steps = data[:, 0]
    e_pot = data[:, 2]
    e_kin = data[:, 1]
    e_tot = e_pot + e_kin
    temp = data[:, 3]

    # plot
    fig = plt.figure()
    sub_pot = fig.add_subplot(411)
    sub_kin = fig.add_subplot(412)
    sub_tot = fig.add_subplot(413)
    sub_temp = fig.add_subplot(414)

    sub_pot.plot(steps, e_pot)
    sub_kin.plot(steps, e_kin)
    sub_tot.plot(steps, e_tot)
    sub_temp.plot(steps, temp)

    # set labels
    sub_pot.set_ylabel("Potential Energy")
    sub_kin.set_ylabel("Kinetic Energy")
    sub_tot.set_ylabel("Total Energy")
    sub_temp.set_ylabel("Temperature")

    sub_tot.set_xlabel("step")

    if fname_out is None:
        plt.show()
    else:
        plt.savefig(fname_out)
        plt.close(fig)


def hist_thermodata(simdir, fname_out, phase='both'):

    """ histograms for thermodata from a simulation
    IN:
    simdir = output directory of a nasti simulation
    phase = e, p, b (equilibration, production, both)
    """

    # load data
    if phase not in ["both", "eq", "prod"]:
        raise RuntimeError("{} is not a valid parameter for phase".format(phase))

    data = None
    fn_eq = os.path.join(simdir, "eq.thermodat")
    fn_prod = os.path.join(simdir, "prod.thermodat")

    if phase == "eq":
        data = np.loadtxt(fn_eq)
    elif phase == "prod":
        data = np.loadtxt(fn_prod)
    elif phase == "both":
        data = np.loadtxt(fn_eq)
        data = np.concatenate((data, np.loadtxt(fn_prod)))

    e_pot = data[:, 2]
    e_kin = data[:, 1]
    e_tot = e_pot + e_kin
    temp = data[:, 3]

    nbins = int(np.sqrt(float(len(e_pot))))

    # plot
    fig = plt.figure()
    sub_pot = fig.add_subplot(221)
    sub_kin = fig.add_subplot(222)
    sub_tot = fig.add_subplot(223)
    sub_temp = fig.add_subplot(224)

    sub_pot.hist(e_pot, bins=nbins, normed=True)
    sub_kin.hist(e_kin, bins=nbins, normed=True)
    sub_tot.hist(e_tot, bins=nbins, normed=True)
    sub_temp.hist(temp, bins=nbins, normed=True)

    # set labels
    sub_pot.set_xlabel("Potential Energy (mean={:.2f})".format(e_pot.mean()))
    sub_kin.set_xlabel("Kinetic Energy (mean={:.2f})".format(e_kin.mean()))
    sub_tot.set_xlabel("Total Energy (mean={:.2f})".format(e_tot.mean()))
    sub_temp.set_xlabel("Temperature (mean={:.2f})".format(temp.mean()))

    # add gaussians
    x = np.linspace(e_pot.min(), e_pot.max(), 1000)
    y = stats.norm.pdf(x, e_pot.mean(), e_pot.std())
    sub_pot.plot(x, y, color='r')

    x = np.linspace(e_kin.min(), e_kin.max(), 1000)
    y = stats.norm.pdf(x, e_kin.mean(), e_kin.std())
    sub_kin.plot(x, y, color='r')

    x = np.linspace(e_tot.min(), e_tot.max(), 1000)
    y = stats.norm.pdf(x, e_tot.mean(), e_tot.std())
    sub_tot.plot(x, y, color='r')

    x = np.linspace(temp.min(), temp.max(), 1000)
    y = stats.norm.pdf(x, temp.mean(), temp.std())
    sub_temp.plot(x, y, color='r')

    plt.tight_layout()

    if fname_out is None:
        plt.show()
    else:
        plt.savefig(fname_out)
        plt.close(fig)


def get_numbered_subdirs(d):

    """ get numbered subdirectories (usually numbered by seed) """

    subdirs = [os.path.join(d, sd) for sd in os.listdir(d)
               if os.path.isdir(os.path.join(d, sd)) and sd.isdigit()]
    return subdirs


def one_case_thermoplot(directory):

    """ function for the threadpool """

    add_path = make_pathjoinfunc(directory)
    for phase in ['eq', 'both', 'prod']:
        fn_out = add_path(phase + '.pdf')
        plot_thermodata(directory, fname_out=fn_out, phase=phase)
        fn_hist_out = add_path(phase + '_hist.pdf')
        hist_thermodata(directory, fname_out=fn_hist_out, phase=phase)


def thermoplots(directories, mapfunc=map):

    """ make thermodata plots for a list of directories
    with simulation data """

    mapfunc(one_case_thermoplot, directories)


def calc_geometries_one_case(directory, dirname_traj_geo):

    in_path = make_pathjoinfunc(directory)
    rna = RnaC3Struct()
    rna.load_binary(in_path('rna.bin'))
    rna.load_trajectory_in(in_path('prod.traj'))
    traj_geo_dir = in_path(dirname_traj_geo)
    mkdir_safe(traj_geo_dir)
    rna.compute_and_save_geometries_trajectory(traj_geo_dir)


def calc_geometries(directories,
                    merge_dir,
                    dirname_traj_geo,
                    mapfunc):

    """ calculate and merge all the distances, angles, dihedrals """

    # calculate the geometries for each simulation ..
    tmp_func_calc_geom = functools.partial(calc_geometries_one_case,
                                           dirname_traj_geo=dirname_traj_geo)

    mapfunc(tmp_func_calc_geom, directories)

    # .. and merge them
    dirs2merge = [os.path.join(d, dirname_traj_geo) for d in directories]
    mkdir_safe(merge_dir)
    dfit = DataFit(merge_dir)
    dfit.merge_bin(dirs2merge)


def plot_geometries(merge_dir,
                    outdir_hist,
                    dir_refdata,
                    plotlabel_data,
                    plotlabel_refdata):

    """ make histograms of distances, angles, and dihedrals saved
    in merge_dir and dir_refdata, the files will be writen to outdir_hist """

    refdata = DataFit(dir_refdata)
    refdata.set_plotlabel(plotlabel_refdata)
    dfit = DataFit(merge_dir)
    dfit.set_plotlabel(plotlabel_data)
    mkdir_safe(outdir_hist)
    refdata.write_histograms_multi(dfit, outdir_hist)


def plot_geometries_each(dir_refdata, directories, dirname_traj_geo):

    ''' make geometry-histograms for each simulation run separately '''

    refdata = DataFit(dir_refdata)
    for d in directories:
        geo_dir = os.path.join(d, dirname_traj_geo)
        outdir_hist = os.path.join(d, 'histograms')
        mkdir_safe(outdir_hist)
        dfit = DataFit(geo_dir)
        refdata.write_histograms_multi(dfit, outdir_hist)


def calc_and_plot_geometries(directories,
                             outdir,
                             dir_refdata,
                             mapfunc=map,
                             plotlabel_data=None,
                             plotlabel_refdata=None,
                             plot_each=False):

    """ calculate the geometries, save and merge them and finally plot
    and save histograms """

    dirname_traj_geo = 'traj_geo'
    merge_dir = os.path.join(outdir, "merged_geometries")

    calc_geometries(directories,
                    merge_dir,
                    dirname_traj_geo,
                    mapfunc)

    outdir_hist = os.path.join(outdir, 'histograms')

    plot_geometries(merge_dir,
                    outdir_hist,
                    dir_refdata,
                    plotlabel_data,
                    plotlabel_refdata)

    if plot_each:
        plot_geometries_each(dir_refdata, directories, dirname_traj_geo)


def plot_geometries_multi_paper(dir_nast,
                                dir_nasti,
                                dir_ref,
                                fname_out,
                                datatypes,
                                n_cols,
                                n_lines,
                                figsize,
                                tight_layout_rectangle,
                                label_fontsize,
                                legend_fontsize):

    directories = [dir_nast, dir_nasti, dir_ref]
    labels = ['NAST', 'NASTI', 'RIBOSOME']
    # n_lines = int(np.ceil(float(len(datatypes)) / float(n_cols)))

    plotopts_general = {'normed': True, 'histtype': 'step', 'linewidth': 3}
    plotopts_specific = [{'color': 'b', 'linestyle': ':'},
                         {'color': 'r', 'linestyle': '--'},
                         {'color': 'k', 'linestyle': '-'}]

    datafit_list = [DataFit(d) for d in directories]
    for df, lab in zip(datafit_list, labels):
        df.set_plotlabel(lab)
        df.set_labelsize(label_fontsize)

    fig = plt.figure(figsize=figsize)

    for i, datatype in enumerate(datatypes):
        sub = fig.add_subplot(n_lines, n_cols, i + 1)
        datafit_histograms_subplot(sub,
                                   datafit_list,
                                   datatype,
                                   plotopts_general,
                                   plotopts_specific)
    handles, labs = sub.get_legend_handles_labels()
    fig.legend(handles,
               labels,
               loc='upper center',
               ncol=3,
               fontsize=legend_fontsize)
    plt.tight_layout(rect=tight_layout_rectangle)
    plt.savefig(fname_out)
    plt.close(fig)


###################
# RMSD and GDT_TS #
###################


class Best:

    def __init__(self, smaller_is_better):

        self.val = None
        self.index = None
        self.casepath = None
        self.smaller_is_better = smaller_is_better

    def update_smaller(self, values, path):

        minval = values.min()
        if self.val is None or minval < self.val:
            self.val = minval
            self.index = values.argmin()
            self.casepath = path

    def update_larger(self, values, path):

        maxval = values.max()
        if self.val is None or maxval > self.val:
            self.val = maxval
            self.index = values.argmax()
            self.casepath = path

    def update(self, values, path):

        if self.smaller_is_better:
            self.update_smaller(values, path)
        else:
            self.update_larger(values, path)

    def __str__(self):

        return "{} {} {}".format(self.casepath, self.val, self.index)


class BestCases:

    def __init__(self):

        self.rmsd = Best(True)
        self.gdt_ts = Best(False)
        self.energy = Best(True)

    def update(self, path, energies, rmsd, gdt_ts):

        self.rmsd.update(rmsd, path)
        self.gdt_ts.update(gdt_ts, path)
        self.energy.update(energies, path)

    def write_pdbs(self, outdir):

        for best, measure in [(self.rmsd, "rmsd"),
                              (self.gdt_ts, "gdt_ts"),
                              (self.energy, "energy")]:

            rna = RnaC3Struct()
            fn_bin = os.path.join(best.casepath, "rna.bin")
            rna.load_binary(fn_bin)
            rna.load_trajectory_in(os.path.join(best.casepath, "prod.traj"))
            rna.load_trajectory_snapshot(best.index)
            rna.remove_centre_of_mass()
            fn_out = os.path.join(outdir, "best_{}.pdb".format(measure))
            rna.save_pdb(fn_out)

    def __str__(self):

        str_ = ""
        for best, measure in [(self.rmsd, "rmsd"),
                              (self.gdt_ts, "gdt_ts"),
                              (self.energy, "energy")]:
            str_ += "{} {}\n".format(measure, best)
        return str_


def compute_rmsd_gdtts(outdir, simdirs, fn_refstruct, make_plots, getbest):

    ''' compute rmsd and gdt-ts score for a bunch of trajectories
    (found in simdirs) with respect to the reference structure, which is
    saved in a binary file,
    if make_plots makes timesteps are plotted against rmsd and gdt_ts
    and saved in the corresponding simulation directory '''

    # read the reference structure
    rna_ref = RnaC3Struct()
    rna_ref.load_binary(fn_refstruct)

    rna_traj = RnaC3Struct()
    rna_traj.load_binary(fn_refstruct)

    fname_gdtts = 'gdt_ts.bin'
    fname_rmsd = 'rmsd.bin'
    fname_epot = 'e_pot.bin'

    fn_epot = os.path.join(outdir, fname_epot)
    fn_rmsd = os.path.join(outdir, fname_rmsd)
    fn_gdtts = os.path.join(outdir, fname_gdtts)

    best_case = None
    if getbest:
        best_case = BestCases()

    with open(fn_epot, 'wb') as f_e_pot, open(fn_rmsd, 'wb') as f_rmsd, open(fn_gdtts, 'wb') as f_gdtts:

        for d in simdirs:
            fn_traj = os.path.join(d, 'prod.traj')

            rna_traj.load_trajectory_in(fn_traj)
            # compute rmsds
            rmsds, gdt_ts = rna_traj.rmsd_gdtts_traj(rna_ref)
            # load energy
            fn_thermodat = os.path.join(d, 'prod.thermodat')
            pot_e = np.loadtxt(fn_thermodat)[:, 2]

            # save locally
            rmsds.tofile(os.path.join(d, fname_rmsd))
            gdt_ts.tofile(os.path.join(d, fname_gdtts))
            pot_e.tofile(os.path.join(d, fname_epot))

            # save energies and rmsd to collection
            rmsds.tofile(f_rmsd)
            pot_e.tofile(f_e_pot)
            gdt_ts.tofile(f_gdtts)

            if getbest:
                best_case.update(d, pot_e, rmsds, gdt_ts)

            # make plots
            if make_plots:

                steps = np.loadtxt(fn_thermodat)[:, 0]

                fig = plt.figure()
                sub_rmsd = fig.add_subplot(211)
                sub_gdtts = fig.add_subplot(212)

                sub_rmsd.plot(steps, rmsds)
                sub_gdtts.plot(steps, gdt_ts)

                sub_rmsd.set_xlabel('steps')
                sub_rmsd.set_ylabel('rmsd')
                sub_gdtts.set_xlabel('steps')
                sub_gdtts.set_ylabel('gdt_ts')
                sub_gdtts.set_ylim([0., 1.])

                fn_rmsd_step_plot = os.path.join(d, 'rmsd_gdtts.png')
                plt.tight_layout()
                plt.savefig(fn_rmsd_step_plot)
                plt.close(fig)

    # load data
    rmsd = np.fromfile(fn_rmsd)
    pot_e = np.fromfile(fn_epot)
    gdt_ts = np.fromfile(fn_gdtts)

    return rmsd, pot_e, gdt_ts, best_case


def plot_measure_vs_energy(name, energy, measure,
                           outdir, xlim, elim, write_outdir):

    ''' plot the measure (rmsd or gdt-ts) versus the energy '''

    fig = plt.figure()
    sub = fig.add_subplot(111)

    sub.plot(measure,
             energy,
             linestyle=' ',
             marker='+')

    label_fontsize = 25

    if name == "rmsd":
        sub.set_xlabel('{} (nm)'.format(name), fontsize=label_fontsize)
    else:
        sub.set_xlabel('{}'.format(name), fontsize=label_fontsize)
    sub.set_ylabel('Energy (kJ)', fontsize=label_fontsize)
    title = 'Mean: {}={:.2f}, E={:.2f}'.format(name,
                                               measure.mean(),
                                               energy.mean())
    if write_outdir:
        title = '{} {}'.format(outdir, title)
    sub.set_title(title, fontsize=20)
    sub.set_xlim(xlim)
    if elim is not None:
        sub.set_ylim(elim)

    sub.tick_params(axis='both', which='major', labelsize=15)
    sub.tick_params(axis='both', which='minor', labelsize=12)

    fn_plot = os.path.join(outdir, '{}.png'.format(name))
    #plt.tight_layout()
    plt.savefig(fn_plot)
    plt.close(fig)


def compute_and_plot_energy_rmsd_gdtts(casedirs,
                                       fn_refstruct,
                                       elim=None,
                                       write_outdir=True):

    ''' for a bunch of directories (casedirs) with subdirectories named by
    the integer randomseeds of the simulation, plot rmsd and gdt_ts versus
    energy in separate plots and save them in the case-directory '''

    for d in casedirs:
        subdirs = get_numbered_subdirs(d)
        rmsd, energy, gdt_ts, best_cases = compute_rmsd_gdtts(d, subdirs,
                                                              fn_refstruct,
                                                              True, True)

        plot_measure_vs_energy('rmsd', energy, rmsd, d,
                               [0, None], elim, write_outdir)

        plot_measure_vs_energy('GDT_TS', energy, gdt_ts, d,
                               [0, 1], elim, write_outdir)

        best_cases.write_pdbs(d)
        print best_cases


def make_histograms_from_binfiles(bin_filenames, x_label, fn_out, hist_labels,
                                  bin_width, xlim=None, opacity=0.5):

    """ used to compare distributions, for example rmsd and gdt-ts """

    colors = ['r', 'b', 'g', 'y']

    fig = plt.figure()
    sub = fig.add_subplot(111)
    for i, fn in enumerate(bin_filenames):
        data = np.fromfile(fn)
        #bins = np.arange(0.,  data.max()+bin_width, bin_width)
        bins = np.arange(0., xlim[1] + bin_width, bin_width)
        sub.hist(data, normed=True, bins=bins,
                 color=colors[i], alpha=opacity,
                 label=hist_labels[i])
        if x_label is not None:
            sub.set_xlabel(x_label, fontsize=25)
        if xlim is not None:
            sub.set_xlim(xlim)
    sub.legend(fontsize=25)
    sub.tick_params(axis='both', which='major', labelsize=20)
    sub.tick_params(axis='both', which='minor', labelsize=15)
    plt.tight_layout()
    plt.savefig(fn_out)
    plt.close(fig)
