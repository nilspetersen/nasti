#!/usr/bin/env python2

import tarfile
import urllib2
import gzip
import os
import re
from nasti.utils import mkdir_safe


class BundleMap:

    """ map for chainIDs in pdb-bundles """

    def __init__(self, pdbdir, pdbid):

        """ will read the bundle map-file """

        self._mapping = {}
        rx = re.compile("....-pdb-bundle(\d+).pdb:")
        bundle_i = None
        fn = "{0}/{1}/{1}-chain-id-mapping.txt".format(pdbdir, pdbid)
        with open(fn, 'r') as f:
            for line in f:
                line = line.strip()
                if len(line) == 0:
                    continue
                match_obj = re.match(rx, line)
                if match_obj:
                    bundle_i = match_obj.groups()[0]
                    continue
                if bundle_i is None:
                    continue
                l = line.split()
                if len(l) != 2:
                    continue
                self._mapping[l[1]] = (bundle_i, l[0])

    def __str__(self):

        """ get a string representation """

        s = "old, new, bundle_i\n"
        for chain_id in self._mapping.iterkeys():
            bundle_i, new_chain_id = self._mapping[chain_id]
            s += "{} {} {}\n".format(chain_id, new_chain_id, bundle_i)
        return s

    def get_info(self, chain_id):

        """ get the file number and the chain_id as it is in this file """

        return self._mapping[chain_id]


class RnaChainInfo:

    """ simple class to store pdbid, chainID and model number """

    def __init__(self, pdbid, model_i, chain_id):

        """ store the given info """

        self.pdbid = pdbid
        self.chain_id = chain_id
        self.model_i = int(model_i)
        self.is_bundle = False
        self.bundle_i = None # in case it is a bundle, it's the number of the file
        self.bundle_file_chain = None # chain id as written in the pdbfile

    def add_bundle_info(self, bundle_map):

        """  get info which file and chain to use from the pdb bundles """

        (self.bundle_i, self.bundle_file_chain) = bundle_map.get_info(self.chain_id)
        self.is_bundle = True

    def __eq__(self, other):

        """ equal? """

        return(
            self.pdbid == other.pdbid and
            self.chain_id == other.chain_id and
            self.model_i == other.model_i and
            self.is_bundle == other.is_bundle and
            self.bundle_i == other.bundle_i and
            self.bundle_file_chain == other.bundle_file_chain)

    def __hash__(self):

        """ hash based on str() method """

        return(hash(str(self)))

    def __str__(self):

        """ string representation """

        s =  "{} {} {}".format(self.pdbid, self.chain_id, self.model_i)
        if self.is_bundle:
            s += " {} {}".format(self.bundle_i, self.bundle_file_chain)
        return s

    def fname_pdb(self):

        """ get a filename (lowercase) for a pdb- or bundle-file """

        if self.is_bundle:
            return "{0}/{0}-pdb-bundle{1}.pdb".format(self.pdbid, self.bundle_i)
        return self.pdbid + ".pdb"

    def get_chain_id_file(self):

        """ get the chain-id as it is in the file """

        if self.is_bundle:
            return self.bundle_file_chain
        return self.chain_id

    def get_chain_id_absolute(self):

        """ get the chain_id """

        return self.chain_id

    def get_model_i(self):

        """ simple getter for the model index """

        return self.model_i


class StructList:

    """ stores a list of RNA structures """

    def __init__(self):

        """ intitialize """

        self.representatives = {}

    def __eq__(self, other):

        """ equality for all! """

        if not set(self.representatives.keys()) == set(other.representatives.keys()):
            return False
        for pdbid in self.representatives.iterkeys():
            if not set(self.representatives[pdbid]) == set(other.representatives[pdbid]):
                return False
            if len(self) != len(other):
                return False
            return True

    def __len__(self):

        """ number of representative structures """

        l = 0
        for pdbid in self.representatives.iterkeys():
            l += len(self.representatives[pdbid])
        return l

    def __str__(self):

        """ guess! """

        s = ""
        for pdbid in self.representatives.keys():
            for rna_chain_info  in self.representatives[pdbid]:
                s += str(rna_chain_info) + "\n"
        return s

    def iter_rna_chains(self):

        """ iterate and get filename, chain_id and model index in each iteration """

        for pdbid in self.representatives.iterkeys():
            for rna_chain_info in self.representatives[pdbid]:
                yield rna_chain_info

    def write_txt(self, fname):

        """ write to a simple text file """

        with open(fname, "w") as f:
            f.write(str(self))

    def read_txt(self, fname):

        """ read from a simple text file """

        with open(fname, 'r') as f:
            for line in f:
                l = line.strip().split()
                ll = len(l)
                if ll not in [3, 5]:
                    err_msg = "{} is not a correct text-file for StructList object,".format(fname)
                    err_msg += "since a line has {} but should only have 3 or 5 entries".format(ll)
                    raise RuntimeError(err_msg)
                pdbid = l[0]
                rna_chain_info = RnaChainInfo(pdbid, l[2], l[1])
                if ll == 5:
                    rna_chain_info.is_bundle = True
                    rna_chain_info.bundle_i = int(l[3])
                    rna_chain_info.bundle_file_chain = l[4]
                if pdbid in self.representatives:
                    self.representatives[pdbid].append(rna_chain_info)
                else:
                    self.representatives[pdbid] = [rna_chain_info]

    def read_bgsu_list(self, fname):

        """ read a list as formatted on the bgsu website (http://rna.bgsu.edu/rna3dhub/nrlist) """

        self.representatives = {}
        with open(fname, 'r') as f:
            for line in f:
                line = line.strip()
                if len(line) == 0:
                    continue
                line = line[1:-1]
                entries = line.split('","')
                chains = entries[1].split("+")
                for chain in chains:
                    rep = chain.split("|")
                    pdbid = rep[0].lower()
                    chain_id = rep[1]
                    model_i = rep[2]
                    if not pdbid in self.representatives:
                        self.representatives[pdbid] = [RnaChainInfo(pdbid, chain_id, model_i)]
                    else:
                        self.representatives[pdbid].append(RnaChainInfo(pdbid, chain_id, model_i))

    def dl_pdbs(self, pdbdir):

        """ download the pdbfiles """

        mkdir_safe(pdbdir)

        for pdbid in self.representatives.keys():
            # check if the file already exists
            fn_pdb = os.path.join(pdbdir, pdbid + ".pdb")
            if os.path.exists(fn_pdb):
                continue
            dir_bundle = os.path.join(pdbdir, pdbid)
            if os.path.exists(dir_bundle):
                bundle_map = BundleMap(pdbdir, pdbid)
                for rna_chain_info in self.representatives[pdbid]:
                    rna_chain_info.add_bundle_info(bundle_map)
                continue
            # try to download a simple file first ..
            try:
                url_pdb = "https://files.rcsb.org/download/{}.pdb.gz".format(pdbid.upper())
                #urllib2.urlcleanup()
                dl_handler = urllib2.urlopen(url_pdb)
                fn_pdb = os.path.join(pdbdir, pdbid + ".pdb")
                fn_pdb_gz = '{}.gz'.format(fn_pdb)
                with open(fn_pdb_gz, "wb") as f_gz:
                    f_gz.write(dl_handler.read())
                gz_handler = gzip.open(fn_pdb_gz, "rb")
                with open(fn_pdb, "wb") as pdb_file:
                    pdb_file.writelines(gz_handler)
                gz_handler.close()
                os.remove(fn_pdb_gz)
            # .. and if it fails try to get a bundle
            except urllib2.HTTPError:
                fn_bundle_targz = "{}-pdb-bundle.tar.gz".format(pdbid)
                url_bundle = "http://ftp.wwpdb.org/pub/pdb/compatible/pdb_bundle/{0}/{1}/{1}-pdb-bundle.tar.gz".format(pdbid[1:3], pdbid)
                #urllib.urlcleanup()
                dl_handler = urllib2.urlopen(url_bundle)
                with open(fn_bundle_targz, "wb") as f_targz:
                    f_targz.write(dl_handler.read())

                tar = tarfile.open(fn_bundle_targz)
                tar.extractall(dir_bundle)
                tar.close()
                os.remove(fn_bundle_targz)

                # get bundle mapping and store that stuff !!!
                bundle_map = BundleMap(pdbdir, pdbid)
                for rna_chain_info in self.representatives[pdbid]:
                    rna_chain_info.add_bundle_info(bundle_map)


if __name__ == "__main__":

    fn_bgsu = "/local/petersen/projects/nasti/tests/data/testlist.csv"
    struct_list = StructList()
    struct_list.read_bgsu_list(fn_bgsu)
    print(struct_list)
    struct_list.dl_pdbs("/local/petersen/projects/nasti/tests/data/pdbs/")
    print(struct_list)
    print(len(struct_list))
