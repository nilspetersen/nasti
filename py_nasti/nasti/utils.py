
""" helper functions """

import os
import functools


def mkdir_safe(dir_name):

    """ make a directory if it does not exist,
    this is safe if multiple processes are running
    in parallel """

    try:
        os.makedirs(dir_name)
    except OSError as err:
        if not os.path.exists(dir_name):
            raise err


class Vividict(dict):

    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value

    def get_n_leafs(self):

        ret = 0
        for val in self.itervalues():
            if isinstance(val, type(self)):
                ret += val.get_n_leafs()
            else:
                ret += 1
        return ret


def make_pathjoinfunc(*args):

    return functools.partial(os.path.join, *args)
