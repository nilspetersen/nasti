
import sys
import numpy as np
import os

from nasti.rnaC3struct import RnaC3Struct
from nasti.simulator import Simulation
from nasti.utils import mkdir_safe


class NastiNoValidParticleFoundException(RuntimeError):

    ''' throw this if you cannot add another particle '''

    pass


class RandomUnfold:

    ''' procedure to build an rna model which fits the
    secondary-structure restraints as in previous nast version '''

    def __init__(self, fff, sec, seed, outdir):

        ''' initialize '''

        mkdir_safe(outdir)
        self.outdir = outdir

        self.fff = fff
        self.sec = sec
        self.n = len(self.sec)
        # initialize rna !
        self.rna = RnaC3Struct()
        self.rna.set_traj_maxlen(self.n)

        self.bondlen = fff.get_bb_bondlen()
        self.coords = np.array([[0., 0., 0.], [0., 0., self.bondlen]])
        self.bondlen_sq = self.bondlen**2
        # grep random right_pos
        np.random.seed(seed)  # set the seed here ???
        self.seed = seed  # save the seed for the simulation runs
        self.i_left = np.random.random_integers(self.n-1) - 1
        self.i_right = self.i_left + 1

    def _new_particle(self, ri, other_particles):

        ''' make a new particle next to ri  '''

        # compute nearby particles
        ri_multi = np.repeat([ri], len(other_particles), axis=0)
        dist_sq = ((ri_multi - other_particles)**2).sum(axis=1)
        nearby = other_particles[dist_sq < 4. * self.bondlen_sq]

        # make candidates
        z_list = np.linspace(-self.bondlen, self.bondlen, 20)
        phi_list = np.arange(0, 2*np.pi, 0.3)

        z = np.repeat(z_list, len(phi_list))
        phi = np.tile(phi_list, len(z_list))

        theta = np.arcsin(z / self.bondlen)
        cos_theta = np.cos(theta)
        x = self.bondlen * cos_theta * np.cos(phi)
        y = self.bondlen * cos_theta * np.sin(phi)

        candidates = np.stack((x, y, z), axis=1)
        lc = len(candidates)
        candidates += np.repeat([ri], lc, axis=0)

        # remove candidates that are to close to nearby positions
        ln = len(nearby)
        candis = np.repeat(candidates, ln, axis=0)
        nearbys = np.tile(nearby, (lc, 1))

        distances_sq = ((candis - nearbys)**2).sum(axis=1).reshape((lc, ln))
        min_dist_sq = distances_sq.min(axis=1)

        candidates = candidates[min_dist_sq > self.bondlen_sq]

        if len(candidates) == 0:
            msg = 'Could not add another particle'
            raise NastiNoValidParticleFoundException(msg)

        # random choice of one of the candidates
        return candidates[np.random.random_integers(len(candidates)) - 1]

    def _add_left(self):

        """ add a new start particle """

        if self.i_left > 0:
            new_particle = self._new_particle(self.coords[0], self.coords[1:])
            self.coords = np.concatenate(([new_particle], self.coords))
            self.i_left -= 1

    def _add_right(self):

        """ add a particle at the end of the chain """

        if self.i_right < self.n-1:
            new_particle = self._new_particle(self.coords[-1], self.coords[:-1])
            self.coords = np.concatenate((self.coords, [new_particle]))
            self.i_right += 1

    def _add_particles(self):

        """ add one particle at each end of the chain """

        self._add_left()
        self._add_right()

    def get_fn_traj_out(self):

        """ get the name of the trajectory """

        return os.path.join(self.outdir, 'random_unfold.traj')

    def get_fn_thermodata(self):

        """ get the name of the thermodata-file """

        return os.path.join(self.outdir, 'random_unfold.thermodat')

    def get_fn_final_structure(self, suffix="bin"):

        """ get the name of the final structure """

        return os.path.join(self.outdir, "ru_final."+suffix)

    def build_molecule(self,
                       steps_per_iter,
                       snapshot_interval,
                       preferred_platform,
                       preferred_gpu,
                       verbose,
                       cmm_removal_frequency=1000):

        """ iteratively adds particles and simulates """

        f_thermo = None
        # init the trajectory
        write_trajectory = True
        if snapshot_interval < 1 or snapshot_interval > steps_per_iter\
           or snapshot_interval is None:
            write_trajectory = False
        else:
            # init the thermodata file
            f_thermo = open(self.get_fn_thermodata(), "w")

        if write_trajectory:
            self.rna.init_trajectory_out(self.get_fn_traj_out())

        i = 0

        while self.i_left > 0 or self.i_right < self.n - 1:

            i += 1
            if verbose:
                sys.stdout.write('.')
                if(i % 50 == 0):
                    print ''
                sys.stdout.flush()

            self._add_particles()

            # get the secondary structure substructure
            sec_sub = self.sec.get_substructure(self.i_left,
                                                self.i_right)

            # set the coordinates of the rna and the secondary structure
            self.rna.set_secondary_structure_and_coords(sec_sub, self.coords)

            # set the secondary structure in the force field
            self.fff.set_secondary_structure(sec_sub)

            # initialize the simulation
            simseed = self.i_left**4 + self.i_right**3 + 15 % self.i_right
            sim = Simulation(
                self.rna,
                self.fff,
                simseed,
                self.outdir,
                write_trajectory=write_trajectory,
                verbose=False,
                cmm_removal_frequency=cmm_removal_frequency,
                preferred_platform=preferred_platform,
                preferred_gpu=preferred_gpu)

            # run the simulation
            sim.thermostat_on()
            sim.simulate(steps_per_iter, snapshot_interval, f_thermo)

            # get the coordinates of the last step of the simulation
            self.coords = sim.get_coords_from_context()

        if verbose and i % 50 != 0:
            print ""
        # close the trajectory
        self.rna.close_trajectory_out()

        # close the thermodata file
        if f_thermo:
            f_thermo.close()

        # save the final molecule
        self.rna.set_secondary_structure_and_coords(self.sec, self.coords)
        self.rna.save_binary(self.get_fn_final_structure())
        self.rna.save_pdb(self.get_fn_final_structure("pdb"))
