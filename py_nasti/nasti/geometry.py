
import numpy as np


def angles(r_i, r_j, r_k):

    """ compute angles for lists of atom coordinates """

    r_ij = r_i - r_j
    r_kj = r_k - r_j
    crossnorm = np.linalg.norm(np.cross(r_ij, r_kj), axis=1)
    dot = (r_ij * r_kj).sum(axis=1)
    ang = np.arctan2(crossnorm, dot)
    return ang


def angle(ri, rj, rk):

    """ compute a singe angle for three particles ri, rj, rk """

    return angles(ri.reshape((1,3)), rj.reshape((1,3)), rk.reshape((1,3)))[0]


def dihedrals(r_i, r_j, r_k, r_l):

    """
    compute a list of dihedrals
    IN: ri, rj, rk, rl: lists of atom coordinates
    """

    r_ij = r_i - r_j
    r_kj = r_k - r_j
    r_kl = r_k - r_l

    m = np.cross(r_ij, r_kj)
    n = np.cross(r_kj, r_kl)

    m_norm_sq = (m * m).sum(axis=1)
    n_norm_sq = (n * n).sum(axis=1)

    m_norm = np.sqrt(m_norm_sq)
    n_norm = np.sqrt(n_norm_sq)

    cos_dih = (m * n).sum(axis=1)

    not_0 = cos_dih != 0
    cos_dih[not_0] /= m_norm[not_0] * n_norm[not_0]

    cos_dih[cos_dih > 1.] = 1.
    cos_dih[cos_dih < -1.] = -1.

    dihedral = np.ones(len(cos_dih)) * 1000.

    # close to the singularity in acos(), use asin() instead
    g099 = np.abs(cos_dih) > 0.99

    cross_mn = np.cross(m[g099], n[g099])
    scale = m_norm_sq[g099] * n_norm_sq[g099]

    dihedral[g099] = np.arcsin(np.sqrt((cross_mn * cross_mn).sum(axis=1) / scale))

    g099s0 = g099 * (cos_dih < 0)
    dihedral[g099s0] = np.pi - dihedral[g099s0]

    # else
    s099 = np.logical_not(g099)
    dihedral[s099] = np.arccos(cos_dih[s099])

    # check the sign!
    sign = np.sign((r_ij * n).sum(axis=1))
    sign[sign == 0] = 1
    dihedral *= sign

    return dihedral


def dihedral(ri, rj, rk, rl):

    """ compute a single dihedral """

    return dihedrals(ri.reshape((1, 3)),
                     rj.reshape((1, 3)),
                     rk.reshape((1, 3)),
                     rl.reshape((1, 3)))[0]
