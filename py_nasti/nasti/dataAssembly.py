
import os

from nasti.rnaC3struct import RnaC3Struct
from nasti.structlist import StructList
from nasti.datafit import DataFit
from nasti.utils import mkdir_safe


class DataAssembly:

    """ organizer for downloading structures, extracting
    information and fitting a force field. """

    def __init__(self, base_dir, case):

        """ initialize """

        self.structlist_dl = None
        self.structlist_training = None

        self.init_dir_tree(base_dir, case)

        # read the list for the download-structures
        # if it already exists as txt-file
        fn = self._fn_dl_list()
        if os.path.isfile(fn):
            self.set_dl_list(fn, "txt")

        fn_train = self._fn_training_list()
        if os.path.isfile(fn_train):
            self.set_training_list(fn_train, "txt")

    def _makedir(self, new_dir):

        """ create a directory if it does not exist """

        mkdir_safe(new_dir)

    def init_dir_tree(self, base_dir, case):

        """ creates all the required directories if they do not exist """

        # shared for all cases
        self.base_dir = base_dir
        self._makedir(self.base_dir)
        self.pdb_dir = os.path.join(base_dir, "pdbs")
        self.chain_dir = os.path.join(base_dir, "chains")
        self.geodata_dir = os.path.join(base_dir, "geometries")

        # casedir - there can be multiple !
        self.case_dir = os.path.join(base_dir, "results_{}".format(case))

        # dl
        self.dl_dir = os.path.join(self.case_dir, "dl")
        self.dl_dir_data = os.path.join(self.dl_dir, "data")
        self.dl_dir_plot = os.path.join(self.dl_dir, "plots")

        # training
        self.train_dir = os.path.join(self.case_dir, "train")
        self.train_dir_data = os.path.join(self.train_dir, "data")
        self.train_dir_plot = os.path.join(self.train_dir, "plots")

    def _get_struct_list(self, fname, filetype):

        """ load a struct_list, either from .txt or bgsu file """

        structlist = StructList()
        if filetype == "txt":
            structlist.read_txt(fname)
        elif filetype == "bgsu":
            structlist.read_bgsu_list(fname)
        else:
            err_msg = "{} is not a valid file-type for a structlist".format(filetype)
            raise RuntimeError(err_msg)
        return structlist

    def set_dl_list(self, fname, filetype):

        """ read the download-structlist from .txt or bgsu file """

        self.structlist_dl = self._get_struct_list(fname, filetype)

    def _fn_dl_list(self):

        """ structlist of the downloaded files """

        return os.path.join(self.base_dir, "dl_list.txt")

    def set_training_list(self, fname, filetype):

        """ read the training-structlist from .txt or bgsu file """

        self.structlist_training = self._get_struct_list(fname, filetype)

    def _fn_training_list(self):

        """ filename of the structlist for the trainig data """

        return os.path.join(self.base_dir, "train_list.txt")

    def _fn_chain(self, ch_info):

        """ filename of rna chain files (binary) """

        fn = "{}_{}.rna".format(ch_info.pdbid, ch_info.get_chain_id_absolute())
        return os.path.join(self.chain_dir, fn)

    def _fn_feats(self, ch_info):

        """ filename of features (distances, angles, dihedrals) to rna chains (binary) """

        fn = "{}_{}.npz".format(ch_info.pdbid, ch_info.get_chain_id_absolute())
        return os.path.join(self.geodata_dir, fn)

    def dl_pdbs(self):

        """ download the pdb-file if they do not exist yet """

        self._makedir(self.pdb_dir)
        self.structlist_dl.dl_pdbs(self.pdb_dir)
        self.structlist_dl.write_txt(self._fn_dl_list())

    def parse_chains(self):

        """ parse all the pdb chains and save them """

        self._makedir(self.chain_dir)

        for chain_info in self.structlist_dl.iter_rna_chains():

            # load the structure
            fn = os.path.join(self.pdb_dir, chain_info.fname_pdb())
            rna = RnaC3Struct()
            rna.init_pdb(fn, chain_info.get_chain_id_file(), chain_info.get_model_i())

            # save chain
            fn = self._fn_chain(chain_info)
            rna.save_binary(fn)

    def compute_features(self):

        """ read all the chains, compute the features to each of them """

        self._makedir(self.geodata_dir)

        for chain_info in self.structlist_dl.iter_rna_chains():
            print str(chain_info)
            rna = RnaC3Struct()
            rna.load_binary(self._fn_chain(chain_info))
            rna.compute_and_save_geometries(self._fn_feats(chain_info))

    def _merge_feats(self, structlist, directory):

        """ merge geometric data of structures in a given list """

        if structlist is None:
            return

        self._makedir(directory)
        fnames = [self._fn_feats(ch) for ch in structlist.iter_rna_chains()]
        DataFit(directory).merge(fnames)

    def merge_feats(self):

        """ merge the features if the lists are present """

        self._merge_feats(self.structlist_dl, self.dl_dir_data)
        self._merge_feats(self.structlist_training, self.train_dir_data)

    def _hist(self, datadir, plotdir):

        """ make histograms for a specific dataset [e.g. training] """

        if not os.path.exists(datadir):
            return

        datafit = DataFit(datadir)
        self._makedir(plotdir)
        datafit.write_histograms(plotdir, plot_density_func=True)

    def histograms(self):

        """ make histograms for all datasets """

        self._hist(self.dl_dir_data, self.dl_dir_plot)
        self._hist(self.train_dir_data, self.train_dir_plot)

    def _write_ff(self, datadir, outdir):

        """ write a force field for a case """

        if not os.path.exists(datadir):
            return

        datafit = DataFit(datadir)
        datafit.write_ff_params(os.path.join(outdir, "ffparams.txt"))

    def write_ff(self):

        """ write force fields for all result cases """

        self._write_ff(self.dl_dir_data, self.dl_dir)
        self._write_ff(self.train_dir_data, self.train_dir)
