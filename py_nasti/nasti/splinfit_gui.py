
import shutil
import sys
import os
import time
import Tkinter as tk
import tkFileDialog
import numpy as np
from scipy import constants

import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
from matplotlib.backend_bases import key_press_handler

from nasti.datafit import DataFit
from nasti.splinefit import BSplineFit
from nasti.utils import mkdir_safe


class SplineFitGui(tk.Frame):

    """ a gui application to fit splines """

    def __init__(self, path=None):

        """ initialize """

        tk.Frame.__init__(self, None)
        self.grid()
        self.create_widgets()

        self.df = None
        self.datadir = None
        self.results_dir = None
        self.data = None
        self.submitted = False

        self.datatypes = [
            'ang_bb_000', 'ang_bb_100', 'ang_bb_010', 'ang_bb_110',
            'ang_bb_001', 'ang_bb_101', 'ang_bb_011', 'ang_bb_111'
        ]

        if path is None:
            self.open_directory()
        else:
            self._set_directory(path)
        self.case_i = 0
        self._load()

    def create_widgets(self):

        """ create all the widgets in the window """

        self._make_controls()
        self._make_figure()

    def _labeled_entry(self, parent, txt, default=''):

        """ make a labeled entry """

        frame = tk.Frame(parent)
        frame.grid()

        var = tk.StringVar()
        var.set(default)

        label = tk.Label(frame, text=txt, width=15, anchor=tk.E)
        label.grid(column=0, row=0)

        entry = tk.Entry(frame, textvar=var, width=10)
        entry.grid(column=1, row=0)

        return var

    def _labeled_scale(self, parent, txt, default, from_, to, stepsize):

        """ make a labeled scake """

        frame = tk.Frame(parent)
        frame.grid()

        label = tk.Label(frame, text=txt, width=10, anchor=tk.E)
        label.grid(column=0, row=0)

        var = tk.DoubleVar()
        var.set(default)

        scale = tk.Scale(frame, from_=from_, to=to,
                         resolution=stepsize, showvalue=True,
                         variable=var, orient=tk.HORIZONTAL, width=20)
        scale.grid(column=1, row=0)
        return var

    def _checkbox(self, parent, txt, default=0):

        ''' make a labeled checkbox '''

        frame = tk.Frame(parent)
        frame.grid()

        var = tk.IntVar()
        var.set(default)

        checkbox = tk.Checkbutton(frame, text=txt, variable=var, selectcolor='black')
        checkbox.grid()

        return var

    def _make_controls(self):

        """ create buttons and fields """

        self.control_frame = tk.Frame(self)
        self.control_frame.pack(side=tk.LEFT)

        self.w_str = self._labeled_entry(self.control_frame, 'w', default='')
        self.s_str = self._labeled_entry(self.control_frame, 's', default='1')
        self.pseudocount_str = self._labeled_entry(self.control_frame, 'pseudocount', default='0')

        # cutoff
        self.cutoff = self._labeled_scale(self.control_frame, 'cutoff', 0, 0, 0.0001, 0.00001)
        self.temp = self._labeled_scale(self.control_frame, 'temperature', 300, 250, 350, 10)

        # interpolate checkbox
        self.do_interpolate = self._checkbox(self.control_frame,
                                             'interpolate', 1)

        # submit button
        self.submit_button = tk.Button(self.control_frame, text='Submit', command=self.submit)
        self.submit_button.grid()

        # need a message box
        self.message = tk.StringVar()
        self.message.set("Press submit if variables are set")
        self.message_box = tk.Label(self.control_frame, textvar=self.message, height=7)
        self.message_box.grid()

        # next, previous save control
        next_prev_frame = tk.Frame(self.control_frame)
        next_prev_frame.grid()
        prev_button = tk.Button(next_prev_frame, text="<-", command=self._previous)
        next_button = tk.Button(next_prev_frame, text="->", command=self._next)
        self.case_str = tk.StringVar()
        self.nr_str = tk.StringVar()
        caselabel = tk.Label(next_prev_frame, textvar=self.case_str, height=2)
        nr_label = tk.Label(next_prev_frame, textvar=self.nr_str, height=2)
        save_button = tk.Button(next_prev_frame, text="save", command=self._save)

        prev_button.grid(row=0, column=0)
        next_button.grid(row=0, column=1)
        nr_label.grid(row=1, column=0)
        caselabel.grid(row=1, column=1)
        save_button.grid(row=2, column=0, columnspan=2)

        clear_button = tk.Button(self.control_frame, text='Clear', command=self._clear)
        clear_button.grid()

        # quit button
        self.quit_button = tk.Button(self.control_frame, text='Quit', command=self.end)
        self.quit_button.grid()

        self.ctrl_msg = tk.StringVar()
        ctrl_msg_box = tk.Label(self.control_frame, textvar = self.ctrl_msg, height=2)
        ctrl_msg_box.grid()

    def _make_figure(self):

        """ make the matplotlib canvas """

        # the figure
        self.canvas_frame = tk.Frame(self)
        self.canvas_frame.pack(side=tk.LEFT)

        self.fig = Figure(figsize=(5, 8), dpi=100)

        self.sub_hist = self.fig.add_subplot(211)
        self.sub_fit = self.fig.add_subplot(212)

        self.sub_hist.set_xlim([0, np.pi])
        self.sub_fit.set_xlim([0, np.pi])

        # self.sub_hist.set_title('Data')
        self.sub_hist.set_xlabel('angle')
        self.sub_hist.set_ylabel('p')

        self.sub_fit.set_xlabel('angle')
        self.sub_fit.set_ylabel('E')

        self.canvas = FigureCanvasTkAgg(self.fig, master=self.canvas_frame)
        self.canvas.show()
        self.canvas.get_tk_widget().pack()

        # matplotlib control-bar
        toolbar = NavigationToolbar2TkAgg(self.canvas, self.canvas_frame)
        toolbar.update()

    def end(self):

        """ new quit function """

        self.quit()

    def _clear_sub(self, sub):

        """ clear the second subplot with the fitted curve """

        sub.cla()
        self.canvas.show()

    def _get_float(self, strvar, name, min_=None, max_=None):

        """ get a float from a tkinter str-variable """

        var = None
        try:
            var = float(strvar.get())
        except ValueError:
            raise RuntimeError("{} is not a float!".format(name))
        if min_ is not None:
            if var < min_:
                raise RuntimeError("{} has to be larger than {}!".format(name, min_))
        if max_ is not None:
            if var > max_:
                raise RuntimeError("{} has to be smaller than {}!".format(name, max_))
        return var

    def submit(self):

        """ submit """

        # check input
        w = None
        s = None
        co = self.cutoff.get()
        pseudocount = 0
        try:
            if len(self.w_str.get().strip()):
                w = self._get_float(self.w_str, "w")
                if w == 0:
                    w = None
            s = self._get_float(self.s_str, "s")
            pseudocount = self._get_float(self.pseudocount_str, "pseudocount", 0.)
        except RuntimeError as e:
            self.message.set(str(e))
            self._clear_sub(self.sub_fit)
            return

        # plot
        self.sub_fit.cla()

        if co > 0.:
            n_cut = int(round(len(self.data) * co))
            fit_dat = np.sort(self.data)[n_cut:]
        else:
            fit_dat = self.data

        binned, edges = self._bin_data(fit_dat)
        bw = edges[1] - edges[0]

        counts = None
        # add pseudocount if necessary
        if pseudocount > 0.:
            counts = np.ones(len(binned)+1, dtype=np.float64) * pseudocount
            counts[1:] += binned
            centers = edges - bw / 2.
            #edges_complete = np.concatenate((np.array([edges[0]-bw]), edges))
        else:
            counts = binned
            centers = edges[1:] - bw / 2.
            #edges_complete = edges
            counts = counts.astype(np.float64)

        # compute probabilities and energies
        prob_dens = self._prob_dens(counts, bw)
        energies = -constants.R * self.temp.get() * np.log(prob_dens)

        if self.do_interpolate.get():

            zero_i = np.arange(len(counts))[counts == 0]

            if len(zero_i) > 0:
                idcs = zero_i[:-1] + 1 != zero_i[1:]
                end_pos = np.concatenate((zero_i[:-1][idcs], [zero_i[-1]]))
                start_pos = np.concatenate(([zero_i[0]], zero_i[1:][idcs]))

                for i, sp in enumerate(start_pos):
                    ep = end_pos[i]
                    start_val = energies[sp - 1]
                    end_val = energies[ep + 1]
                    dx = float(end_val - start_val) / (ep - sp + 2)
                    val = start_val

                    for pos in range(sp, ep+1):
                        val += dx
                        energies[pos] = val

        # weights
        weights = None
        if w is not None:
            weights = w * counts

        ## spline fitting
        smoothing_factor = s * 1000000.
        sf = BSplineFit(x=centers, y=energies, degree=2, smoothing=smoothing_factor, weights=weights)
        sf.add_angle_extension()
        self.sf = sf

        ## plotting
        # draw the curve
        x = np.linspace(centers[0], np.pi, 1000)
        fitted = sf.evaluate_with_bins(xvals=x)

        # plot
        self.sub_fit.plot(centers, energies, marker='+', linestyle=' ')
        self.sub_fit.plot(x, fitted, marker='', linestyle='-', color='r')
        self.sub_fit.set_xlim([0, np.pi])
        self.canvas.show()

        # leave a message
        msg = "w = {}\ns = {}\ncutoff = {}\npseudocount = {}\n".format(w, s, co, pseudocount)
        msg += "temperature = {}\n".format(self.temp.get())
        self.message.set(msg)

        self.submitted = True
        self.lastsubmit_w = w
        self.lastsubmit_s = s
        self.lastsubmit_co = co
        self.lastsubmit_temp = self.temp.get()
        self.lastsubmit_pseudocount = pseudocount

    def _bin_data(self, data):

        """ make a histogram """

        # get the bin width from
        # Wang et al. (2015) 3dRNAscore: a distance and
        # torsion angle dependent evaluation function of 3D RNA structures
        bin_width = 3.49 * data.std() / len(data)**(1./3.)
        nbins = int(round((data.max() - data.min()) / bin_width))
        binned_data, bin_edges = np.histogram(data, bins=nbins)
        return binned_data, bin_edges

    def _prob_dens(self, counts, bw):

        """ compute probability density
            bw = bin width
        """

        return counts.astype(np.float64) / counts.sum() / bw

    def _load(self):

        """ load data """

        datatype = self.datatypes[self.case_i]
        self.data = self.df.read(datatype)

        binned_data, bin_edges = self._bin_data(self.data)
        bw = bin_edges[1] - bin_edges[0]
        prob_dens = self._prob_dens(binned_data, bw)

        # plot the histogram
        self._clear_sub(self.sub_hist)
        self._clear_sub(self.sub_fit)
        self.message.set("Press submit if variables are set")

        self.sub_hist.bar(bin_edges[:-1] + bw/2., prob_dens, width=bw)
        self.sub_hist.set_xlim([0, np.pi])
        self.canvas.show()

        # set labels
        self.case_str.set(datatype)
        self.nr_str.set(str(self.case_i+1))
        self.submitted = False

        if os.path.isfile(self._get_fn_params()):
            self._load_params()
            self.submit()

    def _check_directory(self, path):

        """ check if all required files are in the selected path """

        for datatype in self.datatypes:
            if not os.path.isfile(os.path.join(path, datatype+".bin")):
                return False
        return True

    def _set_directory(self, path):

        """ set the datadir path """

        if self._check_directory(path):
            self.datadir = path
            self.df = DataFit(self.datadir)
        else:
            raise RuntimeError("{} is not a valid path".format(path))
        self.results_dir = os.path.join(self.datadir, "splinefits")
        try:
            mkdir_safe(self.results_dir)
        except Exception as e:
            self.results_dir = None
            self.ctrl_msg.set("could not create results directory\nsaving and loading not possible")

    def open_directory(self):

        """ open the directory with all the angle-data (in .bin files) """

        title = "choose a directory"
        while True:
            path = tkFileDialog.askdirectory(title=title)
            try:
                self._set_directory(path)
                return
            except RuntimeError:
                title = "{} is not good, choose another one".format(path)

    def _next(self):

        """ load the next case """

        if self.case_i < len(self.datatypes) - 1:
            self.case_i += 1
            self._load()

    def _previous(self):

        """ load the previous case """

        if self.case_i > 0:
            self.case_i -= 1
            self._load()

    def _get_fn_params(self):

        """ get the filename of a parameter file """

        return os.path.join(self.results_dir, self.datatypes[self.case_i] + ".parameters")

    def _get_fn_plot(self):

        """ get the filename of a pdf-file for the plot """

        return os.path.join(self.results_dir, self.datatypes[self.case_i] + ".pdf")

    def _get_fn_energyterm(self, datatype=None):

        """ get the filename of a python file with an openmm energy term """

        if datatype is None:
            datatype = self.datatypes[self.case_i]
        return os.path.join(self.results_dir, datatype + ".formula")

    def _have_params_file(self):

        """ check if a parameter file exists already """

        return os.path.isfile(self._get_fn_params())

    def _write_params(self):

        """ write params to a parameter-file """

        if not self.submitted:
            self.message.set("no parameters submitted to save yet!")
        else:
            fn_out = self._get_fn_params()
            with open(fn_out, "w") as f:
                f.write("w={}\n".format(self.lastsubmit_w))
                f.write("s={}\n".format(self.lastsubmit_s))
                f.write("c={}\n".format(self.lastsubmit_co))
                f.write("T={}\n".format(self.lastsubmit_temp))
                f.write("p={}".format(self.lastsubmit_pseudocount))

    def _load_params(self):

        """ load params from parameter-file """

        self.submitted = False
        fn_in = self._get_fn_params()
        with open(fn_in) as f:
            for line in f:
                l = line.split("=")
                key = l[0].strip()
                val = l[1].strip()
                if key == "w":  # weights
                    if val == "None":
                        self.w_str.set("")
                    else:
                        self.w_str.set(val)
                elif key == "s":  # smoothing factor
                    self.s_str.set(val)
                elif key == "c":  # cutoff
                    self.cutoff.set(float(val))
                elif key == "T":  # temperature
                    self.temp.set(float(val))
                elif key == "p":  # pseudocount
                    self.pseudocount_str.set(val)

    def _write_energy_term(self):

        """ write a python script with the openmm energy-term """

        with open(self._get_fn_energyterm(), "w") as f:
            s = "#case={}\n".format(self.datatypes[self.case_i])
            s += "#weight={}\n#smoothing={}\n".format(self.lastsubmit_w, self.lastsubmit_s)
            s += "#cutoff={}\n#temperature={}\n".format(self.lastsubmit_co, self.lastsubmit_temp)
            s += "#pseudocount={}\n".format(self.lastsubmit_pseudocount)
            s += self.sf.openmm_energy_formula()
            s += "\n"
            f.write(s)

    def _can_merge(self):

        """ tell if all energyterms are ready they can be merged """

        for datatype in self.datatypes:
            if not os.path.isfile(self._get_fn_energyterm(datatype)):
                return False
        return True

    def _merge_forcefield(self):

        """ merge all energy terms to a single file """

        fn_ff = os.path.join(self.results_dir, "formulas.txt")
        with open(fn_ff, "w") as f_out:
            for datatype in self.datatypes:
                with open(self._get_fn_energyterm(datatype), "r") as f_in:
                    for line in f_in:
                        f_out.write(line)

    def _save(self):

        """ save stuff """

        if self.results_dir is not None:
            self._write_params()
            self.fig.savefig(self._get_fn_plot())
            self._write_energy_term()
            if self._can_merge():
                self._merge_forcefield()

    def _clear(self):

        """ delete all saved files """

        if os.path.exists(self.results_dir):
            shutil.rmtree(self.results_dir)
            os.makedirs(self.results_dir)


if __name__ == "__main__":

    path = None
    if len(sys.argv) > 1:
        path = sys.argv[1]

    app = SplineFitGui(path=path)
    app.master.title('Splinefit')
    app.mainloop()
