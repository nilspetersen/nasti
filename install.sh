#!/usr/bin/env bash

# ADAPT THESE ?

nasti_src_dir="$(pwd)"
dir_install_base=$nasti_src_dir/build

# THE REST IS FIXED, DON'T CHANGE IF YOU DON'T KNOW

start_dir="$(pwd)"

function err_exit {
    echo
    echo "ERROR IN INSTALL.SH: $1"
    echo "(To clean up just delete $dir_install_base)"
    cd $start_dir
    exit 1
}

function assert_tool {
    type "$1" &> /dev/null || err_exit "need $1, but did not find it"
}

virtual_env_name=venv_nasti

dir_nastiplugin_src=$nasti_src_dir/openmm_plugin
dir_nastiplugin_build=$dir_install_base/nastiplugin_build

dir_openmm_build=$dir_install_base/openmm_build
dir_openmm_install=$dir_install_base/openmm_install

dirs=($dir_install_base
      $dir_nastiplugin_build
      $dir_openmm_build
      $dir_openmm_install
     )

fname_python_dependencies=$nasti_src_dir/py_nasti/requirementsA.txt

echo
echo "##############################################"
echo "# Hi,                                        #"
echo "# this is the NASTI installation script, the #"
echo "# following will happen:                     #"
echo "# - a python virtual environment is set up   #"
echo "# - python dependencies are installed        #"
echo "# - OpenMM 7.2 is downloaded and build in a  #"
echo "#   local directory                          #"
echo "# - an OpenMM-plugin with NASTIs specific    #"
echo "#   energy terms is built                    #"
echo "# - NASTI is installed into the virtual      #"
echo "#   environment using pip                    #"
echo "##############################################"
echo

required_tools=(gcc
		g++
		make
		python2.7
		cmake
		virtualenv
		doxygen
)
for tool in "${required_tools[@]}"
do
   assert_tool $tool 
done

echo "building NASTI in $dir_install_base"

pypath="$(which python2.7)"

if [ ! -f $pypath ]; then
    err_exit "Python2.7 not found!"
else
    echo "Found python2.7 at $pypath"
fi

# build the directory tree
for dir in "${dirs[@]}"
do
    mkdir $dir || err_exit "failed to create directory $dir"
done

echo
echo "##############################################"
echo "# Building a python virtual environment to   #"
echo "# install NASTI                              #"
echo "##############################################"
echo

# PYTHON VIRTUAL ENVIRONMENT
# create virtual environment
cd $dir_install_base
virtualenv --no-site-packages $virtual_env_name \
	   --python=$pypath || err_exit "failed to create python virtual environment"
# activate virtual environment
source $virtual_env_name/bin/activate || err_exit "failed to load python virtual environment"
# upgrade pip
pip install --upgrade pip || err_exit "failed to upgrade pip"
# reset pypath
pypath="$(which python2.7)"
echo "new python path is $pypath"

# PYTHON DEPENDENCIES
pip install -r $fname_python_dependencies || err_exit "pip failed to install python dependencies"

cd $dir_install_base

echo
echo "##############################################"
echo "# OpenMM 7.2 build and installation          #"
echo "##############################################"
echo

# OPENMM
# download
dir_openmm_src=$dir_install_base/openmm-7.2.0
wget https://github.com/pandegroup/openmm/archive/7.2.0.tar.gz \
     || err_exit "failed to download OpenMM 7.2"
fname_openmm_tarball=7.2.0.tar.gz
tar -xzf $fname_openmm_tarball || err_exit "could not extract $fname_openmm_tarball"
# cmake
cd $dir_openmm_build
cmake_openmm_args=(
      -DCMAKE_INSTALL_PREFIX=$dir_openmm_install 
      -DCMAKE_BUILD_TYPE=Release 
      -DOPENMM_BUILD_PYTHON_WRAPPERS=ON 
      -DOPENMM_BUILD_SHARED_LIB=ON 
      -DOPENMM_BUILD_CPU_TESTS=OFF  # building tests ?
      -DOPENMM_BUILD_REFERENCE_TESTS=OFF 
      -DOPENMM_BUILD_SERIALIZATION_TESTS=OFF 
      -DOPENMM_BUILD_CUDA_COMPILER_PLUGIN=OFF  # cuda
      -DOPENMM_BUILD_CUDA_DOUBLE_PRECISION_TESTS=OFF 
      -DOPENMM_BUILD_CUDA_LIB=OFF 
      -DOPENMM_BUILD_CUDA_TESTS=OFF 
      -DOPENMM_BUILD_OPENCL_DOUBLE_PRECISION_TESTS=OFF # openCL
      -DOPENMM_BUILD_OPENCL_LIB=OFF 
      -DOPENMM_BUILD_OPENCL_TESTS=OFF 
      -DOPENMM_BUILD_C_AND_FORTRAN_WRAPPERS=OFF  # c warppers
      -DOPENMM_BUILD_EXAMPLES=OFF  # examples
      -DOPENMM_BUILD_AMOEBA_PLUGIN=ON   # plugins (required for python)
      -DOPENMM_BUILD_RPMD_PLUGIN=ON 
      -DOPENMM_BUILD_DRUDE_PLUGIN=ON
      -DOPENMM_BUILD_PME_PLUGIN=OFF 
      -DOPENMM_BUILD_AMOEBA_CUDA_LIB=OFF # opencl and cuda for plugins
      -DOPENMM_BUILD_DRUDE_CUDA_LIB=OFF 
      -DOPENMM_BUILD_DRUDE_OPENCL_LIB=OFF 
      -DOPENMM_BUILD_RPMD_CUDA_LIB=OFF
      -DOPENMM_BUILD_RPMD_OPENCL_LIB=OFF
      -DPYTHON_EXECUTABLE=$pypath
)
cmake $dir_openmm_src "${cmake_openmm_args[@]}" || err_exit "cmake configuration for OpenMM failed"
make || err_exit "OpenMM build failed"
make install || err_exit "OpenMM install failed"
make PythonInstall || err_exit "OpenMM python-install failed"

echo
echo "##############################################"
echo "# NASTI OpenMM-Plugin build and installation #"
echo "##############################################"
echo

cd $dir_nastiplugin_build
cmake_nastiplugin_args=(
    $dir_nastiplugin_src
    -DCMAKE_INSTALL_PREFIX=$dir_openmm_install
    -DCMAKE_BUILD_TYPE=Release
    -DOPENMM_DIR=$dir_openmm_install
)
cmake "${cmake_nastiplugin_args[@]}" || err_exit "cmake configuration for NASTI-plugin failed"
make || err_exit "NASTI-plugin build failed"
make install || err_exit "NASTI-plugin install failed"
make PythonInstall || err_exit "NASTI-plugin python-install failed"

echo
echo "##############################################"
echo "# NASTI python installation                  #"
echo "##############################################"
echo

# NASTI
# goto rmsd directory and type make (or distribute precompiled ?)
cd $nasti_src_dir/py_nasti/nasti/rmsd
make || err_exit "building rmsd plugin for NASTI failed"
# pip install setup.py
cd $nasti_src_dir/py_nasti
pip install . || err_exit "pip installation of NASTI failed"

# FINALLY
cd $start_dir

echo
echo "##############################################"
echo "# DONE                                       #"
echo "##############################################"
echo
